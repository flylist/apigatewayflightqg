package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/go-redis/redis"
)

func writeAvailabilityData(scheduleID string, schedule interface{}) string {
	client := redis.NewClient(&redis.Options{
		Addr:     redisServer,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer client.Close()
	var key string
	key = redisAvailLog + ":" + scheduleID
	//	log.Println(getLine(), key)
	byteData, _ := json.Marshal(schedule)
	err := client.Set(key, string(byteData), time.Minute*redisAging).Err()

	//	log.Println(getLine(), err)
	if err != nil {
		return fmt.Sprint(getLine(), "- error storing availability - ", err)
	}
	return ""
}

func convertPartnerToAPI(connectionParams *connectionParamsODT, signature string, departureCode string, partnerResponse availabilityResponseEnvelopeODT,
	apiResponse *apiAvailabilityResponseODT) {
	availibityByTrip := &partnerResponse.Body.Data.GetTripAvailabilityResponse
	schedules := availibityByTrip.Schedules
	apiResponse.Status = true
	arrAirportData := make(map[string]airportODT)

	aiportData := getAirportDetail(departureCode)
	if aiportData != nil {
		arrAirportData[departureCode] = *aiportData
	}

	for _, arrayOfJourneyMarket := range schedules.ArrayOfJourneyDateMarket {
		for _, dayOfJourney := range arrayOfJourneyMarket.JourneyDateMarket {
			journeys := dayOfJourney.Journeys.Journey
			for _, journey := range journeys {
				var schedule apiFlightScheduleODT
				var tmpJourneyMarket journeyDateMarketODT
				byteData, _ := xml.Marshal(dayOfJourney)
				xml.Unmarshal(byteData, &tmpJourneyMarket)
				tmpJourneyMarket.Journeys.Journey = []journeyODT{}
				tmpJourneys := &tmpJourneyMarket.Journeys.Journey
				//				tmpJourney := &tmpJourneyMarket.Journeys.Journey[0]
				totalDuration := uint16(0)
				schedule.Detail = []apiScheduleDetailODT{}
				stop := 0

				totalSegments := len(journey.Segments.Segment)

				var nextSegment *segmentODT = nil
				tmpJourney := journey
				tmpJourney.Segments.Segment = []segmentODT{}
				for segmentIdx, segment := range journey.Segments.Segment {
					tmpSegment := segment
					if segmentIdx+1 < totalSegments {
						nextSegment = &journey.Segments.Segment[segmentIdx+1]
					} else {
						nextSegment = nil
					}
					tmpSegment.Fares.Fare = []fareODT{}
					if schedule.DepartureDate == "" {
						td, _ := time.Parse("2006-01-02T15:04:05", segment.STD)
						schedule.DepartureDate = td.Format("2006-01-02 15:04:05")
						schedule.DepartureTime = td.Format("15:04")
					}
					ta, _ := time.Parse("2006-01-02T15:04:05", segment.STA)
					schedule.ArrivalDate = ta.Format("2006-01-02 15:04:05")
					schedule.ArrivalTime = ta.Format("15:04")
					schedule.Name = "Citilink"
					adultFare := float64(0)
					childFare := float64(0)
					srcFare := float64(0)
					infantFare := float64(0)
					selectedFSK := ""
					tmpSegments := &tmpJourney.Segments.Segment
					selectedFare := fareODT{}
					for i, fare := range segment.Fares.Fare {
						_ = i
						//						scheduleDetail.Class = fare.ClassOfService
						//						scheduleDetail.ClassW = fare.FareClassOfService
						tmpAdultFare := float64(0)
						tmpChildFare := float64(0)
						tmpSRCFare := float64(0)
						tmpInfantFare := float64(0)

						for _, paxFare := range fare.PaxFares.PaxFare {
							var ptrFare *float64
							//								log.Println(getLine(), "Pax Type ", paxFare.PaxType, serviceCharge.Amount)
							switch paxFare.PaxType {
							case "INF":
								ptrFare = &tmpInfantFare
							case "SRC":
								ptrFare = &tmpSRCFare
							case "CHD":
								ptrFare = &tmpChildFare
							default:
								ptrFare = &tmpAdultFare
							}
							for _, serviceCharge := range paxFare.ServiceCharges.BookingServiceCharge {

								multiplier := float64(1)
								switch serviceCharge.ChargeType {
								case "FarePrice", "TravelFee", "ServiceCharge", "Tax", "AddOnServicePrice", "AddOnServiceFee", "AddOnServiceMarkup", "FareSurcharge", "AddOnServiceCancelFee":
									multiplier = 1
								case "Discount":
									multiplier = -1
								default:
									log.Println(getLine(), "-Warning-unmapped ChargeType:", serviceCharge.ChargeType)
									multiplier = 0
								}
								tmpFloat := float64(serviceCharge.Amount)
								*ptrFare = *ptrFare + (tmpFloat * multiplier)
							}
						}
						if selectedFSK != "" && tmpAdultFare > adultFare {
							continue
						}
						selectedFare = fare
						byteFare, _ := xml.Marshal(fare.PaxFares)
						/*						log.Println(getLine(), journey.JourneySellKey)
												log.Println(getLine(), segment.SegmentSellKey)
												log.Println(getLine(), fare.FareSellKey)
												log.Println(getLine(), string(byteFare))*/
						xml.Unmarshal(byteFare, &selectedFare.PaxFares)
						selectedFSK = fare.FareSellKey
						infantFare = tmpInfantFare
						childFare = tmpChildFare
						adultFare = tmpAdultFare
						srcFare = tmpSRCFare
					}
					if selectedFare.AvailableCount == 0 {
						continue
					}
					tmpSegment.Fares.Fare = append(tmpSegment.Fares.Fare, selectedFare)
					//					tmpJourney.Segments.Segment = append(tmpJourney.Segments.Segment, tmpSegment)
					schedule.PriceAdult += adultFare
					schedule.PriceChild += childFare
					schedule.PriceInfant += infantFare
					schedule.PriceSRC += srcFare

					var tmpID searchIDODT
					tmpID.LogID = signature
					tmpID.FareSellKey = selectedFSK
					tmpID.SegmentSellKey = segment.SegmentSellKey
					tmpID.JourneySellKey = journey.JourneySellKey
					byteString, _ := json.Marshal(tmpID)
					schedule.ScheduleID = createHash256(string(byteString)) //fmt.Sprintf("%x", byteString)
					totalLegs := len(segment.Legs.Leg)
					for legIdx, leg := range segment.Legs.Leg {
						stop++
						var scheduleDetail apiScheduleDetailODT
						scheduleDetail.FlightNumber = strings.Trim(leg.FlightDesignator.CarrierCode, " ") + "-" +
							strings.Trim(leg.FlightDesignator.FlightNumber, " ")
						if schedule.FlightNumber == "" {
							schedule.FlightNumber = scheduleDetail.FlightNumber
						}
						scheduleDetail.ImgSrc = connectionParams.ImageURL
						scheduleDetail.AirlinesName = "CITILINK"
						scheduleDetail.AirlinesShortRealName = "CITILINK"
						scheduleDetail.OperatedBy = "Citilink"
						tmpName := strings.ToLower(scheduleDetail.AirlinesShortRealName)
						scheduleDetail.AirlinesShortRealNameUCWords = strings.Title(tmpName)
						scheduleDetail.AirportTax = dayOfJourney.IncludeTaxesAndFees
						val, ok := arrAirportData[leg.ArrivalStation]
						if !ok {
							airportData := getAirportDetail(leg.ArrivalStation)
							if airportData != nil {
								arrAirportData[leg.ArrivalStation] = *airportData
								val = *airportData
								ok = true
							}
						}
						scheduleDetail.ArrivalCity = leg.ArrivalStation
						var arrivalTZ *time.Location
						var departureTZ *time.Location

						if ok {
							scheduleDetail.ArrivalCityName = val.CityName
							scheduleDetail.ArrivalAirportName = val.AirportName
							var err error
							arrivalTZ, err = time.LoadLocation(val.TimeZone)
							if err != nil {
								arrivalTZ = nil
								log.Println(getLine(), "Error loading timezone for ", val.ID, val.TimeZone)
							}
						}

						ta, _ := time.Parse("2006-01-02T15:04:05", leg.STA)
						scheduleDetail.ArrivalDateTime = ta.Format("2006-01-02 15:04")
						scheduleDetail.SimpleArrivalTime = ta.Format("15:04")
						td, _ := time.Parse("2006-01-02T15:04:05", leg.STD)
						scheduleDetail.DepartureDateTime = td.Format("2006-01-02 15:04")
						scheduleDetail.SimpleDepartureTime = td.Format("15:04")
						scheduleDetail.DepartureCity = leg.DepartureStation
						schedule.FullVia = schedule.FullVia + leg.DepartureStation + "-" + leg.ArrivalStation +
							"(" + scheduleDetail.SimpleDepartureTime + "-" + scheduleDetail.SimpleArrivalTime + ") "

						val, ok = arrAirportData[leg.DepartureStation]
						if !ok {
							airportData := getAirportDetail(leg.DepartureStation)
							if airportData != nil {
								arrAirportData[leg.DepartureStation] = *airportData
								val = *airportData
								ok = true
							}
						}

						if ok {
							scheduleDetail.DepartureCityName = val.CityName
							scheduleDetail.DepartureAirportName = val.AirportName
							var err error
							departureTZ, err = time.LoadLocation(val.TimeZone)
							if err != nil {
								departureTZ = nil
								log.Println(getLine(), "Error loading timezone for ", val.ID, val.TimeZone)
							}
						}

						if departureTZ != nil && arrivalTZ != nil {
							t0, _ := time.ParseInLocation("2006-01-02T15:04:05", leg.STD, departureTZ)
							t1, _ := time.ParseInLocation("2006-01-02T15:04:05", leg.STA, arrivalTZ)
							diff := t1.Sub(t0)
							scheduleDetail.DurationHour = fmt.Sprint(int(diff.Hours()))
							scheduleDetail.DurationMinute = fmt.Sprint(int(diff.Minutes()) % 60)
							scheduleDetail.DurationTime = uint16(diff.Minutes())
							totalDuration = totalDuration + scheduleDetail.DurationTime

							var nextLeg *legODT = nil

							if (legIdx + 1) < totalLegs {
								nextLeg = &segment.Legs.Leg[legIdx+1]
							} else if nextSegment != nil {
								nextLeg = &nextSegment.Legs.Leg[0]
							}
							if nextLeg != nil {
								nextDeparture := nextLeg.STD
								t2, _ := time.ParseInLocation("2006-01-02T15:04:05", nextDeparture, arrivalTZ)
								diffTransit := t2.Sub(t1)
								diffTransit = diffTransit.Round(time.Minute)
								totalDuration = totalDuration + uint16(diffTransit.Minutes())
								transitHr := diffTransit / time.Hour
								scheduleDetail.TransitDurationHour = uint16(transitHr)
								diffTransit -= transitHr * time.Hour
								transitMin := diffTransit / time.Minute
								scheduleDetail.TransitDurationMinute = uint16(transitMin)
								scheduleDetail.TransitArrivalTextCity = scheduleDetail.ArrivalCityName
								scheduleDetail.TransitArrivalTexttime = scheduleDetail.ArrivalDateTime
							}
							if segment.International {
								scheduleDetail.Terminal = "International"
							} else {
								scheduleDetail.Terminal = "Domestic"
							}

						} else {
							totalDuration = 0
						}
						schedule.Detail = append(schedule.Detail, scheduleDetail)
					}

					var timeDuration time.Duration = time.Duration(totalDuration) * time.Minute
					timeDuration = timeDuration.Round(time.Minute)
					hrs := timeDuration / time.Hour
					timeDuration -= hrs * time.Hour
					mins := timeDuration / time.Minute

					hoursString := ""
					minutesString := ""
					if int(hrs) > 0 {
						hoursString = fmt.Sprintf("%d j ", hrs)
					}
					if int(mins) > 0 {
						minutesString = fmt.Sprintf("%d m", mins)
					}
					schedule.Duration = hoursString + minutesString
					if (stop - 1) > 0 {
						schedule.Stop = fmt.Sprintf("%d transit", (stop - 1))
					} else {
						schedule.Stop = "Langsung"
					}
					log.Println(getLine(), "add segment ", tmpSegment.SegmentSellKey)
					*tmpSegments = append(*tmpSegments, tmpSegment)
				}
				*tmpJourneys = append(*tmpJourneys, tmpJourney)
				tmpJourneyMarket.Signature = signature
				errString := writeAvailabilityData(schedule.ScheduleID, tmpJourneyMarket)
				if errString != "" {
					apiResponse.ErrorString = fmt.Sprint(getLine(), "-", errString)
					apiResponse.Data = apiAvailabilityDataODT{}
					apiResponse.ResultCode = 100
					apiResponse.ResultString = "Error logging availability"
					apiResponse.Status = false
					return
				}
				totalDuration = 0
				if len(schedule.Detail) < 1 {
					continue
				}
				schedule.FullVia = strings.Trim(schedule.FullVia, " ")
				//							log.Println(getLine(), "dc ", departureCode, " sdc ", segment.DepartureStation)
				if dayOfJourney.DepartureStation == departureCode {
					apiResponse.Data.Departures = append(apiResponse.Data.Departures, schedule)
				} else {
					apiResponse.Data.Returns = append(apiResponse.Data.Returns, schedule)
				}

			}

		}
	}
}

func getFlightAvailability(connectionParams *connectionParamsODT, stringRequest string) string {

	apiResponse := &apiBasicResponseODT{APIVersion: VersionString}
	signature, errCode, errString, connectionParams := getSignature(connectionParams)

	if errString != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", errString)
		apiResponse.ResultCode = int16(errCode)
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error signature")
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	//	byteString, _ := json.MarshalIndent(connectionParams, " ", " ")
	//	log.Println(getLine(), string(byteString))
	apiRequest := &apiScheduleRequestODT{}
	err := json.Unmarshal([]byte(stringRequest), apiRequest)

	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error decoding request")
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	xmlInLog := apiRequest.TraceInStringReq || connectionParams.LogInData
	xmlOutLog := apiRequest.TraceOutStringReq || connectionParams.LogOutData
	qgAvailabilityRequest := &availabilityRequestODT{}

	qgAvailabilityRequest.Header.ContractVersion = ContractVersion
	qgAvailabilityRequest.Header.Signature = signature

	requestData := &(qgAvailabilityRequest.Body.Data)
	requestData.TripAvailabilityRequest.LowFareMode = false
	requestData.TripAvailabilityRequest.LoyaltyFilter = "MonetaryOnly"

	availabilityRequest := availabilityRequestDataODT{}
	availabilityRequest.ArrivalStation = apiRequest.Data.ArrivalCode
	availabilityRequest.DepartureStation = apiRequest.Data.DepartureCode
	availabilityRequest.BeginDate = apiRequest.Data.DepartureDate + "T00:00:00"
	if apiRequest.Data.EndDepartureDate != "" {
		availabilityRequest.EndDate = apiRequest.Data.EndDepartureDate + "T00:00:00"
	} else {
		availabilityRequest.EndDate = apiRequest.Data.DepartureDate + "T00:00:00"
	}
	availabilityRequest.InboundOutbound = "Both"
	availabilityRequest.CarrierCode = "QG"
	availabilityRequest.FlightType = "All"
	availabilityRequest.PaxCount = apiRequest.Data.Adult + apiRequest.Data.Child + apiRequest.Data.Infant
	availabilityRequest.Dow = "Daily"
	availabilityRequest.CurrencyCode = "IDR"
	availabilityRequest.AvailabilityType = "Default"
	availabilityRequest.MaximumConnectingFlight = uint16(connectionParams.MaxConnectings)
	availabilityRequest.AvailabilityFilter = "Default"
	availabilityRequest.FareClassControl = "Default"
	availabilityRequest.MinimumFarePrice = "0"
	availabilityRequest.MaximumFarePrice = "0"
	availabilityRequest.SSRCollectionsMode = "None"
	availabilityRequest.IncludeAllotments = false
	for i := uint8(0); i < apiRequest.Data.Adult; i++ {
		//	if apiRequest.Data.Adult > 0 {
		ppt := paxPriceTypeODT{PaxType: "ADT"}
		availabilityRequest.PaxPriceTypes.PaxPriceType = append(availabilityRequest.PaxPriceTypes.PaxPriceType, ppt)
	}

	for i := uint8(0); i < apiRequest.Data.Child; i++ {
		//	if apiRequest.Data.Child > 0 {
		ppt := paxPriceTypeODT{PaxType: "CHD"}
		availabilityRequest.PaxPriceTypes.PaxPriceType = append(availabilityRequest.PaxPriceTypes.PaxPriceType, ppt)
	}

	for i := uint8(0); i < apiRequest.Data.Infant; i++ {
		//	if apiRequest.Data.Infant > 0 {
		ppt := paxPriceTypeODT{PaxType: "INF"}
		availabilityRequest.PaxPriceTypes.PaxPriceType = append(availabilityRequest.PaxPriceTypes.PaxPriceType, ppt)
	}

	availabilityRequest.JourneySortKeys.JourneySortKey = append(availabilityRequest.JourneySortKeys.JourneySortKey, "EarliestDeparture")
	availabilityRequest.IncludeTaxesAndFees = true
	availabilityRequest.FareRuleFilter = "Default"
	availabilityRequest.LoyaltyFilter = "MonetaryOnly"
	availabilityRequest.PaxResidentCountry = "ID"
	arrAvailabiltyRequest := &requestData.TripAvailabilityRequest.AvailabilityRequests
	arrAvailabiltyRequest.AvailabilityRequest = append(arrAvailabiltyRequest.AvailabilityRequest, availabilityRequest)
	if apiRequest.Data.Return {
		if apiRequest.Data.ReturnDate == "" {
			apiResponse.ErrorString = fmt.Sprintln(getLine(), "-return true but no return date")
			apiResponse.ResultCode = 100
			apiResponse.Status = apiResponse.ResultCode == 0
			apiResponse.ResultString = fmt.Sprintln("Return date is mandatory if return = true")
			byteData, _ := json.Marshal(apiResponse)
			return string(byteData)
		}
		availabilityRequest.ArrivalStation = apiRequest.Data.DepartureCode
		availabilityRequest.DepartureStation = apiRequest.Data.ArrivalCode
		availabilityRequest.BeginDate = apiRequest.Data.ReturnDate + "T00:00:00"
		if apiRequest.Data.EndReturnDate != "" {
			availabilityRequest.EndDate = apiRequest.Data.EndReturnDate + "T00:00:00"
		} else {
			availabilityRequest.EndDate = apiRequest.Data.ReturnDate + "T00:00:00"
		}
		arrAvailabiltyRequest.AvailabilityRequest = append(arrAvailabiltyRequest.AvailabilityRequest, availabilityRequest)
	}
	cmd := "availability"

	soapCmd, errString := getCmdData(connectionParams, cmd)
	if errString != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Error get command data-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error get connection data")
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	if soapCmd == nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Command data not found-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Connection data not found")
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	byteData, _ := xml.MarshalIndent(qgAvailabilityRequest, " ", " ")
	if xmlOutLog {
		log.Println(getLine(), "To Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	req, err := http.NewRequest("POST", soapCmd.URL, bytes.NewReader(byteData))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error preparing request")
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}
	req.Header.Add("Content-Type", "text/xml")
	req.Header.Add("SOAPAction", soapCmd.ActionHeader)
	//	log.Println(getLine(), connectionParams.SOAPCommand["login"].URL)
	client := &http.Client{}
	log.Println(getLine(), "Send availability request")
	resp, err := client.Do(req)
	log.Println(getLine(), "Receive availability response")
	//	log.Println(getLine(), string(bodyRequest))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error during sending request")
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}
	defer resp.Body.Close()
	byteData, _ = ioutil.ReadAll(resp.Body)
	if xmlInLog {
		log.Println(getLine(), "From Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	var partnerResponse availabilityResponseEnvelopeODT
	err = xml.Unmarshal(byteData, &partnerResponse)

	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error(), "\n", string(byteData))
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error decoding response")
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	byteData, _ = xml.MarshalIndent(partnerResponse, " ", " ")
	if xmlInLog {
		log.Println(getLine(), string(byteData))
	}
	byteData, _ = json.Marshal(apiResponse)
	if partnerResponse.Body.Fault.FaultCode != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", partnerResponse.Body.Fault.FaultCode, "-", partnerResponse.Body.Fault.FaultString)
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Partner error: ", partnerResponse.Body.Fault.FaultString)
		return string(byteData)
	}

	apiFullResponse := &apiAvailabilityResponseODT{apiBasicResponseODT: apiBasicResponseODT{APIVersion: VersionString}}
	log.Println(getLine(), "Convert response")
	convertPartnerToAPI(connectionParams, signature, apiRequest.Data.DepartureCode, partnerResponse, apiFullResponse)
	log.Println(getLine(), "Send response to client")
	byteData, err = json.Marshal(apiFullResponse)
	return string(byteData)
	//	fmt.Fprintf(w, "%s", string(byteData))
}
