package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func updatePassenger(scheduleID string, apiAdult []apiPaxDetailODT, apiChild []apiPaxDetailODT,
	apiInfant []apiPaxDetailODT, xmlInLog bool, xmlOutLog bool) (string, string) {

	apiResponse := &apiBasicResponseODT{APIVersion: VersionString}
	var scheduleInfo journeyDateMarketODT
	scheduleInfoString, errString := getScheduleInfo(scheduleID)
	if errString != "" {
		apiResponse.ResultString = "Error get schedule info"
		apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString, "-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = false
		byteString, _ := json.Marshal(apiResponse)
		return "", string(byteString)
	}

	if scheduleInfoString == "" {
		apiResponse.ResultString = "Schedule id not found - " + scheduleID
		apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString)
		apiResponse.ResultCode = 100
		apiResponse.Status = false
		byteString, _ := json.Marshal(apiResponse)
		return "", string(byteString)
	}
	err := json.Unmarshal([]byte(scheduleInfoString), &scheduleInfo)

	if err != nil {
		apiResponse.ResultString = "Error decoding schedule info"
		apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString, "-", err.Error())
		apiResponse.ResultCode = 100
		apiResponse.Status = false
		byteString, _ := json.Marshal(apiResponse)
		return "", string(byteString)
	}

	signature := scheduleInfo.Signature

	partnerRequest := updatePassengerRequestEnvelopeODT{Header: requestHeaderODT{ContractVersion: ContractVersion, Signature: signature}}
	partnerData := &partnerRequest.Body.Data.UpdatePassengersRequestData.Passengers.Passenger

	//adult
	paxNo := uint8(0)
	for i, apiPax := range apiAdult {
		pax := passengerODT{}
		pax.State = "New"
		pax.PassengerNumber = paxNo
		paxNo = paxNo + 1
		pax.FamilyNumber = 0
		name := bookingNameODT{}
		name.FirstName = apiPax.FirstName
		name.LastName = apiPax.LastName
		name.Title = apiPax.Title
		pax.Names.BookingName = append(pax.Names.BookingName, name)
		if i < len(apiInfant) {
			infant := &passengerInfantODT{}
			infant.State = "New"
			infant.BirthDate = apiInfant[i].BirthDate
			switch strings.ToLower(apiInfant[i].Title) {
			case "mrs", "miss", "ms":
				infant.Gender = "Female"
			default:
				infant.Gender = "Male"
			}
			infant.Nationality = apiInfant[i].Nationality
			infName := bookingNameODT{}
			infName.FirstName = apiInfant[i].FirstName
			infName.LastName = apiInfant[i].LastName
			infName.Title = apiInfant[i].Title
			infant.Names.BookingName = append(infant.Names.BookingName, infName)
			pax.Infant = infant
		}
		switch strings.ToLower(apiPax.Title) {
		case "mrs", "miss", "ms":
			pax.PassengerInfo.Gender = "Female"
		default:
			pax.PassengerInfo.Gender = "Male"
		}
		pax.PassengerInfo.Nationality = apiPax.Nationality
		pax.PassengerInfo.WeightCategory = pax.PassengerInfo.Gender
		paxTypeInfo := passengerTypeInfoODT{}
		paxTypeInfo.BirthDate = apiPax.BirthDate
		paxTypeInfo.PaxType = "ADT"
		paxTypeInfo.State = "New"
		pax.PassengerTypeInfos.PassengerTypeInfo = append(pax.PassengerTypeInfos.PassengerTypeInfo, paxTypeInfo)
		pax.PassengerInfos.PassengerInfo = append(pax.PassengerInfos.PassengerInfo, pax.PassengerInfo)
		pax.PseudoPassenger = false
		pax.PassengerTypeInfo = paxTypeInfo
		*partnerData = append(*partnerData, pax)
	}

	//child
	for _, apiPax := range apiChild {
		pax := passengerODT{}
		pax.State = "New"
		pax.PassengerNumber = paxNo
		paxNo = paxNo + 1
		pax.FamilyNumber = 0
		name := bookingNameODT{}
		//		log.Println(getLine(), apiPax)
		name.FirstName = apiPax.FirstName
		name.LastName = apiPax.LastName
		name.Title = apiPax.Title
		pax.Names.BookingName = append(pax.Names.BookingName, name)
		switch strings.ToLower(apiPax.Title) {
		case "mrs", "miss", "ms":
			pax.PassengerInfo.Gender = "Female"
		default:
			pax.PassengerInfo.Gender = "Male"
		}
		pax.PassengerInfo.Nationality = apiPax.Nationality
		pax.PassengerInfo.WeightCategory = "Child"
		paxTypeInfo := passengerTypeInfoODT{}
		paxTypeInfo.BirthDate = apiPax.BirthDate
		paxTypeInfo.PaxType = "CHD"
		paxTypeInfo.State = "New"
		pax.PassengerTypeInfos.PassengerTypeInfo = append(pax.PassengerTypeInfos.PassengerTypeInfo, paxTypeInfo)
		pax.PassengerInfos.PassengerInfo = append(pax.PassengerInfos.PassengerInfo, pax.PassengerInfo)
		pax.PseudoPassenger = false
		pax.PassengerTypeInfo = paxTypeInfo
		*partnerData = append(*partnerData, pax)
	}

	cmd := "update_pax"

	byteData, _ := xml.MarshalIndent(partnerRequest, " ", " ")
	soapCmd, errString := getCmdData(connectionParams, cmd)
	if errString != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Error get command data-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error get connection data")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	if soapCmd == nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Command data not found-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Connection data not found")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	if xmlOutLog {
		log.Println(getLine(), "To Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	req, err := http.NewRequest("POST", soapCmd.URL, bytes.NewReader(byteData))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error preparing request")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	req.Header.Add("Content-Type", "text/xml")
	req.Header.Add("SOAPAction", soapCmd.ActionHeader)
	client := &http.Client{}
	log.Println(getLine(), "Send update passengers request")
	resp, err := client.Do(req)
	log.Println(getLine(), "Receive update passengers response")

	//	log.Println(getLine(), string(bodyRequest))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error during sending request")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}
	defer resp.Body.Close()
	byteData, _ = ioutil.ReadAll(resp.Body)
	var partnerResponse updatePassengerResponseEnvelopeODT
	err = xml.Unmarshal(byteData, &partnerResponse)
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error(), "\n", string(byteData))
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error decoding response")
		byteData, _ := json.Marshal(apiResponse)
		if xmlInLog {
			log.Println(getLine(), "From Partner ")
			log.Println(getLine(), string(byteData))
			log.Println()
		}

		return "", string(byteData)
	}

	//	log.Println(getLine(), string(byteData))
	byteData, _ = xml.MarshalIndent(partnerResponse, " ", " ")
	if xmlInLog {
		log.Println(getLine(), "From Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	if partnerResponse.Body.Fault.FaultCode != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", partnerResponse.Body.Fault.FaultCode, "-", partnerResponse.Body.Fault.FaultString)
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Partner error: ", partnerResponse.Body.Fault.FaultString)
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}
	return string(byteData), ""
}
