package main

import (
	"encoding/xml"
	"strconv"
)

const ContractVersion = "0"

type prettyFloat64 float64

func (d prettyFloat64) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	floatString := strconv.FormatFloat(float64(d), 'f', -1, 64)
	e.EncodeElement(floatString, start)

	return nil
}

type faultODT struct {
	FaultCode   string `xml:"faultcode,omitempty" json:"fault_code,omitempty" bson:"fault_code,omitempty"`
	FaultString string `xml:"faultstring,omitempty" json:"fault_string,omitempty" bson:"fault_string,omitempty"`
	Detail      string `xml:"detail,omitempty" json:"detail,omitempty" bson:"detail,omitempty"`
}

type requestHeaderODT struct {
	ContractVersion string `xml:"http://schemas.navitaire.com/WebServices ContractVersion" json:"contract_version" bson:"contract_version"`
	Signature       string `xml:"http://schemas.navitaire.com/WebServices Signature,omitempty" json:"signature,omitempty" bson:"signature,omitempty"`
}

type logonRequestData struct {
	DomainCode   string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Session DomainCode" json:"domain_code" bson:"domain_code"`
	AgentName    string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Session AgentName" json:"agent_name" bson:"agent_name"`
	Password     string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Session Password" json:"password" bson:"password"`
	LocationCode string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Session LocationCode" json:"location_code" bson:"location_code"`
	RoleCode     string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Session RoleCode" json:"role_code" bson:"role_code"`
	TerminalInfo string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Session TerminalInfo" json:"terminal_info" bson:"terminal_info"`
}

type logonRequestDataODT struct {
	Param logonRequestData `xml:"logonRequestData" json:"logon_data" bson:"logon_data"`
}
type logonRequestBodyODT struct {
	Xmlns string              `xml:"xmlns:ses,attr" json:"-"` //="http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService" bson:"-"` //="http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService"`
	Data  logonRequestDataODT `xml:"http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService ses:LogonRequest" json:"data" bson:"data"`
}

type logonRequestEnvelopeODT struct {
	XMLName xml.Name            `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	Header  requestHeaderODT    `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    logonRequestBodyODT `xml:"Body" json:"body" bson:"body"`
}

func initLogonRequest() *logonRequestEnvelopeODT {
	lr := &logonRequestEnvelopeODT{}
	lr.Body.Xmlns = "http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService"
	lr.Header.ContractVersion = ContractVersion
	return lr
}

type logonResponseODT struct {
	Signature string `xml:"Signature" json:"signature" bson:"signature"`
}
type logonResponseBodyODT struct {
	Fault faultODT         `xml:"Fault,omitempty" json:"fault,omitempty" bson:"fault,omitempty"`
	Data  logonResponseODT `xml:"LogonResponse" json:"data" bson:"data"`
}

type logonResponseEnvelopeODT struct {
	XMLName xml.Name             `xml:"Envelope"`
	Header  string               `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    logonResponseBodyODT `xml:"Body" json:"body" bson:"body"`
}

type paxPriceTypeODT struct {
	PaxType         string `xml:"PaxType" json:"pax_type" bson:"pax_type"`                                                         //ADT,CHD,SRC
	PaxDiscountCode string `xml:"PaxDiscountCode,omitempty" json:"pax_discount_code,omitempty" bson:"pax_discount_code,omitempty"` //ADT,CHD,SRC
}

type paxPriceTypesODT struct {
	PaxPriceType []paxPriceTypeODT `xml:"PaxPriceType,omitempty" json:"pax_price_types,omitempty" bson:"pax_price_types,omitempty"`
}

type stringsODT struct {
	Strings []string `xml:"string,omitempty" json:"strings,omitempty" bson:"strings,omitempty"`
}

type journeySortKeysODT struct {
	JourneySortKey []string `xml:"JourneySortKey" json:"journey_sort_keys" bson:"journey_sort_keys"` //EarliestDeparture
}

type charsODT struct {
	Char []string `xml:"char,omitempty" json:"char,omitempty" bson:"char,omitempty"`
}

type availabilityRequestDataODT struct {
	DepartureStation          string             `xml:"DepartureStation" json:"departure_code" bson:"departure_code"`                                                               //CGK
	ArrivalStation            string             `xml:"ArrivalStation" json:"arrival_code" bson:"arrival_code"`                                                                     //SBY
	BeginDate                 string             `xml:"BeginDate" json:"departure_date" bson:"departure_date"`                                                                      //2019-09-20'T00:00:00'
	EndDate                   string             `xml:"EndDate,omitempty" json:"return_date" bson:"return_date"`                                                                    //2019-09-20'T00:00:00'
	CarrierCode               string             `xml:"CarrierCode" json:"carrier_code" bson:"carrier_code"`                                                                        //QG
	FlightType                string             `xml:"FlightType" json:"flight_type" bson:"flight_type"`                                                                           //All
	PaxCount                  uint8              `xml:"PaxCount" json:"pax_count" bson:"pax_count"`                                                                                 //1
	Dow                       string             `xml:"Dow" json:"dow" bson:"dow"`                                                                                                  //Daily                                                  //Daily
	CurrencyCode              string             `xml:"CurrencyCode" json:"currency" bson:"currency"`                                                                               //IDR                                           //IDR
	DisplayCurrencyCode       string             `xml:"DisplayCurrencyCode" json:"display_currency" bson:"display_currency"`                                                        //IDR
	DiscountCode              string             `xml:"DiscountCode" json:"discount_code" bson:"discount_code"`                                                                     //IDR
	PromotionCode             string             `xml:"PromotionCode" json:"promotion_code" bson:"promotion_code"`                                                                  //IDR
	AvailabilityType          string             `xml:"AvailabilityType,omitempty" json:"availability_type,omitempty" bson:"availability_type,omitempty"`                           //Default
	SourceOrganization        string             `xml:"SourceOrganization" json:"source_organization" bson:"source_organization"`                                                   //Default
	MaximumConnectingFlight   uint16             `xml:"MaximumConnectingFlights,omitempty" json:"maximum_connecting_flights,omitempty" bson:"maximum_connecting_flights,omitempty"` // for connecting flight
	AvailabilityFilter        string             `xml:"AvailabiltyFilter" json:"availability_filter" bson:"availability_filter"`                                                    //Default                                   //Default
	FareClassControl          string             `xml:"FareClassControl" json:"fare_class_control" bson:"fare_class_control"`                                                       //Default
	MinimumFarePrice          string             `xml:"MinimumFarePrice" json:"minimum_fare_price" bson:"minimum_fare_price"`
	MaximumFarePrice          string             `xml:"MaximumFarePrice" json:"maximum_fare_price" bson:"maximum_fare_price"`
	ProductClassCode          string             `xml:"ProductClassCode,omitempty" json:"product_class_code,omitempty" bson:"product_class_code,omitempty"`
	SSRCollectionsMode        string             `xml:"SSRCollectionsMode" json:"ssr_collections_mode" bson:"ssr_collections_mode"` //None
	InboundOutbound           string             `xml:"InboundOutbound,omitempty" json:"inbound_outbound,omitempty" bson:"inbound_outbound,omitempty"`
	NightsStay                uint16             `xml:"NightsStay" json:"nights_stay" bson:"nights_stay"`
	IncludeAllotments         bool               `xml:"IncludeAllotments" json:"include_allotments" bson:"include_allotments"` //false
	BeginTime                 string             `xml:"BeginTime,omitempty" json:"begin_time,omitempty" bson:"begin_time,omitempty"`
	EndTime                   string             `xml:"EndTime,omitempty" json:"end_time,omitempty" bson:"end_time,omitempty"`
	DepartureStations         stringsODT         `xml:"DepartureStations,omitempty" json:"departure_codes,omitempty" bson:"departure_codes,omitempty"`
	ArrivalStations           stringsODT         `xml:"ArrivalStations,omitempty" json:"arrival_codes,omitempty" bson:"arrival_codes,omitempty"`
	FareTypes                 stringsODT         `xml:"FareTypes,omitempty" json:"fare_types,omitempty" bson:"fare_types,omitempty"`
	ProductClasses            stringsODT         `xml:"ProductClasses,omitempty" json:"product_clases,omitempty" bson:"product_clases,omitempty"`
	FareClasses               stringsODT         `xml:"FareClasses,omitempty" json:"fare_classes,omitempty" bson:"fare_classes,omitempty"`
	PaxType                   string             `xml:"PaxType" json:"pax_type" bson:"pax_type"`                                                   //ADT,CHD,SRC
	PaxPriceTypes             paxPriceTypesODT   `xml:"PaxPriceTypes,omitempty" json:"pax_price_types,omitempty" bson:"pax_price_types,omitempty"` // Only if PaxCount >1
	JourneySortKeys           journeySortKeysODT `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations JourneySortKeys" json:"journey_sort_keys" bson:"journey_sort_keys"`
	TravelClassCodes          charsODT           `xml:"TravelClassCodes,omitempty" json:"travel_class_codes,omitempty" bson:"travel_class_codes,omitempty"`
	IncludeTaxesAndFees       bool               `xml:"IncludeTaxesAndFees" json:"include_taxes_and_fees" bson:"include_taxes_and_fees"` //true
	FareRuleFilter            string             `xml:"FareRuleFilter" json:"fare_rule_filter" bson:"fare_rule_filter"`                  //Default
	LoyaltyFilter             string             `xml:"LoyaltyFilter" json:"loyalty_filter" bson:"loyalty_filter"`                       //MonetaryOnly
	PaxResidentCountry        string             `xml:"PaxResidentCountry" json:"pax_resindent_country" bson:"pax_resindent_country"`    //ID
	TravelClassCodeList       stringsODT         `xml:"TravelClassCodeList" json:"travel_class_code_list" bson:"travel_class_code_list"`
	SystemCode                string             `xml:"SystemCode" json:"system_code" bson:"system_code"`
	CurrentSourceOrganization string             `xml:"CurrentSourceOrganization" json:"current_source_organization" bson:"current_source_organization"`
}

type availabilityRequestsODT struct {
	AvailabilityRequest []availabilityRequestDataODT `xml:"AvailabilityRequest" json:"availabilty_request" bson:"availabilty_request"`
}

type tripAvailabilityRequestODT struct {
	AvailabilityRequests availabilityRequestsODT `xml:"AvailabilityRequests" json:"availability_requests" bson:"availability_requests"`
	LoyaltyFilter        string                  `xml:"LoyaltyFilter" json:"loyalty_filter" bson:"loyalty_filter"` //MonetaryOnly
	LowFareMode          bool                    `xml:"LowFareMode" json:"low_fare_mode" bson:"low_fare_mode"`     //false
}

type getAvailabilityRequestODT struct {
	TripAvailabilityRequest tripAvailabilityRequestODT `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking TripAvailabilityRequest" json:"trip_availability_request" bson:"trip_availability_request"`
}

type availabilityRequestODT struct {
	XMLName xml.Name         `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	Header  requestHeaderODT `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    struct {
		Data getAvailabilityRequestODT `xml:"http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService GetAvailabilityRequest" json:"data" bson:"data"`
	} `xml:"Body" json:"body" bson:"body"`
}

type flightDesignatorODT struct {
	CarrierCode  string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common CarrierCode" json:"carrier_code" bson:"carrier_code"`
	FlightNumber string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common FlightNumber" json:"flight_number" bson:"flight_number"`
	OpSuffix     string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common OpSuffix" json:"op_suffix" bson:"op_suffix"`
}

type bookingServiceChargeODT struct {
	State               string        `xml:"State" json:"state" bson:"state"`
	ChargeType          string        `xml:"ChargeType" json:"charge_type" bson:"charge_type"`
	CollectType         string        `xml:"CollectType" json:"collect_type" bson:"collect_type"`
	ChargeCode          string        `xml:"ChargeCode" json:"charge_code" bson:"charge_code"`
	TicketCode          string        `xml:"TicketCode" json:"ticket_code" bson:"ticket_code"`
	CurrencyCode        string        `xml:"CurrencyCode" json:"currency" bson:"currency"`
	Amount              prettyFloat64 `xml:"Amount" json:"amount" bson:"amount"`
	ChargeDetail        string        `xml:"ChargeDetail" json:"charge_detail" bson:"charge_detail"`
	ForeignCurrencyCode string        `xml:"ForeignCurrencyCode" json:"foreign_currency" bson:"foreign_currency"`
	ForeignAmount       prettyFloat64 `xml:"ForeignAmount" json:"foreign_amount" bson:"foreign_amount"`
}

type serviceChargesODT struct {
	BookingServiceCharge []bookingServiceChargeODT `xml:"BookingServiceCharge" json:"booking_service_charge" bson:"booking_service_charge"`
}

type paxFareODT struct {
	State           string            `xml:"State" json:"state" bson:"state"`
	PaxType         string            `xml:"PaxType" json:"pax_type" bson:"pax_type"`
	PaxDiscountCode string            `xml:"PaxDiscountCode" json:"pax_discount_code" bson:"pax_discount_code"`
	ServiceCharges  serviceChargesODT `xml:"ServiceCharges" json:"service_charges" bson:"service_charges"`
}

type paxFaresODT struct {
	PaxFare []paxFareODT `xml:"PaxFare" json:"pax_fare" bson:"pax_fare"`
}

type fareODT struct {
	Type                   string      `xml:"type,attr" json:"type" bson:"type"`
	State                  string      `xml:"State" json:"state" bson:"state"`
	ClassOfService         string      `xml:"ClassOfService" json:"class_of_service" bson:"class_of_service"`
	ClassType              string      `xml:"ClassType" json:"class_type" bson:"class_type"`
	RuleTariff             string      `xml:"RuleTariff" json:"rule_tariff" bson:"rule_tariff"`
	CarrierCode            string      `xml:"CarrierCode" json:"carrier_code" bson:"carrier_code"`
	RuleNumber             string      `xml:"RuleNumber" json:"rule_number" bson:"rule_number"`
	FareBasisCode          string      `xml:"FareBasisCode" json:"fare_basis_code" bson:"fare_basis_code"`
	FareSequence           uint16      `xml:"FareSequence" json:"fare_sequence" bson:"fare_sequence"`
	FareClassOfService     string      `xml:"FareClassOfService" json:"fare_class_of_service" bson:"fare_class_of_service"`
	FareStatus             string      `xml:"FareStatus" json:"fare_status" bson:"fare_status"`
	FareApplicationType    string      `xml:"FareApplicationType" json:"fare_application_type" bson:"fare_application_type"`
	OriginalClassOfService string      `xml:"OriginalClassOfService" json:"original_class_of_service" bson:"original_class_of_service"`
	XrefClassOfService     string      `xml:"XrefClassOfService" json:"xref_class_of_service" bson:"xref_class_of_service"`
	PaxFares               paxFaresODT `xml:"PaxFares" json:"pax_fares" bson:"pax_fares"`
	ProductClass           string      `xml:"ProductClass" json:"product_class" bson:"product_class"`
	IsAllotmentMarketFare  bool        `xml:"IsAllotmentMarketFare" json:"is_allotment_market_fare" bson:"is_allotment_market_fare"`
	TravelClassCode        string      `xml:"TravelClassCode" json:"travel_class_code" bson:"travel_class_code"`
	FareSellKey            string      `xml:"FareSellKey" json:"fare_sell_key" bson:"fare_sell_key"`
	InboundOutbound        string      `xml:"InboundOutbound" json:"inbound_outbound" bson:"inbound_outbound"`
	AvailableCount         uint16      `xml:"AvailableCount" json:"available_count" bson:"available_count"`
	Status                 string      `xml:"Status" json:"status" bson:"status"`
	SSRIndexes             string      `xml:"SSRIndexes" json:"ssr_indexes" bson:"ssr_indexes"`
}

type faresODT struct {
	Fare []fareODT `xml:"Fare" json:"fare" bson:"fare"`
}

type legInfoODT struct {
	State                 string `xml:"State" json:"state" bson:"state"`
	AdjustedCapacity      uint16 `xml:"AdjustedCapacity" json:"adjusted_capacity" bson:"adjusted_capacity"`
	EquipmentType         string `xml:"EquipmentType" json:"equipment_type" bson:"equipment_type"`
	EquipmentTypeSuffix   string `xml:"EquipmentTypeSuffix" json:"equipment_type_suffix" bson:"equipment_type_suffix"`
	ArrivalTerminal       string `xml:"ArrivalTerminal" json:"arrival_terminal" bson:"arrival_terminal"`
	ArrvLTV               string `xml:"ArrvLTV" json:"arrv_ltv" bson:"arrv_ltv"`
	Capacity              uint16 `xml:"Capacity" json:"capacity" bson:"capacity"`
	CodeShareIndicator    string `xml:"CodeShareIndicator" json:"code_share_indicator" bson:"code_share_indicator"`
	DepartureTerminal     string `xml:"DepartureTerminal" json:"departure_terminal" bson:"departure_terminal"`
	DepvLTV               string `xml:"DepLTV" json:"dep_ltv" bson:"dep_ltv"`
	ETicket               bool   `xml:"ETicket" json:"eticket" bson:"eticket"`
	FlifoUpdated          bool   `xml:"FlifoUpdated" json:"flifo_updated" bson:"flifo_updated"`
	IROP                  bool   `xml:"IROP" json:"irop" bson:"irop"`
	Status                string `xml:"Status" json:"status" bson:"status"`
	Lid                   string `xml:"Lid" json:"lid" bson:"lid"`
	OnTime                string `xml:"OnTime" json:"on_time" bson:"on_time"`
	PaxSTA                string `xml:"PaxSTA" json:"pax_sta" bson:"pax_sta"` //yyyy-mm-dd'T'h24:min:ss
	PaxSTD                string `xml:"PaxSTD" json:"pax_std" bson:"pax_std"` //yyyy-mm-dd'T'h24:min:ss
	PRBCCode              string `xml:"PRBCCode" json:"prbc_code" bson:"prbc_code"`
	ScheduleServiceType   string `xml:"ScheduleServiceType" json:"schedule_service_type" bson:"schedule_service_type"`
	Sold                  uint16 `xml:"Sold" json:"sold" bson:"sold"`
	OutMoveDays           uint16 `xml:"OutMoveDays" json:"out_move_days" bson:"out_move_days"`
	BackMoveDays          uint16 `xml:"BackMoveDays" json:"back_move_days" bson:"back_move_days"`
	LegNests              string `xml:"LegNests" json:"leg_nests" bson:"leg_nests"`
	LegSSRs               string `xml:"LegSSRs" json:"leg_ssrs" bson:"leg_ssrs"`
	OperatingFlightNumber string `xml:"OperatingFlightNumber" json:"operating_flight_number" bson:"operating_flight_number"`
	OperatedByText        string `xml:"OperatedByText" json:"operated_by_text" bson:"operated_by_text"`
	OperatingCarrier      string `xml:"OperatingCarrier" json:"operating_carrier" bson:"operating_carrier"`
	OperatingOpSuffix     string `xml:"OperatingOpSuffix" json:"operating_op_suffix" bson:"operating_op_suffix"`
	SubjectToGovtApproval bool   `xml:"SubjectToGovtApproval" json:"subject_to_govt_approval" bson:"subject_to_govt_approval"`
	MarketingCode         string `xml:"MarketingCode" json:"marketing_code" bson:"marketing_code"`
	ChangeOfDirection     bool   `xml:"ChangeOfDirection" json:"change_of_direction" bson:"change_of_direction"`
	MarketingOverride     bool   `xml:"MarketingOverride" json:"marketing_override" bson:"marketing_override"`
}

type legODT struct {
	State            string              `xml:"State" json:"state" bson:"state"`
	ArrivalStation   string              `xml:"ArrivalStation" json:"arrival_code" bson:"arrival_code"`
	DepartureStation string              `xml:"DepartureStation" json:"departure_code" bson:"departure_code"`
	STA              string              `xml:"STA" json:"sta" bson:"sta"` // yyyy-mm-dd'T'H24:min:ss
	STD              string              `xml:"STD" json:"std" bson:"std"` // yyyy-mm-dd'T'H24:min:ss
	FlightDesignator flightDesignatorODT `xml:"FlightDesignator" json:"flight_designator" bson:"flight_designator"`
	LegInfo          legInfoODT          `xml:"LegInfo" json:"leg_info" bson:"leg_info"`
	OperationsInfo   string              `xml:"OperationsInfo" json:"operations_info" bson:"operations_info"`
	InventoryLegID   string              `xml:"InventoryLegID" json:"inventory_leg_id" bson:"inventory_leg_id"`
}

type legsODT struct {
	Leg []legODT `xml:"Leg" json:"leg" bson:"leg"`
}

type paxBagODT struct {
	State            string `xml:"State,omitempty" json:"state,omitempty" bson:"state,omitempty"`
	PassengerNumber  uint16 `xml:"PassengerNumber,omitempty" json:"passenger_number,omitempty" bson:"passenger_number,omitempty"`
	ArrivalStation   string `xml:"ArrivalStation,omitempty" json:"arrival_station,omitempty" bson:"arrival_station,omitempty"`
	DepartureStation string `xml:"DepartureStation,omitempty" json:"departure_station,omitempty" bson:"departure_station,omitempty"`
	BaggageStatus    string `xml:"BaggageStatus,omitempty" json:"baggage_satus,omitempty" bson:"baggage_satus,omitempty"` /*
		Default Checked Removed Added AddedPrinted Unmapped */
	CompartmentID uint16 `xml:"CompartmentID,omitempty" json:"compartment_id,omitempty" bson:"compartment_id,omitempty"`
	OSTag         string `xml:"OSTag,omitempty" json:"os_tag,omitempty" bson:"os_tag,omitempty"`
	OSTagDate     string `xml:"OSTagDate,omitempty" json:"os_tag_date,omitempty" bson:"os_tag_date,omitempty"`
}

type paxBagsODT struct {
	PaxBag []paxBagODT `xml:"PaxBag,omitempty" json:"pax_bag,omitempty" bson:"pax_bag,omitempty"`
}

type paxScoreODT struct {
	Score            int32  `xml:"Score,score" json:"score,omitempty" bson:"score,omitempty"`
	PassengerNumber  uint16 `xml:"PassengerNumber,omitempty" json:"passenger_number,omitempty" bson:"passenger_number,omitempty"`
	ActionStatusCode string `xml:"ActionStatusCode,omitempty" json:"action_status_code,omitempty" bson:"action_status_code,omitempty"`
	GuestValueCode   string `xml:"GuestValueCode,omitempty" json:"guest_value_code,omitempty" bson:"guest_value_code,omitempty"`
}

type paxScoresODT struct {
	PaxScore []paxScoreODT `xml:"PaxScore,omitempty" json:"pax_score,omitempty" bson:"pax_score,omitempty"`
}

type paxSeatPreferencesODT struct {
	PaxSeatPreference []paxSeatPreferenceODT `xml:"PaxSeatPreference,omitempty" json:"pax_seat_preference,omitempty" bson:"pax_seat_preference,omitempty"`
}

type paxSeatPreferenceODT struct {
	ActionStatusCode string `xml:"ActionStatusCode,omitempty" json:"action_status_code,omitempty" bson:"action_status_code,omitempty"`
	PassengerNumber  uint16 `xml:"PassengerNumber,omitempty" json:"passenger_number,omitempty" bson:"passenger_number,omitempty"`
	PropertyTypeCode string `xml:"PropertyTypeCode,omitempty" json:"property_type_code,omitempty" bson:"property_type_code,omitempty"`
	PropertyCode     string `xml:"PropertyCode,omitempty" json:"property_code,omitempty" bson:"property_code,omitempty"`
	Met              string `xml:"Met,omitempty" json:"met,omitempty" bson:"met,omitempty"`
}

type paxTicketODT struct {
	State              string `xml:"State" json:"state" bson:"state"`
	PassengerNumber    uint16 `xml:"PassengerNumber,omitempty" json:"passenger_number,omitempty" bson:"passenger_number,omitempty"`
	TicketIndicator    string `xml:"TicketIndicator,omitempty" json:"ticket_indicator,omitempty" bson:"ticket_indicator,omitempty"`
	TicketNumber       string `xml:"TicketNumber,omitempty" json:"ticket_number,omitempty" bson:"ticket_number,omitempty"`
	TicketStatus       string `xml:"TicketStatus,omitempty" json:"ticket_status,omitempty" bson:"ticket_status,omitempty"`
	InfantTicketNumber string `xml:"InfantTicketNumber,omitempty" json:"infant_ticket_number,omitempty" bson:"infant_ticket_number,omitempty"`
}

type paxTicketsODT struct {
	PaxTicket paxTicketODT `xml:"PaxTicket,omitempty" json:"pax_ticket,omitempty" bson:"pax_ticket,omitempty"`
}

type boardingPassDetailODT struct {
	GateInformation     string `xml:"GateInformation,omitempty" json:"gate_information,omitempty" bson:"gate_information,omitempty"`
	PriorityInformation string `xml:"PriorityInformation,omitempty" json:"priority_information,omitempty" bson:"priority_information,omitempty"`
	CabinClass          string `xml:"CabinClass,omitempty" json:"cabin_class,omitempty" bson:"cabin_class,omitempty"`
	CompartmentLevel    string `xml:"CompartmentLevel,omitempty" json:"compartment_level,omitempty" bson:"compartment_level,omitempty"`
	BoardingZone        string `xml:"BoardingZone,omitempty" json:"boarding_zone,omitempty" bson:"boarding_zone,omitempty"`
	SeatAssignment      string `xml:"SeatAssignment,omitempty" json:"seat_assignment,omitempty" bson:"seat_assignment,omitempty"`
	SequenceNumber      string `xml:"SequenceNumber,omitempty" json:"sequence_number,omitempty" bson:"sequence_number,omitempty"`
}

type paxSegmentODT struct {
	State            string `xml:"State" json:"state" bson:"state"`
	BoardingSequence string `xml:"BoardingSequence,omitempty" json:"boarding_sequence,omitempty" bson:"boarding_sequence,omitempty"`
	CreatedDate      string `xml:"CreatedDate,omitempty" json:"created_date,omitempty" bson:"created_date,omitempty"`
	LiftStatus       string `xml:"LiftStatus,omitempty" json:"lift_status,omitempty" bson:"lift_status,omitempty"` /*
	 Default CheckedIn Boarded NoShow Unmapped */
	OverBookIndicator string `xml:"OverBookIndicator,omitempty" json:"over_book_indicator,omitempty" bson:"over_book_indikator,omitempty"`
	PassengerNumber   uint16 `xml:"PassengerNumber,omitempty" json:"passenger_number,omitempty" bson:"passenger_number,omitempty"`
	PrioPriorityDate  string `xml:"PriorityDate,omitempty" json:"priority_date,omitempty" bson:"priority_date,omitempty"`
	TripType          string `xml:"TripType,omitempty" json:"trip_type,omitempty" bson:"trip_type,omitempty"` /*
		None OneWay RoundTrip HalfRound OpenJaw CircleTrip All Unmapped */
	TimeChanged                bool           `xml:"TimeChanged,omitempty" json:"time_changed,omitempty" bson:"time_changed,omitempty"`
	POS                        pointOfSaleODT `xml:"POS,omitempty" json:"pos,omitempty" bson:"pos,omitempty"`
	SourcePOS                  pointOfSaleODT `xml:"SourcePOS,omitempty" json:"source_pos,omitempty" bson:"source_pos,omitempty"`
	VerifiedTravelDocs         string         `xml:"VerifiedTravelDocs,omitempty" json:"verified_travel_docs,omitempty" bson:"verified_travel_docs,omitempty"`
	ModifiedDate               string         `xml:"ModifiedDate,omitempty" json:"modified_date,omitempty" bson:"modified_date,omitempty"`
	ActivityDate               string         `xml:"ActivityDate,omitempty" json:"activity_date,omitempty" bson:"activity_date,omitempty"`
	BaggageAllowanceWeight     uint8          `xml:"BaggageAllowanceWeight,omitempty" json:"baggage_allowance_weight,omitempty" bson:"baggage_allowance_weight,omitempty"`
	BaggageAllowanceWeightType string         `xml:"BaggageAllowanceWeightType" json:"baggage_allowance_weight_type,omitempty" bson:"baggage_allowance_weight_type,omitempty"` /*
		Default Pounds Kilograms Unmapped */
	BaggageAllowanceUsed bool                  `xml:"BaggageAllowanceUsed,omitempty" json:"baggage_allowance_used,omitempty" bson:"baggage_allowance_used,omitempty"`
	ReferenceNumber      uint64                `xml:"ReferenceNumber,omitempty" json:"reference_number,omitempty" bson:"reference_number,omitempty"`
	BoardingPassDetail   boardingPassDetailODT `xml:"BoardingPassDetail,omitempty" json:"boarding_pass_detail,omitempty" bson:"boarding_pass_detail,omitempty"`
}

type paxSegmentsODT struct {
	PaxSegment []paxSegmentODT `xml:"PaxSegment,omitempty" json:"pax_segment,omitempty" bson:"pax_segment,omitempty"`
}

type keyValuePairOfstringstringsODT struct {
	KeyValuePairOfstringstring string `xml:"KeyValuePairOfstringstring,omitempty" json:"key_value_pair_of_string_string,omitempty" bson:"key_value_pair_of_string_string,omitempty"`
}

type paxSeatInfoODT struct {
	SeatSet    uint16                         `xml:"SeatSet,omitempty" json:"seat_set,omitempty" bson:"seat_set,omitempty"`
	Deack      uint16                         `xml:"Deck,omitempty" json:"deck,omitempty" bson:"deck,omitempty"`
	Properties keyValuePairOfstringstringsODT `xml:"Properties,omitempty" json:"properties,omitempty" bson:"properties,omitempty"`
}

type paxSeatsODT struct {
	State                 string `xml:"State" json:"state" bson:"state"`
	PassengerNumber       uint16 `xml:"PassengerNumber,omitempty" json:"passenger_number,omitempty" bson:"passenger_number,omitempty"`
	ArrivalStation        string `xml:"ArrivalStation,omitempty" json:"arrival_station,omitempty" bson:"arrival_station,omitempty"`
	DepartureStation      string `xml:"DepartureStation,omitempty" json:"departure_station,omitempty" bson:"departure_station,omitempty"`
	UnitDesignator        string `xml:"UnitDesignator,omitempty" json:"unit_designator,omitempty" bson:"unit_designator,omitempty"`
	CompartmentDesignator string `xml:"CompartmentDesignator,omitempty" json:"compartment_designator,omitempty" bson:"compartment_designator,omitempty"`
	SeatPreference        string `xml:"SeatPreference,omitempty" json:"seat_preference,omitempty" bson:"seat_preference,omitempty"` /*
	 None Window Aisle NoPreference Front Rear WindowFront
	 WindowRear AisleFront AisleRear Unmapped */
	Penalty                int16          `xml:"Penalty,omitempty" json:"penalty,omitempty" bson:"penalty,omitempty"`
	SeatTogetherPreference bool           `xml:"SeatTogetherPreference,omitempty" json:"seat_together_preference,omitempty" bson:"seat_together_preference,omitempty"`
	PaxSeatInfo            paxSeatInfoODT `xml:"PaxSeatInfo,omitempty" json:"pax_seat_info,omitempty" bson:"pax_seat_info,omitempty"`
}

type segmentODT struct {
	State                string                `xml:"State" json:"state" bson:"state"`
	ActionStatusCode     string                `xml:"ActionStatusCode" json:"action_status_code" bson:"action_status_code"`
	ArrivalStation       string                `xml:"ArrivalStation" json:"arrival_code" bson:"arrival_code"`
	CabinOfService       string                `xml:"CabinOfService" json:"cabin_of_service" bson:"cabin_of_service"`
	ChangeReasonCode     string                `xml:"ChangeReasonCode" json:"change_reason_code" bson:"change_reason_code"`
	DepartureStation     string                `xml:"DepartureStation" json:"departure_code" bson:"departure_code"`
	PriorityCode         string                `xml:"PriorityCode" json:"priority_code" bson:"priority_code"`
	SegmentType          string                `xml:"SegmentType" json:"segment_type" bson:"segment_type"`
	STA                  string                `xml:"STA" json:"sta" bson:"sta"` // yyyy-mm-dd'T'H24:min:ss
	STD                  string                `xml:"STD" json:"std" bson:"std"` // yyyy-mm-dd'T'H24:min:ss
	International        bool                  `xml:"International" json:"international" bson:"international"`
	FlightDesignator     flightDesignatorODT   `xml:"FlightDesignator" json:"flight_designator" bson:"flight_designator"`
	XrefFlightDesignator string                `xml:"XrefFlightDesignator" json:"xref_flight_designator" bson:"xref_flight_designator"`
	Fares                faresODT              `xml:"Fares" json:"fares" bson:"fares"`
	Legs                 legsODT               `xml:"Legs" json:"legs" bson:"legs"`
	PaxBags              paxBagsODT            `xml:"PaxBags" json:"pax_bags" bson:"pax_bags"`
	PaxSeats             paxSeatsODT           `xml:"PaxSeats,omitempty" json:"pax_seats,omitempty" bson:"pax_seats,omitempty"`
	PaxSSRs              paxSSRsODT            `xml:"PaxSSRs,omitempty" json:"pax_ssrs,omitempty" bson:"pax_ssrs,omitempty"`
	PaxSegments          paxSegmentsODT        `xml:"PaxSegments,omitempty" json:"pax_segments,omitempty" bson:"pax_segments,omitempty"`
	PaxTickets           paxTicketsODT         `xml:"PaxTickets,omitempty" json:"pax_tickets,omitempty" bson:"pax_tickets,omitempty"`
	PaxSeatPreferences   paxSeatPreferencesODT `xml:"PaxSeatPreferences,omitempty" json:"pax_seat_preferences,omitempty" bson:"pax_seat_preferences,omitempty"`
	SalesDate            string                `xml:"SalesDate,omitempty" json:"sales_date,omitempty" bson:"sales_date,omitempty"`
	SegmentSellKey       string                `xml:"SegmentSellKey,omitempty" json:"segment_sell_key,omitempty" bson:"segment_sell_key,omitempty"`
	PaxScores            paxScoresODT          `xml:"PaxScores,omitempty" json:"pax_scores,omitempty" bson:"pax_scores,omitempty"`
	ChannelType          string                `xml:"ChannelType,omitempty" json:"channel_type,omitempty" bson:"channel_type,omitempty"` /*
		Default Direct Web GDS API Unmapped */
}

type segmentsODT struct {
	Segment []segmentODT `xml:"Segment" json:"segment" bson:"segment"`
}

type journeyODT struct {
	State            string      `xml:"State" json:"State" bson:"State"`
	NotForGeneralUse bool        `xml:"NotForGeneralUse" json:"not_for_general_use" bson:"not_for_general_use"`
	Segments         segmentsODT `xml:"Segments" json:"segments" bson:"segments"`
	JourneySellKey   string      `xml:"JourneySellKey" json:"journey_sell_key" bson:"journey_sell_key"`
}

type journeysODT struct {
	Journey []journeyODT `xml:"Journey" json:"journey" bson:"journey"`
}

type journeyDateMarketODT struct {
	Signature           string      `xml:"-"`
	DepartureDate       string      `xml:"DepartureDate" json:"departure_date" bson:"departure_date"` //yyyy-mm-dd'T00:00:00'
	DepartureStation    string      `xml:"DepartureStation" json:"departure_station" bson:"departure_station"`
	ArrivalStation      string      `xml:"ArrivalStation" json:"arrival_station" bson:"arrival_station"`
	Journeys            journeysODT `xml:"Journeys" json:"journeys" bson:"journeys"`
	IncludeTaxesAndFees bool        `xml:"IncludeTaxesAndFees" json:"include_taxes_and_fees" bson:"include_taxes_and_fees"`
}

type arrayOfJourneyDateMarketODT struct {
	JourneyDateMarket []journeyDateMarketODT `xml:"JourneyDateMarket" json:"journey_date_market" bson:"journey_date_market"`
}

type scheduleODT struct {
	ArrayOfJourneyDateMarket []arrayOfJourneyDateMarketODT `xml:"ArrayOfJourneyDateMarket" json:"array_of_journey_date_market" bson:"array_of_journey_date_market"`
}
type getTripAvailabilityResponseODT struct {
	OtherServiceInfoList string      `xml:"OtherServiceInfoList" json:"other_service_info_list" bson:"other_service_info_list"`
	Schedules            scheduleODT `xml:"Schedules" json:"schedules" bson:"schedules"`
}

type getAvailabilityByTripResponseODT struct {
	GetTripAvailabilityResponse getTripAvailabilityResponseODT `xml:"GetTripAvailabilityResponse" json:"trips" bson:"trips"`
}

type availabilityResponseBodyODT struct {
	Fault faultODT                         `xml:"Fault,omitempty" json:"fault,omitempty" bson:"fault,omitempty"`
	Data  getAvailabilityByTripResponseODT `xml:"GetAvailabilityByTripResponse" json:"data" bson:"data"`
}

type availabilityResponseEnvelopeODT struct {
	XMLName xml.Name                    `xml:"Envelope"`
	Header  string                      `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    availabilityResponseBodyODT `xml:"Body" json:"body" bson:"body"`
}

type journeySellKeyODT struct {
	JourneySellKey      string `xml:"JourneySellKey" json:"journey_sell_key" bson:"journey_sell_key"`
	FareSellKey         string `xml:"FareSellKey" json:"fare_sell_key" bson:"fare_sell_key"`
	StandbyPriorityCode string `xml:"StandbyPriorityCode,omitempty" json:"standby_priority_code,omitempty" bson:"standby_priority_code,omitempty"`
}

type journeSellKeysODT struct {
	SellKeyList []journeySellKeyODT `xml:"SellKeyList" json:"sell_key_list" bson:"sell_key_list"`
}

type paxSSRODT struct {
	State            string `xml:"State,omitempty" json:"state_omitempty" bson:"state_omitempty"`
	ActionStatusCode string `xml:"ActionStatusCode,omitempty" json:"action_status_code,omitempty" bson:"action_status_code,omitempty"` //NN
	ArrivalStation   string `xml:"ArrivalStation,omitempty" json:"arrival_code,omitempty" bson:"arrival_code,omitempty"`
	DepartureStation string `xml:"DepartureStation,omitempty" json:"departure_code,omitempty" bson:"departure_code,omitempty"`
	PassengerNumber  uint8  `xml:"PassengerNumber,omitempty" json:"passenger_number,omitempty" bson:"passenger_number,omitempty"`
	SSRNumber        uint8  `xml:"SSRNumber,omitempty" json:"ssr_number,omitempty" bson:"ssr_number,omitempty"`
	SSRCode          string `xml:"SSRCode,omitempty" json:"ssr_code,omitempty" bson:"ssr_code,omitempty"` //INF
	FeeCode          string `xml:"FeeCode,omitempty" json:"fee_code,omitempty" bson:"fee_code,omitempty"`
	Note             string `xml:"Note,omitempty" json:"note,omitempty" bson:"note,omitempty"`
	SSRValue         string `xml:"SSRValue,omitempty" json:"ssr_value,omitempty" bson:"ssr_value,omitempty"`
}

type paxSSRsODT struct {
	PaxSSR []paxSSRODT `xml:"PaxSSR,omitempty" json:"pax_ssr,omitempty" bson:"pax_ssr,omitempty"`
}

type segmentSSRRequestODT struct {
	FlightDesignator flightDesignatorODT `xml:"FlightDesignator,omitempty" json:"flight_designator,omitempty" bson:"flight_designator,omitempty"`
	STD              string              `xml:"STD,omitempty" json:"std,omitempty" bson:"std,omitempty"` //yyyy-mm-dd'T'hh24:min:ss
	DepartureStation string              `xml:"DepartureStation,omitempty" json:"departure_code,omitempty" bson:"departure_code,omitempty"`
	ArrivalStation   string              `xml:"ArrivalStation,omitempty" json:"arrival_code,omitempty" bson:"arrival_code,omitempty"`
	PaxSSRs          paxSSRsODT          `xml:"PaxSSRs,omitempty" json:"pax_srrs,omitempty" bson:"pax_srrs,omitempty"`
}

type segmentSSRRequestsODT struct {
	SegmentSSRRequest []segmentSSRRequestODT `xml:"SegmentSSRRequest,omitempty" json:"segment_ssr_request,omitempty" bson:"segment_ssr_request,omitempty"`
}

type ssrRequestODT struct {
	SegmentSSRRequests     segmentSSRRequestsODT `xml:"SegmentSSRRequests,omitempty" json:"segment_ssr_requests,omitempty" bson:"segment_ssr_requests,omitempty"`
	CurrencyCode           string                `xml:"CurrencyCode,omitempty" json:"currency,omitempty" bson:"currency,omitempty"`
	CancelFirstSSR         bool                  `xml:"CancelFirstSSR,omitempty" json:"cancel_first_ssr,omitempty" bson:"cancel_first_ssr,omitempty"`                               //false
	SSRFeeForceWaiveOnSell bool                  `xml:"SSRFeeForceWaiveOnSell,omitempty" json:"ssr_fee_force_waive_on_sell,omitempty" bson:"ssr_fee_force_waive_on_sell,omitempty"` //false
}

type sellSSRODT struct {
	SSRRequest ssrRequestODT `xml:"SSRRequest,omitempty" json:"ssr_request,omitempty" bson:"ssr_request,omitempty"`
}

type pointOfSaleODT struct {
	State            string `xml:"State,omitempty" json:"state,omitempty" bson:"state,omitempty"`
	AgencyCode       string `xml:"AgencyCode,omitempty" json:"agency_code,omitempty" bson:"agency_code,omitempty"`
	OrganizationCode string `xml:"OrganizationCode,omitempty" json:"organization_code,omitempty" bson:"organization_code,omitempty"`
	DomainCode       string `xml:"DomainCode,omitempty" json:"domain_code,omitempty" bson:"domain_code,omitempty"`
	LocationCode     string `xml:"LocationCode,omitempty" json:"location_code,omitempty" bson:"location_code,omitempty"`
}

type sellJourneyByKeyRequestDataODT struct {
	ActionStatusCode      string            `xml:"ActionStatusCode,omitempty" json:"action_status_code,omitempty" bson:"action_status_code,omitempty"` //NN
	JourneySellKeys       journeSellKeysODT `xml:"JourneySellKeys,omitempty" json:"journey_sell_keys,omitempty" bson:"journey_sell_keys,omitempty"`
	PaxPriceType          paxPriceTypesODT  `xml:"PaxPriceType,omitempty" json:"pax_price_type,omitempty" bson:"pax_price_type,omitempty"`
	CurrencyCode          string            `xml:"CurrencyCode,omitempty" json:"currency,omitempty" bson:"currency,omitempty"`
	SourcePOS             pointOfSaleODT    `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common SourcePOS,omitempty" json:"source_pos,omitempty" bson:"source_pos,omitempty"`
	PaxCount              uint16            `xml:"PaxCount,omitempty" json:"pax_count,omitempty" bson:"pax_count,omitempty"`
	TypeOfSale            string            `xml:"TypeOfSale,omitempty" json:"type_of_sale,omitempty" bson:"type_of_sale,omitempty"`
	LoyaltyFilter         string            `xml:"LoyaltyFilter,omitempty" json:"loyalty_filter,omitempty" bson:"loyalty_filter,omitempty"` // MonetaryOnly
	IsAllotmentMarketFare bool              `xml:"IsAllotmentMarketFare,omitempty" json:"is_allotment_market_fare,omitempty" bson:"is_allotment_market_fare,omitempty"`
	SourceBookingPOS      string            `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common SourceBookingPOS,omitempty" json:"source_booking_pos,omitempty" bson:"source_booking_pos,omitempty"`
	PreventOverLap        bool              `xml:"PreventOverLap,omitempty" json:"prevent_overlap,omitempty" bson:"prevent_overlap,omitempty"` //false
}

type sellJourneyByKeyRequestODT struct {
	SellJourneyByKeyRequestData sellJourneyByKeyRequestDataODT `xml:"SellJourneyByKeyRequestData" json:"sell_journey_by_key_request_data" bson:"sell_journey_by_key_request_data"`
}

type sellRequestDetailODT struct {
	SellBy                  string                     `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking SellBy" json:"sell_by" bson:"sell_by"` // JourneyBySellKey
	SellJourneyByKeyRequest sellJourneyByKeyRequestODT `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking SellJourneyByKeyRequest,omitempty" json:"sell_journey_by_key_request,omitempty" bson:"sell_journey_by_key_request,omitempty"`
	SellJourneyRequest      string                     `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking SellJourneyRequest,omitempty" json:"sell_journey_request,omitempty" bson:"sell_journey_request,omitempty"`
	SellSSR                 sellSSRODT                 `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking SellSSR,omitempty" json:"sell_ssr,omitempty" bson:"sell_ssr,omitempty"`
	SellFee                 string                     `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking SellFee,omitempty" json:"sell_fee,omitempty" bson:"sell_fee,omitempty"`
}
type sellRequestODT struct {
	SellRequestDetail sellRequestDetailODT `xml:"http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService SellRequestData" json:"sell_request_detail" bson:"sell_request_detail"`
}

type sellRequestBodyODT struct {
	Data sellRequestODT `xml:"http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService SellRequest" json:"data" bson:"data"`
}
type sellRequestEnvelopeODT struct {
	XMLName xml.Name           `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	Header  requestHeaderODT   `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    sellRequestBodyODT `xml:"Body" json:"body" bson:"body"`
}

type sellResponseODT struct {
	BookingUpdateResponseData bookingUpdateResponseDataODT `xml:"BookingUpdateResponseData" json:"booking_update_response_data" bson:"booking_update_response_data"`
}

type sellResponseBodyODT struct {
	Fault faultODT        `xml:"Fault,omitempty" json:"fault,omitempty" bson:"fault,omitempty"`
	Data  sellResponseODT `xml:"SellResponse" json:"data" bson:"data"`
}
type sellResponseEnvelopeODT struct {
	XMLName xml.Name            `xml:"Envelope"`
	Header  string              `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    sellResponseBodyODT `xml:"Body" json:"body" bson:"body"`
}

type bookingNameODT struct {
	State      string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	FirstName  string `xml:"FirstName,omitempty" json:"first_name,omitempty" bson:"first_name,omitempty"`
	MiddleName string `xml:"MiddleName,omitempty" json:"middle_name,omitempty" bson:"middle_name,omitempty"`
	LastName   string `xml:"LastName,omitempty" json:"last_name,omitempty" bson:"last_name,omitempty"`
	Suffix     string `xml:"Suffix,omitempty" json:"suffix,omitempty" bson:"suffix,omitempty"`
	Title      string `xml:"Title,omitempty" json:"salutation,omitempty" bson:"salutation,omitempty"` //MR
}

type bookingNamesODT struct {
	BookingName []bookingNameODT `xml:"BookingName,omitempty" json:"booking_name,omitempty" bson:"booking_name,omitempty"`
}

type bookingContactODT struct {
	State                  string          `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	TypeCode               string          `xml:"TypeCode,omitempty" json:"type_code,omitempty" bson:"type_code,omitempty"`                                                    //P
	Names                  bookingNamesODT `xml:"Names,omitempty" json:"names,omitempty" bson:"names,omitempty"`
	Email                  string          `xml:"EmailAddress,omitempty" json:"email,omitempty" bson:"email,omitempty"`
	HomePhone              string          `xml:"HomePhone,omitempty" json:"home_phone,omitempty" bson:"home_phone,omitempty"`
	OtherPhone             string          `xml:"OtherPhone,omitempty" json:"other_phone,omitempty" bson:"other_phone,omitempty"`
	Fax                    string          `xml:"Fax,omitempty" json:"fax,omitempty" bson:"fax,omitempty"`
	CompanyName            string          `xml:"CompanyName,omitempty" json:"company_name,omitempty" bson:"company_name,omitempty"`
	AddressLine1           string          `xml:"AddressLine1,omitempty" json:"address_line_1,omitempty" bson:"address_line_1,omitempty"`
	AddressLine2           string          `xml:"AddressLine2,omitempty" json:"address_line_2,omitempty" bson:"address_line_2,omitempty"`
	AddressLine3           string          `xml:"AddressLine3,omitempty" json:"address_line_3,omitempty" bson:"address_line_3,omitempty"`
	City                   string          `xml:"City,omitempty" json:"city,omitempty" bson:"city,omitempty"`
	ProvinceState          string          `xml:"ProvinceState,omitempty" json:"province_state,omitempty" bson:"province_state,omitempty"`
	PostalCode             string          `xml:"PostalCode,omitempty" json:"postal_code,omitempty" bson:"postal_code,omitempty"`
	CountryCode            string          `xml:"CountryCode,omitempty" json:"country_code,omitempty" bson:"country_code,omitempty"`
	CultureCode            string          `xml:"CultureCode,omitempty" json:"culture_code,omitempty" bson:"culture_code,omitempty"`
	DistributionOption     string          `xml:"DistributionOption,omitempty" json:"distribution_option,omitempty" bson:"distribution_option,omitempty"` //None
	CustomerNumber         string          `xml:"CustomerNumber,omitempty" json:"customer_number,omitempty" bson:"customer_number,omitempty"`
	NotificationPreference string          `xml:"NotificationPreference,omitempty" json:"notification_preference,omitempty" bson:"notification_preference,omitempty"` //None
	SourceOrganization     string          `xml:"SourceOrganization,omitempty" json:"source_organization,omitempty" bson:"source_organization,omitempty"`
}

type bookingContactListODT struct {
	BookingContact []bookingContactODT `xml:"BookingContact,omitempty" json:"booking_contact,omitempty" bson:"booking_contact,omitempty"`
}

type updateContactsRequestDataODT struct {
	BookingContactList bookingContactListODT `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking BookingContactList,omitempty" json:"booking_contact_list,omitempty" bson:"booking_contact_list,omitempty"`
}

type updateContactsRequestODT struct {
	UpdateContactsRequestData updateContactsRequestDataODT `xml:"updateContactsRequestData,omitempty" json:"update_contacts_request_data,omitempty" bson:"update_contacts_request_data,omitempty"`
}

type updateContactRequestBodyODT struct {
	Data updateContactsRequestODT `xml:"http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService UpdateContactsRequest" json:"data" bson:"data"`
}

type updateContactRequestEnvelopeODT struct {
	XMLName xml.Name                    `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	Header  requestHeaderODT            `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    updateContactRequestBodyODT `xml:"Body" json:"body" bson:"body"`
}

type updateContactResponseODT struct {
	BookingUpdateResponseData bookingUpdateResponseDataODT `xml:"BookingUpdateResponseData,omitempty" json:"booking_update_response_data,omitempty" bson:"booking_update_response_data,omitempty"`
}

type updateContactResponseBody struct {
	Fault faultODT        `xml:"Fault,omitempty" json:"fault,omitempty" bson:"fault,omitempty"`
	Data  sellResponseODT `xml:"UpdateContactsResponse,omitempty" json:"data" bson:"data"`
}
type updateContactResponseEnvelopeODT struct {
	XMLName xml.Name                  `xml:"Envelope"`
	Header  string                    `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    updateContactResponseBody `xml:"Body,omitempty" json:"body" bson:"body"`
}

// passenger data
type passengerFeeODT struct {
	State            string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	ActionStatusCode string `xml:"ActionStatusCode,omitempty" json:"action_status_code,omitempty" bson:"action_status_code,omitempty"`
	FeeCode          string `xml:"FeeCode,omitempty" json:"fee_code,omitempty" bson:"fee_code,omitempty"`
	FeeDetail        string `xml:"FeeDetail,omitempty" json:"fee_detail,omitempty" bson:"fee_detail,omitempty"`
	FeeNumber        uint16 `xml:"FeeNumber,omitempty" json:"fee_number,omitempty" bson:"fee_number,omitempty"`
	FeeType          string `xml:"FeeType,omitempty" json:"fee_type,omitempty" bson:"fee_type,omitempty"` //All Tax TravelFee
	// ServiceFee PaymentFee PenaltyFee SSRFee NonFlightServiceFee UpgradeFee SeatFee
	// BaseFare SpoilageFee NameChangeFee ConvenienceFee BaggageFee FareSurcharge
	// PromotionDiscount Unmapped
	FeeOverride     bool              `xml:"FeeOverride,omitempty" json:"fee_override,omitempty" bson:"fee_override,omitempty"`
	FlightReference string            `xml:"FlightReference,omitempty" json:"flight_reference,omitempty" bson:"flight_reference,omitempty"`
	Note            string            `xml:"Note,omitempty" json:"note,omitempty" bson:"note,omitempty"`
	SSRCode         string            `xml:"SSRCode,omitempty" json:"ssr_code,omitempty" bson:"ssr_code,omitempty"`
	SSRNumber       uint16            `xml:"SSRNumber,omitempty" json:"ssr_number,omitempty" bson:"ssr_number,omitempty"`
	PaymentNumber   uint16            `xml:"PaymentNumber,omitempty" json:"payment_number,omitempty" bson:"payment_number,omitempty"`
	ServiceCharges  serviceChargesODT `xml:"ServiceCharges,omitempty" json:"service_charges,omitempty" bson:"service_charges,omitempty"`
	CreatedDate     string            `xml:"CreatedDate,omitempty" json:"create_date,omitempty" bson:"create_date,omitempty"`
	IsProtected     bool              `xml:"IsProtected,omitempty" json:"is_protected,omitempty" bson:"is_protected,omitempty"`
}

type passengerFeesODT struct {
	PassengerFee []passengerFeeODT `xml:"PassengerFee,omitempty" json:"passenger_fee,omitempty" bson:"passenger_fee,omitempty"`
}

type passengerAddressODT struct {
	State         string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	TypeCode      string `xml:"TypeCode,omitempty" json:"type_code,omitempty" bson:"type_code,omitempty"`
	StationCode   string `xml:"StationCode,omitempty" json:"station_code,omitempty" bson:"station_code,omitempty"`
	CompanyName   string `xml:"CompanyName,omitempty" json:"company_name,omitempty" bson:"company_name,omitempty"`
	AddressLine1  string `xml:"AddressLine1,omitempty" json:"address_line_1,omitempty" bson:"address_line_1,omitempty"`
	AddressLine2  string `xml:"AddressLine2,omitempty" json:"address_line_2,omitempty" bson:"address_line_2,omitempty"`
	AddressLine3  string `xml:"AddressLine3,omitempty" json:"address_line_3,omitempty" bson:"address_line_3,omitempty"`
	City          string `xml:"City,omitempty" json:"city,omitempty" bson:"city,omitempty"`
	ProvinceState string `xml:"ProvinceState,omitempty" json:"province_state,omitempty" bson:"province_state,omitempty"`
	PostalCode    string `xml:"PostalCode,omitempty" json:"postal_code,omitempty" bson:"postal_code,omitempty"`
	CountryCode   string `xml:"CountryCode,omitempty" json:"country_code,omitempty" bson:"country_code,omitempty"`
	Phone         string `xml:"Phone,omitempty" json:"phone,omitempty" bson:"phone,omitempty"`
}

type passengerAddressesODT struct {
	PassengerAddress []passengerAddressODT `xml:"PassengerAddress,omitempty" json:"passenger_addrss,omitempty" bson:"passenger_addrss,omitempty"`
}

type passengerTravelDocumentODT struct {
	State          string          `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	DocTypeCode    string          `xml:"DocTypeCode,omitempty" json:"doc_type_code,omitempty" bson:"doc_type_code,omitempty"`
	IssuedByCode   string          `xml:"IssuedByCode,omitempty" json:"issued_by_code,omitempty" bson:"issued_by_code,omitempty"`
	DocSuffix      string          `xml:"DocSuffix,omitempty" json:"doc_suffix,omitempty" bson:"doc_suffix,omitempty"`
	DocNumber      string          `xml:"DocNumber,omitempty" json:"doc_number,omitempty" bson:"doc_number,omitempty"`
	BirtDate       string          `xml:"DOB,omitempty" json:"birth_date,omitempty" bson:"birth_date,omitempty"`
	Gender         string          `xml:"Gender,omitempty" json:"gender,omitempty" bson:"gender,omitempty"` //Male Female XX
	Nationality    string          `xml:"Nationality,omitempty" json:"nationality,omitempty" bson:"nationality,omitempty"`
	ExpirationDate string          `xml:"ExpirationDate,omitempty" json:"expiration_date,omitempty" bson:"expiration_date,omitempty"`
	Names          bookingNamesODT `xml:"Names,omitempty" json:"names,omitempty" bson:"names,omitempty"`
	BirthCountry   string          `xml:"BirthCountry,omitempty" json:"birth_country,omitempty" bson:"birth_country,omitempty"`
	IssuedDate     string          `xml:"IssuedDate,omitempty" json:"issued_date,omitempty" bson:"issued_date,omitempty"`
}

type passengerTravelDocumentsODT struct {
	PassengerTravelDocument []passengerTravelDocumentODT `xml:"PassengerTravelDocument,omitempty" json:"passenger_travel_document,omitempty" bson:"passenger_travel_document,omitempty"`
}

type passengerBagODT struct {
	State                string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	BaggageID            uint64 `xml:"BaggageID,omitempty" json:"baggage_id,omitempty" bson:"baggage_id,omitempty"`
	OSTag                string `xml:"OSTag,omitempty" json:"os_tag,omitempty" bson:"os_tag,omitempty"`
	OSTagDate            string `xml:"OSTagDate,omitempty" json:"os_tag_date,omitempty" bson:"os_tag_date,omitempty"`
	StationCode          string `xml:"StationCode,omitempty" json:"station_code,omitempty" bson:"station_code,omitempty"`
	Weight               uint16 `xml:"Weight,omitempty" json:"weight,omitempty" bson:"weight,omitempty"`
	WeightType           string `xml:"WeightType,omitempty" json:"weight_type,omitempty" bson:"weight_type,omitempty"` // Default Pounds Kilogram Unmapped
	TaggedToStation      string `xml:"TaggedToStation,omitempty" json:"tagged_to_station,omitempty" bson:"tagged_to_station,omitempty"`
	TaggedToFlightNumber string `xml:"TaggedToFlightNumber,omitempty" json:"tagged_to_flight_number,omitempty" bson:"tagged_to_flight_number,omitempty"`
	LRTIndicator         bool   `xml:"LRTIndicator,omitempty" json:"lrt_indicator,omitempty" bson:"lrt_indicator,omitempty"`
	BaggageType          string `xml:"BaggageType,omitempty" json:"baggage_type,omitempty" bson:"baggage_type,omitempty"`
	TaggedToCarrierCode  string `xml:"TaggedToCarrierCode,omitempty" json:"tagged_carrier_code,omitempty" bson:"tagged_carrier_code,omitempty"`
}

type passengerBagsODT struct {
	PassengerBag []passengerBagODT `xml:"PassengerBag,omitempty" json:"passenger_bag,omitempty" bson:"passenger_bag,omitempty"`
}

type passengerTypeInfoODT struct {
	State     string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	BirthDate string `xml:"DOB,omitempty" json:"birth_date,omitempty" bson:"birth_date,omitempty"`
	PaxType   string `xml:"PaxType,omitempty" json:"pax_type,omitempty" bson:"pax_type,omitempty"`
}

type passengerInfosODT struct {
	PassengerInfo []passengerInfoODT `xml:"PassengerInfo,omitempty" json:"passenger_info,omitempty" bson:"passenger_info,omitempty"`
}

type passengerTypeInfosODT struct {
	PassengerTypeInfo []passengerTypeInfoODT `xml:"PassengerTypeInfo,omitempty" json:"passenger_type_info,omitempty" bson:"passenger_type_info,omitempty"`
}

type passengerInfantODT struct {
	State           string          `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	BirthDate       string          `xml:"DOB,omitempty" json:"birth_date,omitempty" bson:"birth_date,omitempty"`
	Gender          string          `xml:"Gender,omitempty" json:"gender,omitempty" bson:"gender,omitempty"` //Male Female XX Unmapped
	Nationality     string          `xml:"Nationality,omitempty" json:"nationality,omitempty" bson:"nationality,omitempty"`
	ResidentCountry string          `xml:"ResidentCountry,omitempty" json:"resident_country,omitempty" bson:"resident_country,omitempty"`
	Names           bookingNamesODT `xml:"Names,omitempty" json:"names,omitempty" bson:"names,omitempty"`
}

type passengerInfantsODT struct {
	PassengerInfant []passengerInfantODT `xml:"PassengerInfo,omitempty" json:"passenger_info,omitempty" bson:"passenger_info,omitempty"`
}

type passengerInfoODT struct {
	State           string        `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	BalanceDue      prettyFloat64 `xml:"BalanceDue,omitempty" json:"balance_due,omitempty" bson:"balance_due,omitempty"`
	Gender          string        `xml:"Gender,omitempty" json:"gender,omitempty" bson:"gender,omitempty"` //Male Female XX
	Nationality     string        `xml:"Nationality,omitempty" json:"nationality,omitempty" bson:"nationality,omitempty"`
	ResidentCountry string        `xml:"ResidentCountry,omitempty" json:"resindent_country,omitempty" bson:"resindent_country,omitempty"`
	TotalCost       prettyFloat64 `xml:"TotalCost,omitempty" json:"total_cost,omitempty" bson:"total_cost,omitempty"`
	WeightCategory  string        `xml:"WeightCategory,omitempty" json:"weight_category,omitempty" bson:"weight_category,omitempty"` // Male Female Child Unmapped
}

type passengerODT struct {
	State                    string                      `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	PassengerPrograms        passengerProgramsODT        `xml:"PassengerPrograms,omitempty" json:"passenger_programs,omitempty" bson:"passenger_programs,omitempty"`
	CustomerNumber           string                      `xml:"CustomerNumber,omitempty" json:"customer_number,omitempty" bson:"customer_number,omitempty"`
	PassengerNumber          uint8                       `xml:"PassengerNumber,omitempty" json:"passenger_number,omitempty" bson:"passenger_number,omitempty"`
	FamilyNumber             uint8                       `xml:"FamilyNumber,omitempty" json:"FamilyNumber,omitempty" bson:"FamilyNumber,omitempty"`
	PaxDiscountCode          string                      `xml:"PaxDiscountCode,omitempty" json:"pax_discount_coude,omitempty" bson:"pax_discount_coude,omitempty"`
	Names                    bookingNamesODT             `xml:"Names,omitempty" json:"names,omitempty" bson:"names,omitempty"`
	Infant                   *passengerInfantODT         `xml:"Infant,omitempty" json:"infant,omitempty" bson:"infant,omitempty"`
	PassengerInfo            passengerInfoODT            `xml:"PassengerInfo,omitempty" json:"passenger_info,omitempty" bson:"passenger_info,omitempty"`
	PassengerProgram         passengerProgramODT         `xml:"PassengerProgram,omitempty" json:"passenger_program,omitempty" bson:"passenger_program,omitempty"`
	PassengerFees            passengerFeesODT            `xml:"PassengerFees,omitempty" json:"passenger_fees,omitempty" bson:"passenger_fees,omitempty"`
	PassengerAddresses       passengerAddressesODT       `xml:"PassengerAddresses,omitempty" json:"passenger_addresses,omitempty" bson:"passenger_addresses,omitempty"`
	PassengerTravelDocuments passengerTravelDocumentsODT `xml:"PassengerTravelDocuments,omitempty" json:"passenger_travel_documents,omitempty" bson:"passenger_travel_documents,omitempty"`
	PassengerBags            passengerBagsODT            `xml:"PassengerBags,omitempty" json:"passenger_bags,omitempty" bson:"passenger_bags,omitempty"`
	PassengerID              uint64                      `xml:"PassengerID,omitempty" json:"passenger_id,omitempty" bson:"passenger_id,omitempty"`
	PassengerTypeInfos       passengerTypeInfosODT       `xml:"PassengerTypeInfos,omitempty" json:"passenger_type_infos,omitempty" bson:"passenger_type_infos,omitempty"`
	PassengerInfos           passengerInfosODT           `xml:"PassengerInfos,omitempty" json:"passenger_infos,omitempty" bson:"passenger_infos,omitempty"`
	PassengerInfants         passengerInfantsODT         `xml:"PassengerInfants,omitempty" json:"passenger_infants,omitempty" bson:"passenger_infants,omitempty"`
	PseudoPassenger          bool                        `xml:"PseudoPassenger,omitempty" json:"pseudo_passenger,omitempty" bson:"pseudo_passenger,omitempty"`
	PassengerTypeInfo        passengerTypeInfoODT        `xml:"PassengerTypeInfo,omitempty" json:"passenger_type_info,omitempty" bson:"passenger_type_info,omitempty"`
}

type passengerProgramODT struct {
	State            string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	ProgramCode      string `xml:"ProgramCode,omitempty" json:"program_code,omitempty" bson:"program_code,omitempty"`
	ProgramLevelCode string `xml:"ProgramLevelCode,omitempty" json:"program_level_code,omitempty" bson:"program_level_code,omitempty"`
	ProgramNumber    string `xml:"ProgramNumber,omitempty" json:"program_number,omitempty" bson:"program_number,omitempty"`
}

type passengerProgramsODT struct {
	PassengerProgram []passengerProgramODT `xml:"PassengerProgram,omitempty" json:"passenger_program,omitempty" bson:"passenger_program,omitempty"`
}

type passengersODT struct {
	Passenger []passengerODT `xml:"Passenger,omitempty" json:"passenger,omitempty" bson:"passenger,omitempty"`
}

type updatePassengersRequestDataODT struct {
	Passengers         passengersODT `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking Passengers" json:"passengers" bson:"passengers"`
	WaiveNameChangeFee bool          `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking WaiveNameChangeFee" json:"waive_name_change_fee" bson:"waive_name_change_fee"` //false
}

type updatePassengersRequestODT struct {
	UpdatePassengersRequestData updatePassengersRequestDataODT `xml:"updatePassengersRequestData" json:"update_passengers_request_data" bson:"update_passengers_request_data"`
}

type updatePassengerRequestBodyODT struct {
	Data updatePassengersRequestODT `xml:"http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService UpdatePassengersRequest" json:"data" bson:"data"`
}
type updatePassengerRequestEnvelopeODT struct {
	XMLName xml.Name                      `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	Header  requestHeaderODT              `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    updatePassengerRequestBodyODT `xml:"Body" json:"body" bson:"body"`
}

type bookingSumODT struct {
	BalanceDue                  prettyFloat64 `xml:"BalanceDue" json:"balance_due" bson:"balance_due"`
	AuthorizedBalanceDue        prettyFloat64 `xml:"AuthorizedBalanceDue" json:"authorized_balance_due" bson:"authorized_balance_due"`
	SegmentCount                uint8         `xml:"SegmentCount" json:"segment_count" bson:"segment_count"`
	PassiveSegmentCount         uint8         `xml:"PassiveSegmentCount" json:"passive_segment_count" bson:"passive_segment_count"`
	TotalCost                   prettyFloat64 `xml:"TotalCost" json:"total_cost" bson:"total_cost"`
	PointsBalanceDue            prettyFloat64 `xml:"PointsBalanceDue" json:"points_balance_due" bson:"points_balance_due"`
	TotalPointCost              prettyFloat64 `xml:"TotalPointCost" json:"total_point_cost" bson:"total_point_cost"`
	AlternateCurrencyCode       string        `xml:"AlternateCurrencyCode,omitempty" json:"alternate_currency_code,omitempty" bson:"alternate_currency_code,omitempty"`
	AlternateCurrencyBalanceDue string        `xml:"AlternateCurrencyBalanceDue,omitempty" json:"alternate_currency_balance_due,omitempty" bson:"alternate_currency_balance_due,omitempty"`
}

type successODT struct {
	RecordLocator string        `xml:"RecordLocator,omitempty" json:"record_locator,omitempty" bson:"record_locator,omitempty"`
	PNRAmount     bookingSumODT `xml:"PNRAmount,omitempty" json:"pnr_amount,omitempty" bson:"pnr_amount,omitempty"`
}

type warningODT struct {
	WarningText string `xml:"WarningText,omitempty" json:"warning_text,omitempty" bson:"warning_text,omitempty"`
}

type errorODT struct {
	ErrorText string `xml:"ErrorText,omitempty" json:"error_text,omitempty" bson:"error_text,omitempty"`
}

type otherServiceInformationODT struct {
	Text        string `xml:"Text,omitempty" json:"text,omitempty" bson:"text,omitempty"`
	OsiSeverity string `xml:"OsiSeverity,omitempty" json:"osi_severity,omitempty" bson:"osi_severity,omitempty"` //General Warning Critical Unmapped
	OSITypeCode string `xml:"OSITypeCode,omitempty" json:"osi_type_code,omitempty" bson:"osi_type_code,omitempty"`
	SubType     string `xml:"SubType,omitempty" json:"sub_type,omitempty" bson:"sub_type,omitempty"`
}

type bookingUpdateResponseDataODT struct {
	Success                  successODT                 `xml:"Success,omitempty" json:"success,omitempty" bson:"success,omitempty"`
	Warning                  warningODT                 `xml:"Warning,omitempty" json:"warning,omitempty" bson:"warning,omitempty"`
	Error                    errorODT                   `xml:"Error,omitempty" json:"error,omitempty" bson:"error,omitempty"`
	OtherServiceInformations otherServiceInformationODT `xml:"OtherServiceInformations,omitempty" json:"other_service_informations,omitempty" bson:"other_service_informations,omitempty"`
}

type updatePassengerResponseODT struct {
	BookingUpdateResponseData bookingUpdateResponseDataODT `xml:"BookingUpdateResponseData,omitempty" json:"booking_update_response_data,omitempty" bson:"booking_update_response_data,omitempty"`
}

type updatePassengerResponseEnvelopeODT struct {
	XMLName xml.Name `xml:"Envelope"`
	Header  string   `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    struct {
		Fault faultODT                   `xml:"Fault,omitempty" json:"fault,omitempty" bson:"fault,omitempty"`
		Data  updatePassengerResponseODT `xml:"UpdatePassengerResponse" json:"data" bson:"data"`
	} `xml:"Body" json:"body" bson:"body"`
}

type bookingHoldODT struct {
	State        string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	HoldDateTime string `xml:"HoldDateTime,omitempty" json:"hold_date_time,omitempty" bson:"hold_date_time,omitempty"`
}

type receivedByInfoODT struct {
	State                   string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	ReceivedBy              string `xml:"ReceivedBy,omitempty" json:"received_by,omitempty" bson:"received_by,omitempty"`
	ReceivedReference       string `xml:"ReceivedReference,omitempty" json:"received_reference,omitempty" bson:"received_reference,omitempty"`
	ReferralCode            string `xml:"ReferralCode,omitempty" json:"referral_code,omitempty" bson:"referral_code,omitempty"`
	LatestReceivedBy        string `xml:"LatestReceivedBy,omitempty" json:"latest_received_by,omitempty" bson:"latest_received_by,omitempty"`
	LatestReceivedReference string `xml:"LatestReceivedReference,omitempty" json:"latest_received_reference,omitempty" bson:"latest_received_reference,omitempty"`
}

type recordLocatorODT struct {
	State              string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	SystemDomainCode   string `xml:"SystemDomainCode,omitempty" json:"system_domain_code,omitempty" bson:"system_domain_code,omitempty"`
	SystemCode         string `xml:"SystemCode,omitempty" json:"system_code,omitempty" bson:"system_code,omitempty"`
	RecordCode         string `xml:"RecordCode,omitempty" json:"record_code,omitempty" bson:"record_code,omitempty"`
	InteractionPurpose string `xml:"InteractionPurpose,omitempty" json:"interaction_purpose,omitempty" bson:"interaction_purpose,omitempty"`
	HostedCarrierCode  string `xml:"HostedCarrierCode,omitempty" json:"hosted_carrier_code,omitempty" bson:"hosted_carrier_code,omitempty"`
}

type recordLocatorsODT struct {
	RecordLocator []recordLocatorODT `xml:"RecordLocator,omitempty" json:"record_locator,omitempty" bson:"record_locator,omitempty"`
}

type bookingCommentODT struct {
	State       string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	CommentType string `xml:"CommentType,omitempty" json:"comment_type,omitempty" bson:"comment_type,omitempty"`                                           // Default Itinerary
	//Manifest Alert Archive Unmapped
	CommentText         string         `xml:"CommentText" json:"comment_text,omitempty" bson:"comment_text,omitempty"`
	PointOfSale         pointOfSaleODT `xml:"PointOfSale,omitempty" json:"point_of_sale,omitempty" bson:"point_of_sale,omitempty"`
	CreatedDate         string         `xml:"CreatedDate,omitempty" json:"create_date,omitempty" bson:"create_date,omitempty"`
	SendToBookingSource bool           `xml:"SendToBookingSource,omitempty" json:"send_to_booking_source,omitempty" bson:"send_to_booking_source,omitempty"`
}

type bookingCommentsODT struct {
	BookingComment []bookingCommentODT `xml:"BookingComment,omitempty" json:"booking_comment,omitempty" bson:"booking_comment,omitempty"`
}

type bookingCommitRequestDataODT struct {
	State                string                `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	RecordLocator        string                `xml:"RecordLocator,omitempty" json:"record_locator,omitempty" bson:"record_locator,omitempty"`
	CurrencyCode         string                `xml:"CurrencyCode,omitempty" json:"currency_code,omitempty" bson:"currency_code,omitempty"`
	PaxCount             uint8                 `xml:"PaxCount,omitempty" json:"pax_count,omitempty" bson:"pax_count,omitempty"`
	SystemCode           string                `xml:"SystemCode,omitempty" json:"system_code,omitempty" bson:"system_code,omitempty"`
	BookingID            uint64                `xml:"BookingID" json:"booking_id" bson:"booking_id"`
	BookingParentID      uint64                `xml:"BookingParentID" json:"booking_parent_id" bson:"booking_parent_id"`
	ParentRecordLocator  string                `xml:"ParentRecordLocator,omitempty" json:"parent_record_locator,omitempty" bson:"parent_record_locator,omitempty"`
	BookingChangeCode    string                `xml:"BookingChangeCode,omitempty" json:"booking_change_code,omitempty" bson:"booking_change_code,omitempty"`
	GroupName            string                `xml:"GroupName,omitempty" json:"group_name,omitempty" bson:"group_name,omitempty"`
	SourcePOS            pointOfSaleODT        `xml:"SourcePOS,omitempty" json:"source_pos,omitempty" bson:"source_pos,omitempty"`
	BookingHold          bookingHoldODT        `xml:"BookingHold,omitempty" json:"booking_hold,omitempty" bson:"booking_hold,omitempty"`
	ReceivedBy           receivedByInfoODT     `xml:"ReceivedBy,omitempty" json:"received_by,omitempty" bson:"received_by,omitempty"`
	RecordLocators       recordLocatorsODT     `xml:"RecordLocators,omitempty" json:"record_locators,omitempty" bson:"record_locators,omitempty"`
	Passengers           passengersODT         `xml:"Passengers,omitempty" json:"passengers,omitempty" bson:"passengers,omitempty"`
	BookingComments      bookingCommentsODT    `xml:"BookingComments,omitempty" json:"booking_comments,omitempty" bson:"booking_comments,omitempty"`
	BookingContacts      bookingContactListODT `xml:"BookingContacts,omitempty" json:"booking_contacts,omitempty" bson:"booking_contacts,omitempty"`
	NumericRecordLocator string                `xml:"NumericRecordLocator" json:"numeric_locator,omitempty" bson:"numeric_locator,omitempty"`
	RestrictionOverride  bool                  `xml:"RestrictionOverride,omitempty" json:"restriction_override,omitempty" bson:"restriction_override,omitempty"`
	ChangeHoldDateTime   bool                  `xml:"ChangeHoldDateTime,omitempty" json:"change_hold_datetime,omitempty" bson:"change_hold_datetime,omitempty"`
	WaiveNameChangeFee   bool                  `xml:"WaiveNameChangeFee,omitempty" json:"waive_name_change_fee,omitempty" bson:"waive_name_change_fee,omitempty"`
	WaivePenaltyFee      bool                  `xml:"WaivePenaltyFee,omitempty" json:"waive_penalty_fee,omitempty" bson:"waive_penalty_fee,omitempty"`
	WaiveSpoilageFee     bool                  `xml:"WaiveSpoilageFee,omitempty" json:"waive_spoilage_fee,omitempty" bson:"waive_spoilage_fee,omitempty"`
	DistributeToContacts bool                  `xml:"DistributeToContacts,omitempty" json:"distribute_contacts,omitempty" bson:"distribute_contacts,omitempty"`
}

type bookingCommitRequestODT struct {
	BookingCommitRequestData bookingCommitRequestDataODT `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking BookingCommitRequestData" json:"update_passengers_request_data" bson:"update_passengers_request_data"`
}

type bookingCommitRequestBodyODT struct {
	Data bookingCommitRequestODT `xml:"http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService BookingCommitRequest" json:"data" bson:"data"`
}

type bookingCommitRequestEnvelopeODT struct {
	XMLName xml.Name                    `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	Header  requestHeaderODT            `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    bookingCommitRequestBodyODT `xml:"Body" json:"body" bson:"body"`
}

type bookingCommitResponseDataODT struct {
	Success                  successODT                 `xml:"Success,omitempty" json:"success,omitempty" bson:"success,omitempty"`
	Warning                  warningODT                 `xml:"Warning,omitempty" json:"warning,omitempty" bson:"warning,omitempty"`
	Error                    errorODT                   `xml:"Error,omitempty" json:"error,omitempty" bson:"error,omitempty"`
	OtherServiceInformations otherServiceInformationODT `xml:"OtherServiceInformations,omitempty" json:"other_service_informations,omitempty" bson:"other_service_informations,omitempty"`
}

type bookingCommitResponseODT struct {
	Detail bookingCommitResponseDataODT `xml:"BookingUpdateResponseData" json:"data" bson:"data"`
}

type bookingCommitResponseBodyODT struct {
	Fault faultODT                 `xml:"Fault,omitempty" json:"fault,omitempty" bson:"fault,omitempty"`
	Data  bookingCommitResponseODT `xml:"BookingCommitResponse" json:"data" bson:"data"`
}

type bookingCommitResponseEnvelopeODT struct {
	XMLName xml.Name                     `xml:"Envelope"`
	Header  string                       `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    bookingCommitResponseBodyODT `xml:"Body" json:"body" bson:"body"`
}

type getByRecordLocatorODT struct {
	RecordLocator string `xml:"RecordLocator,omitempty" json:"record_locator,omitempty" bson:"record_locator,omitempty"`
}

type getByThirdPartyRecordLocatorODT struct {
	SystemCode    string `xml:"SystemCode,omitempty" json:"system_code,omitempty" bson:"system_code,omitempty"`
	RecordLocator string `xml:"RecordLocator,omitempty" json:"record_locator,omitempty" bson:"record_locator,omitempty"`
}

type getByIDODT struct {
	BookingID     string `xml:"BookingID,omitempty" json:"booking_id,omitempty" bson:"booking_id,omitempty"`
	RecordLocator string `xml:"RetrieveFromArchive,omitempty" json:"retrieve_from_archive,omitempty" bson:"retrieve_from_archive,omitempty"`
}

type bookingInfoRequestDataODT struct {
	GetBookingBy                 string                          `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking GetBookingBy" json:"get_booking_by" bson:"get_booking_by"` //
	GetByRecordLocator           getByRecordLocatorODT           `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking GetByRecordLocator,omitempty" json:"get_by_record_locator,omitempty" bson:"get_by_record_locator,omitempty"`
	GetByThirdPartyRecordLocator getByThirdPartyRecordLocatorODT `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking GetByThirdPartyRecordLocator,omitempty" json:"get_by_third_party_record_locator,omitempty" bson:"get_by_third_party_record_locator,omitempty"`
	GetByID                      getByIDODT                      `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking GetByID,omitempty" json:"get_by_id,omitempty" bson:"get_by_id,omitempty"`
}

type bookingInfoRequestODT struct {
	GetBookingReqData bookingInfoRequestDataODT `xml:"GetBookingReqData" json:"data" bson:"data"`
}

type bookingInfoRequestBodyODT struct {
	Data bookingInfoRequestODT `xml:"http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService GetBookingRequest" json:"get_booking_request_data" bson:"get_booking_request_data"`
}

type bookingInfoRequestEnvelopeODT struct {
	XMLName xml.Name                  `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	Header  requestHeaderODT          `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    bookingInfoRequestBodyODT `xml:"Body" json:"body" bson:"body"`
}

type typeOfSaleODT struct {
	PaxResidentCountry string   `xml:"PaxResidentCountry,omitempty" json:"pax_resident_country,omitempty" bson:"pax_resident_country,omitempty"`
	PromotionCode      string   `xml:"PromotionCode,omitempty" json:"promotion_code,omitempty" bson:"promotion_code,omitempty"`
	FareTypes          []string `xml:"FareTypes,omitempty" json:"fare_types,omitempty" bson:"fare_types,omitempty"`
}

type bookingQueueInfoODT struct {
	PassengerID    uint64 `xml:"PassengerID,omitempty" json:"passenger_id,omitempty" bson:"passenger_id,omitempty"`
	WatchListID    uint64 `xml:"WatchListID,omitempty" json:"watch_list_id,omitempty" bson:"watch_list_id,omitempty"`
	QueueCode      string `xml:"QueueCode,omitempty" json:"queue_code,omitempty" bson:"queue_code,omitempty"`
	Notes          string `xml:"Notes,omitempty" json:"notes,omitempty" bson:"notes,omitempty"`
	QueueEventType string `xml:"QueueEventType,omitempty" json:"queue_event_type" bson:"queue_event_type"` /*Default
	BookingBalanceDue BookingNegativeBalance BookingCustomerComment DeclinedPaymentInitial DeclinedPaymentChange
	FareOverride ScheduleTimeChange ScheduleTimeChangeMisconnect ScheduleCancellation FlightDesignatorChange
	ReaccommodationMove GDSCancelWithPendingPayment InvalidPriceStatusOverride FareRestrictionOverride
	HeldBookings InvalidPriceStatus Watchlist NonFlightServiceFee NotAllTicketNumbersReceived
	BookingSegmentOversold ReaccommodationCancel ExternalSSRAutoConfirmed OpCarrierSegUpdate OpCarrierSSRUpdate
	OpCarrierOtherUpdate NameChangeNotAllowed InboundASCNotProcessed OpCarrierInformationChange BookingComponentUpdate
	GroupBookings BankDirectPNROutOfBalance NoSeatAssigned SeatNumberChange SSRNotSupportedOnNewSeat
	FewerSeatPreferencesMetOnNewSeat AOSUnableToConfirmCancel ETicketIssue ETicketFollowup InvoluntaryFlyAhead
	ManualClearanceOnOutage UnbalancedPoints VoluntaryFlightChange InvoluntaryFlightChange
	OpCarrierTimeChange OACarrierTimeChange MustBeSeatGroupViolation
	HoldCancellationFailed ItineraryIntegrity ScheduleTimeChangeWithDynamicQueueCode
	ReaccommodationMoveWithDynamicQueueCode ReducePartyNotProcessed CheckedPassengerUpdate
	Unmapped
	*/
	QueueName   string `xml:"QueueName,omitempty" json:"queue_name,omitempty" bson:"queue_name,omitempty"`
	QueueAction string `xml:"QueueAction,omitempty" json:"queue_action,omitempty" bson:"queue_action,omitempty"` /* Default
	Warning Lock DefaultAndNotify WarningAndNotify LockAndNotify Unmapped */
	QueueMode      string `xml:"QueueMode,omitempty" json:"queue_mode,omitempty" bson:"queue_mode,omitempty"` /* EnQueued DeQueued Unmapped */
	BookingQueueID uint64 `xml:"BookingQueueID,omitempty" json:"booking_queue_id,omitempty" bson:"booking_queue_id,omitempty"`
	BookingID      uint64 `xml:"BookingID,omitempty" json:"booking_id,omitempty" bson:"booking_id,omitempty"`
	SegmentKey     string `xml:"SegmentKey,omitempty" json:"segment_key,omitempty" bson:"segment_key,omitempty"`
	SubQueueCode   string `xml:"SubQueueCode,omitempty" json:"sub_queue_code,omitempty" bson:"sub_queue_code,omitempty"`
}

type bookingQueueInfosODT struct {
	BookingQueueInfo []bookingQueueInfoODT `xml:"BookingQueueInfo,omitempty" json:"booking_queue_info,omitempty" bson:"booking_queue_info,omitempty"`
}

type dccODT struct {
	DCC       string `xml:"DCCRateID,omitempty" json:"dcc,omitempty" bson:"dcc,omitempty"`
	DCCStatus string `xml:"DCCStatus,omitempty" json:"dcc_status,omitempty" bson:"dcc_status,omitempty"` /*
		DCCNotOffered DCCOfferRejected DCCOfferAccepted DCCInitialValue
		MCCInUse
		Unmapped
	*/
	ValidationDCCApplicable bool          `xml:"ValidationDCCApplicable,omitempty" json:"validation_dcc_applicable,omitempty" bson:"validation_dcc_applicable,omitempty"`
	ValidationDCCRateValue  prettyFloat64 `xml:"ValidationDCCRateValue,omitempty" json:"validation_dcc_rate_value,omitempty" bson:"validation_dcc_rate_value,omitempty"`
	ValidationDCCCurrency   string        `xml:"ValidationDCCCurrency,omitempty" json:"validation_dcc_currency,omitempty" bson:"validation_dcc_currency,omitempty"`
	ValidationDCCAmount     prettyFloat64 `xml:"ValidationDCCAmount,omitempty" json:"validation_dcc_amount,omitempty" bson:"validation_dcc_amount,omitempty"`
	ValidationDCCPutInState string        `xml:"ValidationDCCPutInState,omitempty" json:"validation_dcc_put_in_state,omitempty" bson:"validation_dcc_put_in_state,omitempty"`
}

type threeDSecureODT struct {
	BrowserUserAgent           string `xml:"BrowserUserAgent,omitempty" json:"browser_user_agent,omitempty" bson:"browser_user_agent,omitempty"`
	BrowserAccept              string `xml:"BrowserAccept,omitempty" json:"browser_accept,omitempty" bson:"browser_accept,omitempty"`
	RemoteIPAddress            string `xml:"RemoteIpAddress,omitempty" json:"remote_ip_address,omitempty" bson:"remote_ip_address,omitempty"`
	TermURL                    string `xml:"TermUrl,omitempty" json:"term_url,omitempty" bson:"term_url,omitempty"`
	ProxyVia                   string `xml:"ProxyVia,omitempty" json:"proxy_via,omitempty" bson:"proxy_via,omitempty"`
	ValidationTDSApplicable    bool   `xml:"ValidationTDSApplicable,omitempty" json:"validation_tds_applicable,omitempty" bson:"validation_tds_applicable,omitempty"`
	ValidationTDSPaReq         string `xml:"ValidationTDSPaReq,omitempty" json:"validation_tds_pa_req,omitempty" bson:"validation_tds_pa_req,omitempty"`
	ValidationTDSAcsURL        string `xml:"ValidationTDSAcsUrl,omitempty" json:"validation_tds_acs_url,omitempty" bson:"validation_tds_acs_url,omitempty"`
	ValidationTDSPaRes         string `xml:"ValidationTDSPaRes,omitempty" json:"validation_tds_pa_res,omitempty" bson:"validation_tds_pa_res,omitempty"`
	ValidationTDSSuccessful    bool   `xml:"ValidationTDSSuccessful,omitempty" json:"validation_tds_successful,omitempty" bson:"validation_tds_successful,omitempty"`
	ValidationTDSAuthResult    string `xml:"ValidationTDSAuthResult,omitempty" json:"validation_tds_auth_result,omitempty" bson:"validation_tds_auth_result,omitempty"`
	ValidationTDSCavv          string `xml:"ValidationTDSCavv,omitempty" json:"validation_tds_cavv,omitempty" bson:"validation_tds_cavv,omitempty"`
	ValidationTDSCavvAlgorithm string `xml:"ValidationTDSCavvAlgorithm,omitempty" json:"validation_tds_cavv_algorithm,omitempty" bson:"validation_tds_cavv_algorithm,omitempty"`
	ValidationTDSEci           string `xml:"ValidationTDSEci,omitempty" json:"validation_tds_eci,omitempty" bson:"validation_tds_eci,omitempty"`
	ValidationTDSXid           string `xml:"ValidationTDSXid,omitempty" json:"validation_tds_xid,omitempty" bson:"validation_tds_xid,omitempty"`
}

type paymentFieldODT struct {
	FieldName  string `xml:"FieldName,omitempty" json:"field_name,omitempty" bson:"field_name,omitempty"`
	FieldValue string `xml:"FieldValue,omitempty" json:"field_value,omitempty" bson:"field_value,omitempty"`
}

type paymentFieldsODT struct {
	PaymentField []paymentFieldODT `xml:"PaymentField,omitempty" json:"payment_field,omitempty" bson:"payment_field,omitempty"`
}

type paymentAddressODT struct {
	PaymentID     uint64 `xml:"PaymentID,omitempty" json:"payment_id,omitempty" bson:"payment_id,omitempty"`
	CompanyName   string `xml:"CompanyName,omitempty" json:"company_name,omitempty" bson:"company_name,omitempty"`
	AddressLine1  string `xml:"AddressLine1,omitempty" json:"address_line_1,omitempty" bson:"address_line_1,omitempty"`
	AddressLine2  string `xml:"AddressLine2,omitempty" json:"address_line_2,omitempty" bson:"address_line_2,omitempty"`
	AddressLine3  string `xml:"AddressLine3,omitempty" json:"address_line_3,omitempty" bson:"address_line_3,omitempty"`
	City          string `xml:"City,omitempty" json:"city,omitempty" bson:"city,omitempty"`
	ProvinceState string `xml:"ProvinceState,omitempty" json:"province_state,omitempty" bson:"province_state,omitempty"`
	PostalCode    string `xml:"PostalCode,omitempty" json:"postal_code,omitempty" bson:"postal_code,omitempty"`
	CountryCode   string `xml:"CountryCode,omitempty" json:"country_code,omitempty" bson:"country_code,omitempty"`
}

type paymentAddressesODT struct {
	PaymentAddress []paymentAddressODT `xml:"PaymentAddress,omitempty" json:"payment_address,omitempty" bson:"payment_address,omitempty"`
}
type paymentODT struct {
	State         string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	ReferenceType string `xml:"ReferenceType,omitempty" json:"reference_type,omitempty" bson:"reference_type,omitempty"`                                     /*
		Default Booking Session Unmapped*/
	ReferenceID       uint64 `xml:"ReferenceID,omitempty" json:"reference_id,omitempty" bson:"reference_id,omitempty"`
	PaymentMethodType string `xml:"PaymentMethodType,omitempty" json:"payment_method_type,omitempty" bson:"payment_method_type,omitempty"` /*
		PrePaid ExternalAccount AgencyAccount CustomerAccount Voucher Loyalty Unmapped
	*/
	PaymentMethodCode     string        `xml:"PaymentMethodCode,omitempty" json:"payment_method_code,omitempty" bson:"payment_method_code,omitempty"`
	CurrencyCode          string        `xml:"CurrencyCode,omitempty" json:"currency_code,omitempty" bson:"currency_code,omitempty"`
	PaymentAmount         prettyFloat64 `xml:"PaymentAmount,omitempty" json:"payment_amount,omitempty" bson:"payment_amount,omitempty"`
	CollectedCurrencyCode string        `xml:"CollectedCurrencyCode,omitempty" json:"collected_currency_code,omitempty" bson:"collected_currency_code,omitempty"`
	CollectedAmount       prettyFloat64 `xml:"CollectedAmount,omitempty" json:"collected_amount,omitempty" bson:"collected_amount,omitempty"`
	QuotedCurrencyCode    string        `xml:"QuotedCurrencyCode,omitempty" json:"quoted_currency_code,omitempty" bson:"quoted_currency_code,omitempty"`
	QuotedAmount          prettyFloat64 `xml:"QuotedAmount,omitempty" json:"quoted_amount,omitempty" bson:"quoted_amount,omitempty"`
	Status                string        `xml:"Status,omitempty" json:"status,omitempty" bson:"status,omitempty"` /* New
	Received Pending Approved Declined Unknown PendingCustomerAction Unmapped */
	AccountNumber       string `xml:"AccountNumber,omitempty" json:"account_number,omitempty" bson:"account_number,omitempty"`
	AccountNumberID     uint64 `xml:"AccountNumberID,omitempty" json:"account_number_id,omitempty" bson:"account_number_id,omitempty"`
	Expiration          string `xml:"Expiration,omitempty" json:"expiration,omitempty" bson:"expiration,omitempty"` //2019-01-01
	AuthorizationCode   string `xml:"AuthorizationCode,omitempty" json:"authorization_code,omitempty" bson:"authorization_code,omitempty"`
	AuthorizationStatus string `xml:"AuthorizationStatus,omitempty" json:"authorization_status,omitempty" bson:"authorization_status,omitempty"` /*
		Unknown Acknowledged Pending InProcess Approved Declined Referral
		PickUpCard HotCard Voided Retrieval ChargedBack Error
		ValidationFailed Address VerificationCode FraudPrevention Unmapped */
	ParentPaymentID   uint64 `xml:"ParentPaymentID,omitempty" json:"parent_payment_id,omitempty" bson:"parent_payment_id,omitempty"`
	Transferred       bool   `xml:"Transferred,omitempty" json:"transferred,omitempty" bson:"transferred,omitempty"`
	ReconcilliationID uint64 `xml:"ReconcilliationID,omitempty" json:"reconcilliation_id,omitempty" bson:"reconcilliation_id,omitempty"`
	FundedDate        string `xml:"FundedDate,omitempty" json:"funded_date,omitempty" bson:"funded_date,omitempty"`
	Installments      uint16 `xml:"Installments,omitempty" json:"installments,omitempty" bson:"installments,omitempty"`
	PaymentText       string `xml:"PaymentText,omitempty" json:"payment_text,omitempty" bson:"payment_text,omitempty"`
	ChannelType       string `xml:"ChannelType,omitempty" json:"channel_type,omitempty" bson:"channel_type,omitempty"` /*
		Default Direct Web GDS API Unmapped */
	PaymentNumber               uint16              `xml:"PaymentNumber,omitempty" json:"payment_number,omitempty" bson:"payment_number,omitempty"`
	AccountName                 string              `xml:"AccountName,omitempty" json:"account_name,omitempty" bson:"account_name,omitempty"`
	SourcePointOfSale           pointOfSaleODT      `xml:"SourcePointOfSale,omitempty" json:"source_point_of_sale,omitempty" bson:"source_point_of_sale,omitempty"`
	PointOfSale                 pointOfSaleODT      `xml:"PointOfSale,omitempty" json:"point_of_sale,omitempty" bson:"point_of_sale,omitempty"`
	PaymentID                   uint64              `xml:"PaymentID,omitempty" json:"payment_id,omitempty" bson:"payment_id,omitempty"`
	Deposit                     bool                `xml:"Deposit,omitempty" json:"deposit,omitempty" bson:"deposit,omitempty"`
	AccountID                   uint64              `xml:"AccountID,omitempty" json:"account_id,omitempty" bson:"account_id,omitempty"`
	Password                    string              `xml:"Password,omitempty" json:"password,omitempty" bson:"password,omitempty"`
	AccountTransactionCode      string              `xml:"AccountTransactionCode,omitempty" json:"account_transaction_code,omitempty" bson:"account_transaction_code,omitempty"`
	VoucherID                   uint64              `xml:"VoucherID,omitempty" json:"vouncer_id,omitempty" bson:"vouncer_id,omitempty"`
	VoucherTransactionID        uint64              `xml:"VoucherTransactionID,omitempty" json:"vouncer_transaction_id,omitempty" bson:"vouncer_transaction_id,omitempty"`
	OverrideVoucherRestrictions bool                `xml:"OverrideVoucherRestrictions,omitempty" json:"override_voucher_restrictions,omitempty" bson:"override_voucher_restrictions,omitempty"`
	OverrideAmount              bool                `xml:"OverrideAmount,omitempty" json:"override_amount,omitempty" bson:"override_amount,omitempty"`
	RecordLocator               string              `xml:"RecordLocator,omitempty" json:"record_locator,omitempty" bson:"record_locator,omitempty"`
	PaymentAddedToState         bool                `xml:"PaymentAddedToState,omitempty" json:"payment_added_to_state,omitempty" bson:"payment_added_to_state,omitempty"`
	DCC                         dccODT              `xml:"DCC,omitempty" json:"dcc,omitempty" bson:"dcc,omitempty"`
	ThreeDSecure                threeDSecureODT     `xml:"ThreeDSecure,omitempty" json:"tree_d_secure,omitempty" bson:"tree_d_secure,omitempty"`
	PaymentFields               paymentFieldsODT    `xml:"PaymentFields,omitempty" json:"payment_fields,omitempty" bson:"payment_fields,omitempty"`
	PaymentAddresses            paymentAddressesODT `xml:"PaymentAddresses,omitempty" json:"payment_addresses,omitempty" bson:"payment_addresses,omitempty"`
	CreatedDate                 string              `xml:"CreatedDate,omitempty" json:"created_date,omitempty" bson:"created_date,omitempty"`
	CreatedAgentID              uint64              `xml:"CreatedAgentID,omitempty" json:"created_agent_id,omitempty" bson:"created_agent_id,omitempty"`
	ModifiedDate                string              `xml:"ModifiedDate,omitempty" json:"modified_date,omitempty" bson:"modified_date,omitempty"`
	ModifiedAgentID             uint64              `xml:"ModifiedAgentID,omitempty" json:"modified_agent_id,omitempty" bson:"modified_agent_id,omitempty"`
	BinRange                    int                 `xml:"BinRange,omitempty" json:"bin_range,omitempty" bson:"bin_range,omitempty"`
	ApprovalDate                string              `xml:"ApprovalDate,omitempty" json:"approval_date,omitempty" bson:"approval_date,omitempty"`
}

type paymentsODT struct {
	Payment []paymentODT `xml:"Payment,omitempty" json:"payment,omitempty" bson:"payment,omitempty"`
}

type bookingInfoODT struct {
	State         string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	BookingStatus string `xml:"BookingStatus,omitempty" json:"booking_status,omitempty" bson:"booking_status,omitempty"`                                     /*
	 Default Hold Confirmed Closed HoldCanceled
	PendingArchive Archived Unmapped */
	BookingType string `xml:"BookingType,omitempty" json:"booking_type,omitempty" bson:"booking_type,omitempty"`
	ChannelType string `xml:"ChannelType,omitempty" json:"channel_type,omitempty" bson:"channel_type,omitempty"` /*
		Default Direct Web GDS API Unmapped */
	CreatedDate  string `xml:"CreatedDate,omitempty" json:"created_date,omitempty" bson:"created_date,omitempty"`
	ExpiredDate  string `xml:"ExpiredDate,omitempty" json:"expired_date,omitempty" bson:"expired_date,omitempty"`
	ModifiedDate string `xml:"ModifiedDate,omitempty" json:"modified_date,omitempty" bson:"modified_date,omitempty"`
	PriceStatus  string `xml:"PriceStatus,omitempty" json:"price_status,omitempty" bson:"price_status,omitempty"` /*
		Invalid Override Valid Unmapped */
	ProfileStatus string `xml:"ProfileStatus,omitempty" json:"profile_status,omitempty" bson:"profile_status,omitempty"` /*
		Default KnownIndividual ResolutionGroup SelecteeGroup
		NotUsed FailureGroup RandomSelectee Exempt Unmapped */
	ChangeAllowed     bool   `xml:"ChangeAllowed,omitempty" json:"change_allowed,omitempty" bson:"change_allowed,omitempty"`
	CreatedAgentID    uint64 `xml:"CreatedAgentID,omitempty" json:"created_agent_id,omitempty" bson:"created_agent_id,omitempty"`
	ModifiedAgentID   uint64 `xml:"ModifiedAgentID,omitempty" json:"modified_agent_id,omitempty" bson:"modified_agent_id,omitempty"`
	BookingDate       string `xml:"BookingDate,omitempty" json:"booking_date,omitempty" bson:"booking_date,omitempty"`
	OwningCarrierCode string `xml:"OwningCarrierCode,omitempty" json:"owner_carrier_code,omitempty" bson:"owner_carrier_code,omitempty"`
	PaidStatus        string `xml:"PaidStatus,omitempty" json:"paid_status,omitempty" bson:"paid_status,omitempty"` /*
		UnderPaid PaidInFull OverPaid Unmapped */
}

type bookingODT struct {
	State                string                `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	RecordLocator        string                `xml:"RecordLocator,omitempty" json:"record_locator,omitempty" bson:"record_locator,omitempty"`
	CurrencyCode         string                `xml:"CurrencyCode,omitempty" json:"currency_code,omitempty" bson:"currency_code,omitempty"`
	PaxCount             uint8                 `xml:"PaxCount,omitempty" json:"pax_count,omitempty" bson:"pax_count,omitempty"`
	SystemCode           string                `xml:"SystemCode,omitempty" json:"system_code,omitempty" bson:"system_code,omitempty"`
	BookingID            uint64                `xml:"BookingID,omitempty" json:"booking_id,omitempty" bson:"booking_id,omitempty"`
	BookingParentID      uint64                `xml:"BookingParentID,omitempty" json:"booking_parent_id,omitempty" bson:"booking_parent_id,omitempty"`
	ParentRecordLocator  string                `xml:"ParentRecordLocator,omitempty" json:"parent_record_locator,omitempty" bson:"parent_record_locator,omitempty"`
	BookingChangeCode    string                `xml:"BookingChangeCode,omitempty" json:"booking_change_code,omitempty" bson:"booking_change_code,omitempty"`
	GroupName            string                `xml:"GroupName,omitempty" json:"group_name,omitempty" bson:"group_name,omitempty"`
	BookingInfo          bookingInfoODT        `xml:"BookingInfo,omitempty" json:"booking_info,omitempty" bson:"booking_info,omitempty"`
	POS                  pointOfSaleODT        `xml:"POS,omitempty" json:"pos,omitempty" bson:"pos,omitempty"`
	SourcePOS            pointOfSaleODT        `xml:"SourcePOS,omitempty" json:"source_pos,omitempty" bson:"source_pos,omitempty"`
	TypeOfSale           typeOfSaleODT         `xml:"TypeOfSale,omitempty" json:"type_of_sale,omitempty" bson:"type_of_sale,omitempty"`
	BookingHold          bookingHoldODT        `xml:"BookingHold,omitempty" json:"booking_hold,omitempty" bson:"booking_hold,omitempty"`
	BookingSum           bookingSumODT         `xml:"BookingSum,omitempty" json:"booking_sum,omitempty" bson:"booking_sum,omitempty"`
	ReceivedBy           receivedByInfoODT     `xml:"ReceivedBy,omitempty" json:"received_by,omitempty" bson:"received_by,omitempty"`
	RecordLocators       recordLocatorsODT     `xml:"RecordLocators,omitempty" json:"record_locators,omitempty" bson:"record_locators,omitempty"`
	Passengers           passengersODT         `xml:"Passengers,omitempty" json:"passengers,omitempty" bson:"passengers,omitempty"`
	Journeys             journeysODT           `xml:"Journeys,omitempty" json:"journeys,omitempty" bson:"journeys,omitempty"`
	BookingComments      bookingCommentsODT    `xml:"BookingComments,omitempty" json:"booking_comments,omitempty" bson:"booking_comments,omitempty"`
	BookingQueueInfos    bookingQueueInfosODT  `xml:"BookingQueueInfos,omitempty" json:"booking_queue_infos,omitempty" bson:"booking_queue_infos,omitempty"`
	BookingContacts      bookingContactListODT `xml:"BookingContacts,omitempty" json:"booking_contacts,omitempty" bson:"booking_contacts,omitempty"`
	Payments             paymentsODT           `xml:"Payments,omitempty" json:"payments,omitempty" bson:"payments,omitempty"`
	BookingComponents    bookingComponentsODT  `xml:"BookingComponents,omitempty" json:"booking_components,omitempty" bson:"booking_components,omitempty"`
	NumericRecordLocator string                `xml:"NumericRecordLocator,omitempty" json:"numeric_record_locator,omitempty" bson:"numeric_record_locator,omitempty"`
}

type bookingInfoResponseDataODT struct {
	Success                  successODT                 `xml:"Success,omitempty" json:"success,omitempty" bson:"success,omitempty"`
	Warning                  warningODT                 `xml:"Warning,omitempty" json:"warning,omitempty" bson:"warning,omitempty"`
	Error                    errorODT                   `xml:"Error,omitempty" json:"error,omitempty" bson:"error,omitempty"`
	Booking                  bookingODT                 `xml:"Booking,omitempty" json:"booking,omitempty" bson:"booking,omitempty"`
	OtherServiceInformations otherServiceInformationODT `xml:"OtherServiceInformations,omitempty" json:"other_service_informations,omitempty" bson:"other_service_informations,omitempty"`
}

type bookingComponentChargeODT struct {
	State        string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	ChargeNumber uint64 `xml:"ChargeNumber,omitempty" json:"charge_number,omitempty" bson:"charge_number,omitempty"`

	ChargeType string `xml:"ChargeType,omitempty" json:"charge_type,omitempty" bson:"charge_type,omitempty"` /*
		FarePrice Discount IncludedTravelFee IncludedTax TravelFee Tax ServiceCharge PromotionDiscount
		ConnectionAdjustmentAmount AddOnServicePrice IncludedAddOnServiceFee AddOnServiceFee
		Calculated Note AddOnServiceMarkup FareSurcharge Loyalty FarePoints
		DiscountPoints AddOnServiceCancelFee Unmapped */
	ChargeCode  string `xml:"ChargeCode,omitempty" json:"charge_code,omitempty" bson:"charge_code,omitempty"`
	TicketCode  string `xml:"TicketCode,omitempty" json:"ticket_code,omitempty" bson:"ticket_code,omitempty"`
	CollectType string `xml:"CollectType,omitempty" json:"collect_type,omitempty" bson:"collect_type,omitempty"` /*
		SellerChargeable ExternalChargeable SellerNonChargeable ExternalNonChargeable
		ExternalChargeableImmediate Unmapped */
	CurrencyCode        string        `xml:"CurrencyCode,omitempty" json:"currency,omitempty" bson:"currency,omitempty"`
	ChargeAmount        prettyFloat64 `xml:"ChargeAmount,omitempty" json:"charge_amount,omitempty" bson:"charge_amount,omitempty"`
	ChargeDetail        string        `xml:"ChargeDetail,omitempty" json:"charge_detail,omitempty" bson:"charge_detail,omitempty"`
	ForeignCurrencyCode string        `xml:"ForeignCurrencyCode,omitempty" json:"foreign_currency,omitempty" bson:"foreign_currency,omitempty"`
	ForeignAmount       prettyFloat64 `xml:"ForeignAmount,omitempty" json:"foreign_amount,omitempty" bson:"foreign_amount,omitempty"`
	CreatedDate         string        `xml:"CreatedDate,omitempty" json:"created_date,omitempty" bson:"created_date,omitempty"`
	CreatedAgentID      uint64        `xml:"CreatedAgentID,omitempty" json:"created_agent_id,omitempty" bson:"created_agent_id,omitempty"`
}

type bookingComponentChargesODT struct {
	BookingComponentCharge []bookingComponentChargeODT `xml:"BookingComponentCharge,omitempty" json:"booking_component_charge,omitempty" bson:"booking_component_charge,omitempty"`
}

type orderItemODT struct {
	OrderItemData string `xml:"OrderItemData,omitempty" json:"order_item_data,omitempty" bson:"order_item_data,omitempty"`
}

type tcOrderItemODT struct {
	CompanyCode string `xml:"CompanyCode,omitempty" json:"company_code,omitempty" bson:"company_code,omitempty"`
	XMLString   string `xml:"XML,omitempty" json:"xml,omitempty" bson:"xml,omitempty"`
	/* tobe completed
	   "Payment" nillable="true" type="tns:OrderPayment"/>
	   "CRL" nillable="true" type="xs:string"/>
	   "SupplierCode" nillable="true" type="xs:string"/>
	   "CorpDiscountCode" nillable="true" type="xs:string"/>
	   "RuleSetID" type="xs:int"/>
	   "RatingCode" nillable="true" type="xs:string"/>
	   "DepartmentCode" nillable="true" type="xs:string"/>
	   "AllowsSku" type="xs:boolean"/>
	   "AllowsGiftWrap" type="xs:boolean"/>
	   "BasePrice" type="xs:decimal"/>
	   "PreTaxTotalNow" type="xs:decimal"/>
	   "PreTaxTotalLater" type="xs:decimal"/>
	   "TaxTotalLater" type="xs:decimal"/>
	   "TaxTotalNow" type="xs:decimal"/>
	   "TotalLater" type="xs:decimal"/>
	   "TotalNow" type="xs:decimal"/>
	   "BasePriceTotal" type="xs:decimal"/>
	   "DescriptionLong" nillable="true" type="xs:string"/>
	   "DisplayPrice" type="xs:decimal"/>
	   "DisplayPriceTotal" type="xs:decimal"/>
	   "ThumbFilename" nillable="true" type="xs:string"/>
	   "MarkupAmount" type="xs:decimal"/>
	   "MarkupAmountTotal" type="xs:decimal"/>
	   "NewFlag" type="xs:boolean"/>
	   "ActiveStatus" type="xs:boolean"/>
	   "FeesTotal" type="xs:decimal"/>
	   "Field1" nillable="true" type="xs:string"/>
	   "Field2" nillable="true" type="xs:string"/>
	   "Field3" nillable="true" type="xs:string"/>
	   "Field4" nillable="true" type="xs:string"/>
	   "Field5" nillable="true" type="xs:string"/>
	   "SkuExpectedDate" type="xs:dateTime"/>
	   "OrderId" nillable="true" type="xs:string"/>
	   "ItemSequence" type="xs:int"/>
	   "CatalogCode" nillable="true" type="xs:string"/>
	   "VendorCode" nillable="true" type="xs:string"/>
	   "CategoryCode" nillable="true" type="xs:string"/>
	   "ItemTypeCode" nillable="true" type="xs:string"/>
	   "ItemId" nillable="true" type="xs:string"/>
	   "SkuId" type="xs:int"/>
	   "IsDueNow" type="xs:boolean"/>
	   "ExternalSkuId" nillable="true" type="xs:string"/>
	   "ExternalSkuCatalogId" nillable="true" type="xs:string"/>
	   "PurchaseDate" type="xs:dateTime"/>
	   "UsageDate" type="xs:dateTime"/>
	   "ItemDescription" nillable="true" type="xs:string"/>
	   "SkuDescription" nillable="true" type="xs:string"/>
	   "GiftWrapped" type="xs:boolean"/>
	   "GiftWrapMessage" nillable="true" type="xs:string"/>
	   "RoleCodeSupplierPortal" nillable="true" type="xs:string"/>
	   "WarningList" nillable="true" type="tns:ArrayOfTCWarning"/>
	   "TermsConditionsList" nillable="true" type="tns:ArrayOfTermsConditions"/>
	   "CancellationPolicies" nillable="true" type="tns:ArrayOfTermsConditions"/>
	   "ParticipantList" nillable="true" type="tns:ArrayOfParticipant"/>
	   "OrderCustomer" nillable="true" type="tns:OrderCustomer"/>
	   "TaxExempt" type="xs:boolean"/>
	   "OrderItemStatusCode" nillable="true" type="xs:string"/>
	   "TaxAtUnitPrice" type="xs:boolean"/>
	   "CurrencyCode" nillable="true" type="xs:string"/>
	   "PromoCode" nillable="true" type="xs:string"/>
	   "SourceCode" nillable="true" type="xs:string"/>
	   "ContentType" nillable="true" type="xs:string"/>
	   "Quantity" type="xs:int"/>
	   "ListPrice" type="xs:decimal"/>
	   "ListPriceTotal" type="xs:decimal"/>
	   "DiscountAmount" type="xs:decimal"/>
	   "DiscountAmountTotal" type="xs:decimal"/>
	   "PersonalizationPriceTotal" type="xs:decimal"/>
	   "HandlingCharge" type="xs:decimal"/>
	   "HandlingChargeTotal" type="xs:decimal"/>
	   "HandlingDiscount" type="xs:decimal"/>
	   "HandlingDiscountTotal" type="xs:decimal"/>
	   "DiscountedHandlingChargeTotal" type="xs:decimal"/>
	   "DiscountedListPrice" type="xs:decimal"/>
	   "DiscountedListPriceTotal" type="xs:decimal"/>
	   "TaxableTotal" type="xs:decimal"/>
	   "TaxRate" type="xs:int"/>
	   "ServicesTotal" type="xs:decimal"/>
	   "Total" type="xs:decimal"/>
	   "OrderHandling" nillable="true" type="tns:OrderHandling"/>
	   "OrderItemAddress" nillable="true" type="tns:OrderItemAddress"/>
	   "OrderItemPersonalizationList" nillable="true" type="tns:ArrayOfOrderItemPersonalization"/>
	   "OrderItemSkuDetailValueList" nillable="true" type="tns:ArrayOfOrderItemSkuDetail"/>
	   "OrderItemNoteList" nillable="true" type="tns:ArrayOfOrderItemNote"/>
	   "OrderItemLocatorList" nillable="true" type="tns:ArrayOfOrderItemLocator"/>
	   "OrderItemFeeList" nillable="true" type="tns:ArrayOfFee"/>
	   "OrderItemLocationList" nillable="true" type="tns:ArrayOfOrderItemLocation"/>
	   "OrderItemParameterList" nillable="true" type="tns:ArrayOfOrderItemParameter"/>
	   "OrderItemStatusHistoryList" nillable="true" type="tns:ArrayOfOrderItemStatusHistory"/>
	   "PaymentOption" type="q3:PaymentOptions" xmlns:q3="http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations"/>
	   "OrderItemOrderPaymentList" nillable="true" type="q4:ArrayOfint" xmlns:q4="http://schemas.microsoft.com/2003/10/Serialization/Arrays"/>
	   "OrderItemElementList" nillable="true" type="tns:ArrayOfOrderItemElement"/>
	   "CancellationNumber" nillable="true" type="xs:string"/>
	   "CancellationDate" type="xs:dateTime"/>
	   "ComparisonKey" nillable="true" type="xs:string"/>*/
}

type bookingComponentODT struct {
	State              string `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Common State,omitempty" json:"state,omitempty" bson:"state,omitempty"` //New
	BookingID          uint64 `xml:"BookingID,omitempty" json:"booking_id,omitempty" bson:"booking_id,omitempty"`
	BookingComponentID uint64 `xml:"BookingComponentID,omitempty" json:"booking_component_id,omitempty" bson:"booking_component_id,omitempty"`
	OrderID            string `xml:"OrderID,omitempty" json:"order_id,omitempty" bson:"order_id,omitempty"` /*
		pattern = [\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}
	*/
	ItemSequence                      uint16                     `xml:"ItemSequence,omitempty" json:"item_sequence,omitempty" bson:"item_sequence,omitempty"`
	SupplierCode                      string                     `xml:"SupplierCode,omitempty" json:"supplier_code,omitempty" bson:"supplier_code,omitempty"`
	SupplierRecordLocator             string                     `xml:"SupplierRecordLocator,omitempty" json:"supplier_record_locator,omitempty" bson:"supplier_record_locator,omitempty"`
	SystemCode                        string                     `xml:"SystemCode,omitempty" json:"system_code,omitempty" bson:"system_code,omitempty"`
	SystemRecordLocator               string                     `xml:"SystemRecordLocator,omitempty" json:"system_record_locator,omitempty" bson:"system_record_locator,omitempty"`
	RecordReference                   string                     `xml:"RecordReference,omitempty" json:"record_reference,omitempty" bson:"record_reference,omitempty"`
	Status                            string                     `xml:"Status,omitempty" json:"status,omitempty" bson:"status,omitempty"`
	ServiceTypeCode                   string                     `xml:"ServiceTypeCode,omitempty" json:"service_type_code,omitempty" bson:"service_type_code,omitempty"`
	ItemID                            string                     `xml:"ItemID,omitempty" json:"item_id,omitempty" bson:"item_id,omitempty"`
	ItemTypeCode                      string                     `xml:"ItemTypeCode,omitempty" json:"item_type_code,omitempty" bson:"item_type_code,omitempty"`
	ItemDescription                   string                     `xml:"ItemDescription,omitempty" json:"item_description,omitempty" bson:"item_description,omitempty"`
	BeginDate                         string                     `xml:"BeginDate,omitempty" json:"begin_date,omitempty" bson:"begin_date,omitempty"`
	EndDate                           string                     `xml:"EndDate,omitempty" json:"EndDate,omitempty" bson:"EndDate,omitempty"`
	BeginLocationCode                 string                     `xml:"BeginLocationCode,omitempty" json:"begin_location_code,omitempty" bson:"begin_location_code,omitempty"`
	EndLocationCode                   string                     `xml:"EndLocationCode,omitempty" json:"end_location_code,omitempty" bson:"end_location_code,omitempty"`
	PassengerID                       uint64                     `xml:"PassengerID,omitempty" json:"passenger_id,omitempty" bson:"passenger_id,omitempty"`
	CreatedAgentCode                  string                     `xml:"CreatedAgentCode,omitempty" json:"create_agent_code,omitempty" bson:"create_agent_code,omitempty"`
	CreatedOrganizationCode           string                     `xml:"CreatedOrganizationCode,omitempty" json:"create_organization_code,omitempty" bson:"create_organization_code,omitempty"`
	CreatedDomainCode                 string                     `xml:"CreatedDomainCode,omitempty" json:"created_domain_code,omitempty" bson:"created_domain_code,omitempty"`
	CreatedLocationCode               string                     `xml:"CreatedLocationCode,omitempty" json:"created_locatoion_code,omitempty" bson:"created_locatoion_code,omitempty"`
	SourceAgentCode                   string                     `xml:"SourceAgentCode,omitempty" json:"source_agent_code,omitempty" bson:"source_agent_code,omitempty"`
	SourceOrganizationCode            string                     `xml:"SourceOrganizationCode,omitempty" json:"source_organization_code,omitempty" bson:"source_organization_code,omitempty"`
	SourceDomainCode                  string                     `xml:"SourceDomainCode,omitempty" json:"source_domain_code,omitempty" bson:"source_domain_code,omitempty"`
	SourceLocationCode                string                     `xml:"SourceLocationCode,omitempty" json:"source_location_code,omitempty" bson:"source_location_code,omitempty"`
	CreatedAgentID                    uint64                     `xml:"CreatedAgentID,omitempty" json:"created_agent_id,omitempty" bson:"created_agent_id,omitempty"`
	CreatedDate                       string                     `xml:"CreatedDate,omitempty" json:"created_date,omitempty" bson:"created_date,omitempty"`
	ModifiedAgentID                   uint64                     `xml:"ModifiedAgentID,omitempty" json:"modified_agent_id,omitempty" bson:"modified_agent_id,omitempty"`
	ModifiedDate                      string                     `xml:"ModifiedDate,omitempty" json:"modified_date,omitempty" bson:"modified_date,omitempty"`
	BookingComponentCharges           bookingComponentChargesODT `xml:"BookingComponentCharges,omitempty" json:"booking_component_charges,omitempty" bson:"booking_component_charges,omitempty"`
	Historical                        bool                       `xml:"Historical,omitempty" json:"historycal,omitempty" bson:"historycal,omitempty"`
	OrderItem                         orderItemODT               `xml:"OrderItem,omitempty" json:"order_item,omitempty" bson:"order_item,omitempty"`
	DeclinedText                      string                     `xml:"DeclinedText,omitempty" json:"declined_text,omitempty" bson:"declined_text,omitempty"`
	CultureCode                       string                     `xml:"CultureCode,omitempty" json:"culture_code,omitempty" bson:"culture_code,omitempty"`
	OtherServiceInfo                  otherServiceInformationODT `xml:"OtherServiceInfo,omitempty" json:"other_service_info,omitempty" bson:"other_service_info,omitempty"`
	TravelCommTravelCommerceOrderItem tcOrderItemODT             `xml:"TravelCommerceOrderItem,omitempty" json:"travel_commerce_order_item,omitempty" bson:"travel_commerce_order_item,omitempty"`
}

type bookingComponentsODT struct {
	BookingComponent []bookingComponentODT `xml:"BookingComponent,omitempty" json:"booking_component,omitempty" bson:"booking_component,omitempty"`
}

type bookingInfoResponseBodyODT struct {
	Fault faultODT                   `xml:"Fault,omitempty" json:"fault,omitempty" bson:"fault,omitempty"`
	Data  bookingInfoResponseDataODT `xml:"GetBookingResponse" json:"data" bson:"data"`
}

type bookingInfoResponseEnvelopeODT struct {
	XMLName xml.Name                   `xml:"Envelope"`
	Header  string                     `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    bookingInfoResponseBodyODT `xml:"Body" json:"body" bson:"body"`
}

type agencyAccountODT struct {
	AccountID            uint64 `xml:"AccountID,omitempty" json:"account_id,omitempty" bson:"account_id,omitempty"`
	AccountTransactionID uint64 `xml:"AccountTransactionID,omitempty" json:"account_transaction_id,omitempty" bson:"account_transaction_id,omitempty"`
	Password             string `xml:"Password,omitempty" json:"password,omitempty" bson:"password,omitempty"`
}

type creditShellODT struct {
	agencyAccountODT
	AccountTransactionCode string `xml:"AccountTransactionCode,omitempty" json:"account_transaction_code,omitempty" bson:"account_transaction_code,omitempty"`
}

type creditFileODT struct {
	agencyAccountODT
	RecordLocator string `xml:"RecordLocator,omitempty" json:"record_locator,omitempty" bson:"record_locator,omitempty"`
}

type paymentVoucherODT struct {
	VoucherIDField              uint64 `xml:"VoucherIDField,omitempty" json:"voucher_id_field,omitempty" bson:"voucher_id_field,omitempty"`
	VoucherTransaction          uint64 `xml:"VoucherTransaction,omitempty" json:"voucher_transaction,omitempty" bson:"voucher_transaction,omitempty"`
	OverrideVoucherRestrictions bool   `xml:"OverrideVoucherRestrictions,omitempty" json:"override_voucher_restrictions,omitempty" bson:"override_voucher_restrictions,omitempty"`
	OverrideAmount              bool   `xml:"OverrideAmount,omitempty" json:"override_amount,omitempty" bson:"override_amount,omitempty"`
	RecordLocator               string `xml:"RecordLocator,omitempty" json:"record_locator,omitempty" bson:"record_locator,omitempty"`
}

type threeDSecureRequestODT struct {
	BrowserUserAgent string `xml:"BrowserUserAgent,omitempty" json:"browser_user_agent,omitempty" bson:"browser_user_agent,omitempty"`
	BrowserAccept    string `xml:"BrowserAccept,omitempty" json:"browser_accept,omitempty" bson:"browser_accept,omitempty"`
	RemoteIPAddress  string `xml:"RemoteIpAddress,omitempty" json:"remote_ip_address,omitempty" bson:"remote_ip_address,omitempty"`
	TermURL          string `xml:"TermUrl,omitempty" json:"term_url,omitempty" bson:"term_url,omitempty"`
	ProxyVia         string `xml:"ProxyVia,omitempty" json:"proxy_via,omitempty" bson:"proxy_via,omitempty"`
}

type mccRequestODT struct {
	MCCInUse              bool          `xml:"MCCInUse,omitempty" json:"mcc_in_use,omitempty" bson:"mcc_in_use,omitempty"`
	CollectedCurrencyCode string        `xml:"CollectedCurrencyCode,omitempty" json:"collected_currency_code,omitempty" bson:"collected_currency_code,omitempty"`
	CollectedAmount       prettyFloat64 `xml:"CollectedAmount,omitempty" json:"collected_amount,omitempty" bson:"collected_amount,omitempty"`
}

type addPaymentToBookingRequestDataODT struct {
	MessageState string `xml:"MessageState,omitempty" json:"message_state,omitempty" bson:"message_state,omitempty"` /* New
	Clean Modified Deleted Confirmed Unmapped */
	WaiveFee      bool   `xml:"WaiveFee,omitempty" json:"waive_fee,omitempty" bson:"waive_fee,omitempty"`
	ReferenceType string `xml:"ReferenceType,omitempty" json:"reference_type,omitempty" bson:"reference_type,omitempty"` /*
		Default Booking Session Unmapped
	*/
	PaymentMethodType string `xml:"PaymentMethodType,omitempty" json:"payment_method_type,omitempty" bson:"payment_method_type,omitempty"` /*
		PrePaid ExternalAccount AgencyAccount CustomerAccount Voucher Loyalty Unmapped */
	PaymentMethodCode  string        `xml:"PaymentMethodCode,omitempty" json:"payment_method_code,omitempty" bson:"payment_method_code,omitempty"`
	QuotedCurrencyCode string        `xml:"QuotedCurrencyCode,omitempty" json:"quoted_currency_code,omitempty" bson:"quoted_currency_code,omitempty"`
	QuotedAmount       prettyFloat64 `xml:"QuotedAmount,omitempty" json:"quoted_amount,omitempty" bson:"quoted_amount,omitempty"`
	Status             string        `xml:"Status,omitempty" json:"status,omitempty" bson:"status,omitempty"` /* New Received
	Pending Approved Declined Unknown PendingCustomerAction Unmapped */
	AccountNumberID     uint64                 `xml:"AccountNumberID,omitempty" json:"account_number_id,omitempty" bson:"account_number_id,omitempty"`
	AccountNumber       string                 `xml:"AccountNumber,omitempty" json:"account_number,omitempty" bson:"account_number,omitempty"`
	Expiration          string                 `xml:"Expiration,omitempty" json:"expiration,omitempty" bson:"expiration,omitempty"`
	ParentPaymentID     uint64                 `xml:"ParentPaymentID,omitempty" json:"parent_payment_id,omitempty" bson:"parent_payment_id,omitempty"`
	Installments        uint16                 `xml:"Installments,omitempty" json:"installments,omitempty" bson:"installments,omitempty"`
	PaymentText         string                 `xml:"PaymentText,omitempty" json:"payment_text,omitempty" bson:"payment_text,omitempty"`
	Deposit             bool                   `xml:"Deposit,omitempty" json:"deposit,omitempty" bson:"deposit,omitempty"`
	PaymentFields       paymentFieldsODT       `xml:"PaymentFields,omitempty" json:"payment_fields,omitempty" bson:"payment_fields,omitempty"`
	PaymentAddresses    paymentAddressesODT    `xml:"PaymentAddresses,omitempty" json:"payment_addresses,omitempty" bson:"payment_addresses,omitempty"`
	AgencyAccount       agencyAccountODT       `xml:"AgencyAccount,omitempty" json:"agency_count,omitempty" bson:"agency_count,omitempty"`
	CreditShell         creditShellODT         `xml:"CreditShell,omitempty" json:"credit_shell,omitempty" bson:"credit_shell,omitempty"`
	CreditFile          creditFileODT          `xml:"CreditFile,omitempty" json:"credit_file,omitempty" bson:"credit_file,omitempty"`
	PaymentVoucher      paymentVoucherODT      `xml:"PaymentVoucher,omitempty" json:"payment_voucher,omitempty" bson:"payment_voucher,omitempty"`
	ThreeDSecureRequest threeDSecureRequestODT `xml:"ThreeDSecureRequest,omitempty" json:"three_d_secure_request,omitempty" bson:"three_d_secure_request,omitempty"`
	MCCRequest          mccRequestODT          `xml:"MCCRequest,omitempty" json:"mcc_request,omitempty" bson:"mcc_request,omitempty"`
	AuthorizationCode   string                 `xml:"AuthorizationCode,omitempty" json:"authorization_code,omitempty" bson:"authorization_code,omitempty"`
}

type addPaymentRequestODT struct {
	DetailData addPaymentToBookingRequestDataODT `xml:"http://schemas.navitaire.com/WebServices/DataContracts/Booking addPaymentToBookingReqData" json:"detail_data" bson:"detail_data"`
}

type addPaymentRequestBodyODT struct {
	Data addPaymentRequestODT `xml:"http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService AddPaymentToBookingRequest" json:"get_booking_request_data" bson:"get_booking_request_data"`
}

type addPaymentRequestEnvelopeODT struct {
	XMLName xml.Name                 `xml:"http://schemas.xmlsoap.org/soap/envelope/ Envelope"`
	Header  requestHeaderODT         `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    addPaymentRequestBodyODT `xml:"Body" json:"body" bson:"body"`
}

type bookingPaymentResponseODT struct {
	Success                  successODT                 `xml:"Success,omitempty" json:"success,omitempty" bson:"success,omitempty"`
	Warning                  warningODT                 `xml:"Warning,omitempty" json:"warning,omitempty" bson:"warning,omitempty"`
	Error                    errorODT                   `xml:"Error,omitempty" json:"error,omitempty" bson:"error,omitempty"`
	Booking                  bookingODT                 `xml:"Booking,omitempty" json:"booking,omitempty" bson:"booking,omitempty"`
	OtherServiceInformations otherServiceInformationODT `xml:"OtherServiceInformations,omitempty" json:"other_service_informations,omitempty" bson:"other_service_informations,omitempty"`
}

type otherServiceInformationsODT struct {
	OtherServiceInformation []otherServiceInformationODT `xml:"OtherServiceInformation,omitempty" json:"other_service_information,omitempty" bson:"other_service_information,omitempty"`
}

type paymentValidationErrorODT struct {
	ErrorType string `xml:"ErrorType,omitempty" json:"error_type" bson:"error_type"` /*
	   Unknown Other AccountNumber Amount ExpirationDate RestrictionHours
	   MissingAccountNumber MissingExpirationDate PaymentSystemUnavailable
	   MissingParentPaymentID InProcessPaymentChanged InvalidNumberOfInstallments
	   CreditShellCommentRequired NoQuotedCurrencyProvided NoBaseCurrencyForBooking
	   QuotedCurrencyDoesNotMatchBaseCurrency QuotedRefundAmountNotLessThanZero
	   QuotedPaymentAmountIsLessThanZero RoleCodeNotFound
	   UnknownOrInactivePaymentMethod DepositPaymentsNotAllowedForPaymentMethod
	   UnableToRetrieveRoleCodeSettings DepositPaymentsNotAllowedForRole
	   PaymentMethodNotAllowedForRole InvalidAccountNumberLength
	   PaymentTextIsRequired InvalidPaymentTextLength
	   InvalidMiscPaymentFieldLength MiscPaymentFieldRequired
	   BookingCurrencyIsInvalidForSkyPay SkyPayExceptionThrown
	   InvalidAccountNumberForPaymentMethod InvalidELVTransaction
	   BlackListedCard InvalidPaymentAddress InvalidSecurityCode
	   InvalidCurrencyCode InvalidAmount PossibleFraud InvalidCustomerAccount
	   AccountHolderIsNotAnAgency InvalidStartDate InvalidInitialPaymentStatus
	   PaymentCurrencyMustMatchBookingCurrency CollectedAmountMustMatchPaymentAmount
	   RefundsNotAllowedUsingThisPaymentMethod CreditShellAmountGreaterThanOrEqualToZero
	   CreditFileAmountLessThanOrEqualToZero InvalidPrepaidApprovalCodeLength
	   AccountNumberFailedModulousCheck NoExternalRatesAvailable
	   ExternalCurrencyConversion StoredCardSecurityViolation
	*/
	ErrorDescription string `xml:"ErrorDescription,omitempty" json:"error_description,omitempty" bson:"error_description,omitempty"`
	AttributeName    string `xml:"AttributeName,omitempty" json:"attribute_name,omitempty" bson:"attribute_name,omitempty"`
}

type paymentValidationErrorsODT struct {
	PaymentValidationError []paymentValidationErrorODT `xml:"PaymentValidationError,omitempty" json:"payment_validation_error,omitempty" bson:"payment_validation_error,omitempty"`
}

type validationPaymentODT struct {
	Payment                 paymentODT                 `xml:"Payment,omitempty" json:"payment,omitempty" bson:"payment,omitempty"`
	PaymentValidationErrors paymentValidationErrorsODT `xml:"PaymentValidationErrors,omitempty" json:"payment_validation_errors,omitempty" bson:"payment_validation_errors,omitempty"`
}

type addPaymentToBookingResponseDataODT struct {
	OtherServiceInfoList otherServiceInformationsODT `xml:"OtherServiceInfoList,omitempty" json:"other_service_info_lists,omitempty" bson:"other_service_info_lists,omitempty"`
	ValidationPayment    validationPaymentODT        `xml:"ValidationPayment,omitempty" json:"validation_payment,omitempty" bson:"validation_payment,omitempty"`
}

type addPaymentToBookingResponseODT struct {
	BookingPaymentResponse addPaymentToBookingResponseDataODT `xml:"BookingPaymentResponse,omitempty" json:"booking_payment_response,omitempty" bson:"booking_payment_response,omitempty"`
}

type addPaymentResponseBodyODT struct {
	Fault faultODT                       `xml:"Fault,omitempty" json:"fault,omitempty" bson:"fault,omitempty"`
	Data  addPaymentToBookingResponseODT `xml:"AddPaymentToBookingResponse" json:"data" bson:"data"`
}

type addPaymentResponseEnvelopeODT struct {
	XMLName xml.Name                  `xml:"Envelope"`
	Header  string                    `xml:"Header,omitempty" json:"-" bson:"-"`
	Body    addPaymentResponseBodyODT `xml:"Body" json:"body" bson:"body"`
}
