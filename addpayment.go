package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func addPayment(connectionParams *connectionParamsODT, inputSignature string,
	bookingInfo bookingInfoResponseEnvelopeODT,
	xmlInLog bool, xmlOutLog bool) (string, string) {

	apiResponse := &apiBasicResponseODT{APIVersion: VersionString}
	signature := inputSignature
	if signature == "" {
		tmpSignature, errCode, errString, tmpConnectionParams := getSignature(connectionParams)
		connectionParams = tmpConnectionParams
		if errString != "" {
			apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", errString)
			apiResponse.ResultCode = int16(errCode)
			apiResponse.Status = apiResponse.ResultCode == 0
			apiResponse.ResultString = fmt.Sprintln("Error signature")
			byteData, _ := json.Marshal(apiResponse)
			return "", string(byteData)
		}
		signature = tmpSignature

	}

	partnerRequest := addPaymentRequestEnvelopeODT{Header: requestHeaderODT{ContractVersion: ContractVersion, Signature: signature}}
	partnerData := &partnerRequest.Body.Data.DetailData
	partnerData.MessageState = "New"
	partnerData.ReferenceType = "Session"
	partnerData.PaymentMethodType = "AgencyAccount"
	partnerData.PaymentMethodCode = "AG"
	bookingData := bookingInfo.Body.Data.Booking
	partnerData.QuotedCurrencyCode = bookingData.CurrencyCode
	partnerData.QuotedAmount = bookingData.BookingSum.BalanceDue
	partnerData.Status = "New"
	partnerData.AccountNumber = connectionParams.OrganizationCode //bookingData.POS.OrganizationCode
	cmd := "add_payment"

	soapCmd, errString := getCmdData(connectionParams, cmd)
	if errString != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Error get command data-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error get connection data")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	if soapCmd == nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Command data not found-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Connection data not found")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	byteData, _ := xml.MarshalIndent(partnerRequest, " ", " ")
	if xmlOutLog {
		log.Println(getLine(), "To Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	req, err := http.NewRequest("POST", soapCmd.URL, bytes.NewReader(byteData))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error preparing request")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	req.Header.Add("Content-Type", "text/xml")
	req.Header.Add("SOAPAction", soapCmd.ActionHeader)
	client := &http.Client{}
	log.Println(getLine(), "Send add payment request")
	resp, err := client.Do(req)
	log.Println(getLine(), "Receive add payment from response")

	//	log.Println(getLine(), string(bodyRequest))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error during sending request")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}
	defer resp.Body.Close()
	byteData, _ = ioutil.ReadAll(resp.Body)
	var partnerResponse addPaymentResponseEnvelopeODT
	err = xml.Unmarshal(byteData, &partnerResponse)
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error(), "\n", string(byteData))
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error decoding response")
		byteData, _ := json.Marshal(apiResponse)
		if xmlInLog {
			log.Println(getLine(), "From Partner ")
			log.Println(getLine(), string(byteData))
			log.Println()
		}

		return "", string(byteData)
	}

	byteData, _ = xml.MarshalIndent(partnerResponse, " ", " ")
	if xmlInLog {
		log.Println(getLine(), "From Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	if partnerResponse.Body.Fault.FaultCode != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", partnerResponse.Body.Fault.FaultCode, "-", partnerResponse.Body.Fault.FaultString)
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Partner error: ", partnerResponse.Body.Fault.FaultString)
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	paymentValidations := partnerResponse.Body.Data.BookingPaymentResponse.ValidationPayment.PaymentValidationErrors.PaymentValidationError
	if len(paymentValidations) > 0 {
		apiResponse.ResultString = fmt.Sprintln("Partner error: ", paymentValidations[0].ErrorType)
		byteData, _ := xml.Marshal(paymentValidations)
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", string(byteData))
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		byteData, _ = json.Marshal(apiResponse)
		return "", string(byteData)

	}
	return string(byteData), ""
}
