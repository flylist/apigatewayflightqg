package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func flight(w http.ResponseWriter, r *http.Request) {
	//    ip, port, err := net.SplitHostPort(r.RemoteAddr)

	log.Println(getLine(), "\n-------------------")
	log.Println(getLine(), r.RemoteAddr, " Start Citlink API Request")
	defer printEnd(fmt.Sprint(getLine(), r.RemoteAddr, " End Citilink API Request"))
	w.Header().Add("Content-Type", "application/json")
	/*		w.Header().Add("Access-Control-Allow-Methods", "POST,GET,HEAD")
			w.Header().Add("Access-Control-Allow-Origin", "*")
			request_header := r.Header.Get("Access-Control-Request-Headers")
			if request_header != "" {
				w.Header().Add("Access-Control-Allow-Headers", request_header)
			}
	*/
	var apiRequest apiBasicRequest
	var requestString string
	var responseString string

	connectionParams, _ := readConnectionParams("")
	reqBody, _ := ioutil.ReadAll(r.Body)
	requestString = string(reqBody)
	err := json.Unmarshal(reqBody, &apiRequest)
	if err != nil {
		line := getLine()
		errorResponse := &apiBasicResponseODT{APIVersion: VersionString}
		errorResponse.ResultCode = 100
		errorResponse.Status = false
		errorResponse.ErrorString = fmt.Sprint(line, " error decoding request ", err.Error())
		errorResponse.ResultString = "Error decoding request"
		jsonText, _ := json.Marshal(&errorResponse)
		fmt.Fprintf(w, "%s", string(jsonText))
		return

	}
	switch apiRequest.Product {
	case "FLIGHT":
		switch apiRequest.Command {
		case "SCHEDULE":
			//		byteString, _ := json.MarshalIndent(connectionParams, " ", " ")
			//		log.Println(getLine(), string(byteString))
			responseString = getFlightAvailability(connectionParams, requestString)
		case "BOOKING":
			responseString = booking(connectionParams, requestString)
		case "CONFIRM":
			responseString = confirm(connectionParams, requestString)
		default:
			errorResponse := &apiBasicResponseODT{
				APIVersion:   VersionString,
				ResultString: fmt.Sprint(apiRequest.Command, " : invalid command"),
				ResultCode:   100,
				ErrorString:  fmt.Sprint(getLine(), apiRequest.Command, ":invalid command"),
			}
			jsonText, _ := json.Marshal(&errorResponse)
			responseString = string(jsonText)
		}

	default:
		errorResponse := &apiBasicResponseODT{
			APIVersion:   VersionString,
			ResultString: fmt.Sprint(apiRequest.Product, " : invalid product"),
			ResultCode:   100,
			ErrorString:  fmt.Sprint(getLine(), apiRequest.Product, ":invalid product"),
		}
		jsonText, _ := json.Marshal(&errorResponse)
		responseString = string(jsonText)
		break
	}
	fmt.Fprintf(w, "%s", responseString)
}

func config(w http.ResponseWriter, r *http.Request) {
	//    ip, port, err := net.SplitHostPort(r.RemoteAddr)

	log.Println(getLine(), "\n-------------------")
	log.Println(getLine(), r.RemoteAddr, "Start Citlink API Request")
	defer printEnd(fmt.Sprint(getLine(), r.RemoteAddr, "End Citilink API Request"))
	w.Header().Add("Content-Type", "application/json")
	/*		w.Header().Add("Access-Control-Allow-Methods", "POST,GET,HEAD")
			w.Header().Add("Access-Control-Allow-Origin", "*")
			request_header := r.Header.Get("Access-Control-Request-Headers")
			if request_header != "" {
				w.Header().Add("Access-Control-Allow-Headers", request_header)
			}
	*/
	var apiRequest apiBasicRequest
	var responseString string

	reqBody, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(reqBody, &apiRequest)
	if err != nil {
		line := getLine()
		errorResponse := &apiBasicResponseODT{APIVersion: VersionString}
		errorResponse.ResultCode = 100
		errorResponse.Status = false
		errorResponse.ErrorString = fmt.Sprint(line, " error decoding request ", err.Error())
		errorResponse.ResultString = "Error decoding request"
		jsonText, _ := json.Marshal(&errorResponse)
		fmt.Fprintf(w, "%s", string(jsonText))
		return

	}
	switch apiRequest.Product {
	case "CONFIG":
		if apiRequest.Command == "REFRESH" {
			responseString = refreshRedis(r.RemoteAddr)
		} else {
			errorResponse := &apiBasicResponseODT{
				APIVersion:   VersionString,
				ResultString: fmt.Sprint(apiRequest.Command, " : invalid command"),
				ResultCode:   100,
				ErrorString:  fmt.Sprint(getLine(), apiRequest.Command, ":invalid command"),
			}
			jsonText, _ := json.Marshal(&errorResponse)
			responseString = string(jsonText)
		}
	default:
		errorResponse := &apiBasicResponseODT{
			APIVersion:   VersionString,
			ResultString: fmt.Sprint(apiRequest.Product, " : invalid product"),
			ResultCode:   100,
			ErrorString:  fmt.Sprint(getLine(), apiRequest.Product, ":invalid product"),
		}
		jsonText, _ := json.Marshal(&errorResponse)
		responseString = string(jsonText)
		break
	}
	fmt.Fprintf(w, "%s", responseString)
}
