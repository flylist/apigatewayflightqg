package main

import (
	"encoding/json"
	"strings"
)

/*
v2.1
Print request detail for tracing
*/

func refreshRedis(remoteIPPort string) string {
	apiResponse := &apiBasicResponseODT{APIVersion: VersionString, ResultCode: 0, Status: true}
	remoteIP := ""
	if strings.Contains(remoteIPPort, "]") {
		remoteIP = strings.Split(remoteIPPort, "]")[0]
		remoteIP = strings.TrimPrefix(remoteIP, "[")
	} else {
		remoteIP = strings.Split(remoteIPPort, ":")[0]
	}
	if remoteIP != "127.0.0.1" && remoteIP != "::1" {
		apiResponse.ErrorString = getLine() + "-Unauthorized access from ip " + remoteIP
		apiResponse.ResultString = "Unauthorized access from ip " + remoteIP
		apiResponse.ResultCode = 100
		apiResponse.Status = false
		jsonText, _ := json.Marshal(&apiResponse)
		responseString := string(jsonText)
		return responseString

	}
	connectionParams, errString := readConnectionParamsFromMongoDB()
	if connectionParams != nil {
		writeConnectionParamsToRedis(connectionParams)
	} else {
		apiResponse.ErrorString = getLine() + "-Error reading data from DB " + errString
		apiResponse.ResultString = "Error reading data from DB " + errString
		apiResponse.ResultCode = 100
		apiResponse.Status = false
	}
	jsonText, _ := json.Marshal(&apiResponse)
	responseString := string(jsonText)
	return responseString
}
