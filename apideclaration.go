package main

type tracingHeader struct {
	TraceInReq        bool `json:"trace_in_req,omitempty" bson:"-"`
	TraceOutReq       bool `json:"trace_out_req,omitempty" bson:"-"`
	TraceInStringReq  bool `json:"trace_in_string_req,omitempty" bson:"-"`
	TraceOutStringReq bool `json:"trace_out_string_req,omitempty" bson:"-"`
}

type apiBasicRequest struct {
	tracingHeader
	Command string `json:"command" bson:"command"`
	Product string `json:"product" bson:"product"`
}

type apiBasicResponseODT struct {
	APIVersion   string `json:"version"`
	Status       bool   `json:"status" bson:"status"`
	ResultCode   int16  `json:"error_code"`   // 1xx --> internal error, 2xx partner error
	ResultString string `json:"error_msg"`    // line number - error message
	ErrorString  string `json:"error_string"` // detail message
}

type apiScheduleRequestODT struct {
	apiBasicRequest
	Data struct {
		DepartureCode    string `json:"departure_code" bson:"departure_code"` // "CGK",
		ArrivalCode      string `json:"arrival_code" bson:"arrival_code"`     // "DPS",
		DepartureDate    string `json:"departure_date" bson:"departure_date"` // "2019-09-16",
		EndDepartureDate string `json:"end_departure_date,omitempty" bson:"end_departure_date,omitempty"`
		Adult            uint8  `json:"adult" bson:"adult"`             // 1,
		Child            uint8  `json:"child" bson:"child"`             // 1,
		Infant           uint8  `json:"infant" bson:"infant"`           // 1,
		Return           bool   `json:"return" bson:"return"`           // true,
		ReturnDate       string `json:"return_date" bson:"return_date"` // "2019-09-17",
		EndReturnDate    string `json:"end_return_date,omitempty" bson:"end_return_date,omitempty"`
		SeatClass        string `json:"seatClass" bson:"seat_class"` // "Economy"
	} `json:"data" bson:"data"`
}

type apiScheduleDetailODT struct {
	AirlinesName                 string  `json:"airlines_name" bson:"airlines_name"`                                       // "CITILINK",
	AirlinesShortRealName        string  `json:"airlines_short_real_name" bson:"airlines_short_real_name"`                 // "CITILINK",
	AirlinesShortRealNameUCWords string  `json:"airlines_short_real_name_ucwords" bson:"airlines_short_real_name_ucwords"` // "Citilink",
	AirportTax                   bool    `json:"airport_tax" bson:"airport_tax"`                                           // false,
	ArrivalAirportName           string  `json:"arrival_airport_name" bson:"arrival_airport_name"`                         // "Soekarno Hatta Intl Airport",
	ArrivalCity                  string  `json:"arrival_city" bson:"arrival_city"`                                         // "CGK",
	ArrivalCityName              string  `json:"arrival_city_name" bson:"arrival_city_name"`                               // "Jakarta",
	ArrivalDateTime              string  `json:"arrival_date_time" bson:"arrival_date_time"`                               // "2019-09-17 21:45:00",
	CheckInBaggage               float32 `json:"check_in_baggage" bson:"check_in_baggage"`                                 // 20,
	CheckInBaggageUnit           string  `json:"check_in_baggage_unit" bson:"check_in_baggage_unit"`                       // "KG",
	Class                        string  `json:"class" bson:"class"`                                                       // "T",
	ClassW                       string  `json:"class_w" bson:"class_w"`                                                   // "T",
	DepartureAirportName         string  `json:"departure_airport_name" bson:"departure_airport_name"`                     // "Ngurah Rai Int'l",
	DepartureCity                string  `json:"departure_city" bson:"departure_city"`                                     // "DPS",
	DepartureCityName            string  `json:"departure_city_name" bson:"departure_city_name"`                           // "Bali /Denpasar",
	DepartureDateTime            string  `json:"departure_date_time" bson:"departure_date_time"`                           // "2019-09-17 20:45:00",
	DurationHour                 string  `json:"duration_hour" bson:"duration_hour"`                                       // "2j",
	DurationMinute               string  `json:"duration_minute" bson:"duration_minute"`                                   // "0m",
	DurationTime                 uint16  `json:"duration_time" bson:"duration_minute"`                                     // 120,
	FlightNumber                 string  `json:"flight_number" bson:"flight_number"`                                       // "QG-689",
	HasFood                      string  `json:"has_food" bson:"has_food"`                                                 // "0",
	ImgSrc                       string  `json:"img_src" bson:"img_"`                                                      // "https://traveloka.s3.amazonaws.com/imageResource/2015/12/17/1450350561012-6584b693edd67d75cfc25ecff41c5704.png",
	OperatedBy                   string  `json:"operated_by" bson:"operated_by"`                                           // "Citilink",
	ServiceClass                 string  `json:"service_class" bson:"service_class"`                                       // "ECONOMY",
	SimpleArrivalTime            string  `json:"simple_arrival_time" bson:"simple_arrival_time"`                           // "21:45",
	SimpleDepartureTime          string  `json:"simple_departure_time" bson:"simple_departure_time"`                       //"20:45",
	StringArrivalDate            string  `json:"string_arrival_date" bson:"string_arrival_date"`                           // "Selasa, 17 Sep 2019",
	StringArrivalDateShort       string  `json:"string_arrival_date_short" bson:"string_arrival_date_short"`               // "Sel, 17 Sep 2019",
	StringDepartureDate          string  `json:"string_departure_date" bson:"string_departure_date"`                       // "Selasa, 17 Sep 2019",
	StringDepartureDateShort     string  `json:"string_departure_date_short" bson:"string_departure_date_short"`           // "Sel, 17 Sep 2019",
	Terminal                     string  `json:"terminal" bson:"terminal"`                                                 // "Domestic",
	TransitArrivalTextCity       string  `json:"transit_arrival_text_city" bson:"transit_arrival_text_city"`               // "",
	TransitArrivalTexttime       string  `json:"transit_arrival_text_time" bson:"transit_arrival_text_time"`               // "",
	TransitDurationHour          uint16  `json:"transit_duration_hour" bson:"transit_duration_hour"`                       // 0,
	TransitDurationMinute        uint16  `json:"transit_duration_minute" bson:"transit_duration_minute"`                   // 0
}

type apiFlightScheduleODT struct {
	ArrivalDate   string                 `json:"arrival_date" bson:"arrival_date"`     // "2019-09-17 21:45:00"
	ArrivalTime   string                 `json:"arrival_time" bson:"arrival_time"`     // "21:45",
	DepartureDate string                 `json:"departure_date" bson:"departure_date"` // "2019-09-17 20:45:00",
	DepartureTime string                 `json:"departure_time" bson:"departure_time"` // "20:45",
	Detail        []apiScheduleDetailODT `json:"detail" bson:"detail"`
	Duration      string                 `json:"duration" bson:"duration"`           // "2 j 0 m",
	FlightNumber  string                 `json:"flight_number" bson:"flight_number"` // "QG-689",
	FullVia       string                 `json:"full_via" bson:"full_via"`           // "DPS - CGK (20:45 - 21:45)",
	Name          string                 `json:"name" bson:"name"`                   // "Citilink",
	PriceAdult    float64                `json:"price_adult" bson:"price_adult"`     // 1252500,
	PriceChild    float64                `json:"price_child" bson:"price_child"`     // 0,
	PriceInfant   float64                `json:"price_infant" bson:"price_infant"`   // 0,
	PriceSRC      float64                `json:"price_src" bson:"price_src"`
	ScheduleID    string                 `json:"schedule_id" bson:"schedule_id"` // "+ulLuH+fsidu6lov70BJpQda63w324xRsIuzZgmYoCSmJ6ePbfJ+Y4e6AG7nW8vjW+NaOdOsixa6bwPHQ4v0CQMdqOfU7+jDxf1Cy5hypMg6QW2yOzGmRsRPqQsv2Hhgk9IxtEl0QaIc89c4NQvPMYzgSzsvHjgveMOojaAx0j1i1TX4v2AI+CjHKJZ258LU9xWQ0XqDZDDjPqYeXnWNrNSksXWHPJwNOEOErbS3uOg=",
	Stop          string                 `json:"stop" bson:"stop"`               // "Langsung"
}

type apiAvailabilityDataODT struct {
	Departures []apiFlightScheduleODT `json:"departures" bson:"departures"`
	Returns    []apiFlightScheduleODT `json:"returns" bson:"returns"`
}

type apiAvailabilityResponseODT struct {
	apiBasicResponseODT
	Data apiAvailabilityDataODT `json:"data" bson:"data"`
}

type apiContactDetailODT struct {
	Title         string `json:"salutation" bson:"salutation"`
	Fullname      string `json:"fullname" bson:"fullname"`
	FirstName     string `json:"first_name" bson:"first_name"`
	LastName      string `json:"last_name" bson:"last_name"`
	Email         string `json:"email" bson:"email"`
	Phone         string `json:"phone" bson:"phone"`
	AddressLine1  string `json:"address_line_1" bson:"address_line_1"`
	AddressLine2  string `json:"address_line_2" bson:"address_line_2"`
	AddressLine3  string `json:"address_line_3" bson:"address_line_3"`
	City          string `json:"city" bson:"city"`
	ProvinceState string `json:"province_state" bson:"province_state"`
	CountryCode   string `json:"country_code" bson:"country_code"`
}

type apiPaxDetailODT struct {
	FirstName    string `json:"first_name" bson:"first_name"`
	LastName     string `json:"last_name" bson:"last_name"`
	Email        string `json:"email" bson:"email"`
	Phone        string `json:"phone" bson:"phone"`
	BirthDate    string `json:"birth_date" bson:"birth_date"`
	Primary      bool   `json:"primary" bson:"primary"`
	Title        string `json:"salutation" bson:"salutation"`
	Type         string `json:"type" bson:"type"`
	Seat         string `json:"seat" bson:"seat"`
	Nationality  string `json:"nationality" bson:"nationality"`
	IDNumber     string `json:"card_number" bson:"card_number"`
	IDIssueDate  string `json:"card_issue_date" bson:"card_issue_date"`
	IDExpireDate string `json:"card_expire_date" bson:"card_expire_date"`
	Luggage      uint8  `json:"luggage" bson:"luggage"`
}

type apiBookingRequestODT struct {
	apiBasicRequest
	Data struct {
		DepartureCode    string              `json:"departure_code" bson:"departure_code"` // "CGK",
		ArrivalCode      string              `json:"arrival_code" bson:"arrival_code"`     // "DPS",
		DepartureDate    string              `json:"departure_date" bson:"departure_date"` // "2019-09-16",
		Adult            uint8               `json:"adult" bson:"adult"`                   // 1,
		Child            uint8               `json:"child" bson:"child"`                   // 1,
		Infant           uint8               `json:"infant" bson:"infant"`                 // 1,
		ScheduleID       string              `json:"schedule_id" bson:"schedule_id"`
		ReturnScheduleID string              `json:"return_schedule_id" bson:"return_schedule_id"`
		Class            string              `json:"class" bson:"class"`
		SubClass         string              `json:"sub_class" bson:"sub_class"`
		ContactDetail    apiContactDetailODT `json:"contact_detail" bson:"contact_detail"`
		Passengers       []apiPaxDetailODT   `json:"passengers" bson:"passengers"`
	} `json:"data" bson:"data"`
}

type apiBookingResponseDetailODT struct {
	AdultPrice     float64 `json:"adult_price,omitempty" bson:"adult_price,omitempty"`
	ChildPrice     float64 `json:"child_price,omitempty" bson:"child_price,omitempty"`
	InfantPrice    float64 `json:"infant_price,omitempty" bson:"infant_price,omitempty"`
	ArrivalCode    string  `json:"arrival_code,omitempty" bson:"arrival_code,omitempty"`
	ArrivalDate    string  `json:"arrival_date,omitempty" bson:"arrival_date,omitempty"`
	ArrivalName    string  `json:"arrival_name,omitempty" bson:"arrival_name,omitempty"`
	ArrivalTime    string  `json:"arrival_time,omitempty" bson:"arrival_time,omitempty"`
	BookingCode    string  `json:"booking_code,omitempty" bson:"booking_code,omitempty"`
	BookingExpire  string  `json:"booking_expire,omitempty" bson:"booking_expire,omitempty"`
	DepartureCode  string  `json:"departure_code,omitempty" bson:"departure_code,omitempty"`
	DepartureDate  string  `json:"departure_date,omitempty" bson:"departure_date,omitempty"`
	DepartureCName string  `json:"departure_name,omitempty" bson:"departure_name,omitempty"`
	DepartureTime  string  `json:"departure_time,omitempty" bson:"departure_time,omitempty"`
	AirlineName    string  `json:"name,omitempty" bson:"name,omitempty"`
	ScheduleID     string  `json:"schedule_id,omitempty" bson:"schedule_id,omitempty"`
}

type apiBookingResponseDataODT struct {
	AdminFee float64                       `json:"admin_fee,omitempty" bson:"admin_fee,omitempty"`
	Amount   float64                       `json:"amount,omitempty" bson:"amount,omitempty"`
	Details  []apiBookingResponseDetailODT `json:"details,omitempty" bson:"details,omitempty"`

	Discount float64 `json:"discount,omitempty" bson:"discount,omitempty"`
	Tax      float64 `json:"tax,omitempty" bson:"tax,omitempty"`
}

type apiBookingResponseODT struct {
	apiBasicResponseODT
	TrxID        uint64                    `json:"trx_id,omitempty" bson:"trx_id,omitempty"`
	PartnerTrxID string                    `json:"partner_trxid,omitempty" bson:"partner_trxid,omitempty"`
	Amount       float64                   `json:"amount,omitempty" bson:"amount,omitempty"`
	Total        float64                   `json:"total,omitempty" bson:"total,omitempty"`
	Data         apiBookingResponseDataODT `json:"data,omitempty" bson:"data,omitempty"`
}

type apiConfirmRequestODT struct {
	apiBasicRequest
	TrxID        uint64  `json:"trx_id,omitempty" bson:"trx_id,omitempty"`
	PartnerTrxID string  `json:"partner_trxid,omitempty" bson:"partner_trxid,omitempty"`
	Amount       float64 `json:"amount,omitempty" bson:"amount,omitempty"`
}

type apiConfirmResponseODT struct {
	apiBasicResponseODT
	Data interface{} `json:"data,omitempty" bson:"data,omitempty"`
}
