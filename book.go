package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"log"
	"strings"
	"time"
)

func sellConvertPartnerToAPI(signature string, departureCode string, partnerResponse availabilityResponseEnvelopeODT,
	apiResponse *apiAvailabilityResponseODT) {
	availibityByTrip := &partnerResponse.Body.Data.GetTripAvailabilityResponse
	schedules := availibityByTrip.Schedules
	apiResponse.Status = true
	arrAirportData := make(map[string]airportODT)

	aiportData := getAirportDetail(departureCode)
	if aiportData != nil {
		arrAirportData[departureCode] = *aiportData
	}

	for _, arrayOfJourneyMarket := range schedules.ArrayOfJourneyDateMarket {
		for _, dayOfJourney := range arrayOfJourneyMarket.JourneyDateMarket {
			journeys := dayOfJourney.Journeys.Journey
			var schedule apiFlightScheduleODT
			totalDuration := uint16(0)
			for _, journey := range journeys {
				for _, segment := range journey.Segments.Segment {
					td, _ := time.Parse("2006-01-02T15:04:05", segment.STD)
					schedule.DepartureDate = td.Format("2006-01-02 15:04")
					schedule.DepartureTime = td.Format("15:04")
					ta, _ := time.Parse("2006-01-02T15:04:05", segment.STA)
					schedule.ArrivalDate = ta.Format("2006-01-02 15:04")
					schedule.ArrivalTime = ta.Format("15:04")
					schedule.FlightNumber = ""
					schedule.Name = "Citilink"
					schedule.Detail = []apiScheduleDetailODT{}
					for _, leg := range segment.Legs.Leg {
						var scheduleDetail apiScheduleDetailODT
						scheduleDetail.FlightNumber = strings.Trim(leg.FlightDesignator.CarrierCode, " ") + "-" +
							strings.Trim(leg.FlightDesignator.FlightNumber, " ")
						if schedule.FlightNumber == "" {
							schedule.FlightNumber = scheduleDetail.FlightNumber
						}
						scheduleDetail.AirlinesName = "CITILINK"
						scheduleDetail.AirlinesShortRealName = "CITILINK"
						scheduleDetail.OperatedBy = "Citilink"
						tmpName := strings.ToLower(scheduleDetail.AirlinesShortRealName)
						scheduleDetail.AirlinesShortRealNameUCWords = strings.Title(tmpName)
						scheduleDetail.AirportTax = dayOfJourney.IncludeTaxesAndFees
						val, ok := arrAirportData[leg.ArrivalStation]
						if !ok {
							airportData := getAirportDetail(leg.ArrivalStation)
							if airportData != nil {
								arrAirportData[leg.ArrivalStation] = *airportData
								val = *airportData
								ok = true
							}
						}
						scheduleDetail.ArrivalCity = leg.ArrivalStation
						var arrivalTZ *time.Location
						var departureTZ *time.Location

						if ok {
							scheduleDetail.ArrivalCityName = val.CityName
							scheduleDetail.ArrivalAirportName = val.AirportName
							var err error
							arrivalTZ, err = time.LoadLocation(val.TimeZone)
							if err != nil {
								arrivalTZ = nil
								log.Println(getLine(), "Error loading timezone for ", val.ID, val.TimeZone)
							}
						}

						ta, _ := time.Parse("2006-01-02T15:04:05", leg.STA)
						scheduleDetail.ArrivalDateTime = ta.Format("2006-01-02 15:04")
						scheduleDetail.SimpleArrivalTime = ta.Format("15:04")
						td, _ := time.Parse("2006-01-02T15:04:05", leg.STD)
						scheduleDetail.DepartureDateTime = td.Format("2006-01-02 15:04")
						scheduleDetail.SimpleDepartureTime = td.Format("15:04")
						scheduleDetail.DepartureCity = leg.DepartureStation

						val, ok = arrAirportData[leg.DepartureStation]
						if !ok {
							airportData := getAirportDetail(leg.DepartureStation)
							if airportData != nil {
								arrAirportData[leg.DepartureStation] = *airportData
								val = *airportData
								ok = true
							}
						}

						if ok {
							scheduleDetail.DepartureCityName = val.CityName
							scheduleDetail.DepartureAirportName = val.AirportName
							var err error
							departureTZ, err = time.LoadLocation(val.TimeZone)
							if err != nil {
								departureTZ = nil
								log.Println(getLine(), "Error loading timezone for ", val.ID, val.TimeZone)
							}
						}

						if departureTZ != nil && arrivalTZ != nil {
							t0, _ := time.ParseInLocation("2006-01-02T15:04:05", leg.STD, departureTZ)
							t1, _ := time.ParseInLocation("2006-01-02T15:04:05", leg.STA, arrivalTZ)
							diff := t1.Sub(t0)
							scheduleDetail.DurationHour = fmt.Sprint(int(diff.Hours()))
							scheduleDetail.DurationMinute = fmt.Sprint(int(diff.Minutes()) % 60)
							scheduleDetail.DurationTime = uint16(diff.Minutes())
							totalDuration = totalDuration + scheduleDetail.DurationTime
						} else {
							totalDuration = 0
						}

						for _, fare := range segment.Fares.Fare {
							scheduleDetail.Class = fare.ClassOfService
							scheduleDetail.ClassW = fare.FareClassOfService
							for _, paxFare := range fare.PaxFares.PaxFare {
								for _, serviceCharge := range paxFare.ServiceCharges.BookingServiceCharge {
									var ptrFare *float64
									//								log.Println(getLine(), "Pax Type ", paxFare.PaxType, serviceCharge.Amount)
									switch paxFare.PaxType {
									case "INF":
										ptrFare = &schedule.PriceInfant
									case "SRC":
										ptrFare = &schedule.PriceSRC
									case "CHD":
										ptrFare = &schedule.PriceChild
									default:
										ptrFare = &schedule.PriceAdult
									}

									tmpFloat := float64(serviceCharge.Amount)
									if tmpFloat > *ptrFare {
										*ptrFare = tmpFloat
									}
								}
							}
							var tmpID searchIDODT
							tmpID.LogID = signature
							tmpID.FareSellKey = fare.FareSellKey
							tmpID.SegmentSellKey = segment.SegmentSellKey
							tmpID.JourneySellKey = journey.JourneySellKey
							byteString, _ := json.Marshal(tmpID)
							schedule.ScheduleID = fmt.Sprintf("%x", byteString)
							schedule.Detail = append(schedule.Detail, scheduleDetail)
							schedule.Duration = fmt.Sprint(totalDuration)
							totalDuration = 0
							//							log.Println(getLine(), "dc ", departureCode, " sdc ", segment.DepartureStation)
							if segment.DepartureStation == departureCode {
								apiResponse.Data.Departures = append(apiResponse.Data.Departures, schedule)
							} else {
								apiResponse.Data.Returns = append(apiResponse.Data.Returns, schedule)
							}
						}
					}
				}
			}

		}
	}
}

func booking(connectionParams *connectionParamsODT, stringRequest string) string {

	apiResponse := &apiBookingResponseODT{apiBasicResponseODT: apiBasicResponseODT{APIVersion: VersionString}}

	apiRequest := &apiBookingRequestODT{}
	err := json.Unmarshal([]byte(stringRequest), apiRequest)

	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error decoding request")
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	requestData := &apiRequest.Data
	adultPax := []apiPaxDetailODT{}
	childPax := []apiPaxDetailODT{}
	infantPax := []apiPaxDetailODT{}
	var paxData *[]apiPaxDetailODT

	xmlInLog := apiRequest.TraceInStringReq || connectionParams.LogInData
	xmlOutLog := apiRequest.TraceOutStringReq || connectionParams.LogOutData
	scheduleIDs := []string{requestData.ScheduleID}

	if requestData.ReturnScheduleID != "" {
		scheduleIDs = append(scheduleIDs, requestData.ReturnScheduleID)
	}

	// load wib location timezone
	wibLocation, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		log.Println(getLine(), "Error loading timezone location data for Asia/Jakarta ")
	}

	for _, pax := range requestData.Passengers {
		paxType := strings.ToLower(pax.Type)
		var paxTypeConverted string
		paxTypeConverted = paxTypes[paxType]
		if paxTypeConverted == "" {
			apiResponse.ErrorString = fmt.Sprintln(getLine(), "Invalid pax type : ", pax.Type)
			apiResponse.ResultCode = 100
			apiResponse.Status = false
			apiResponse.ResultString = "Invalid pax type : " + pax.Type + ". Allowable value : Adult,Infant,Child"
			byteData, _ := json.Marshal(apiResponse)
			return string(byteData)
		}
		paxDetail := apiPaxDetailODT{}
		byteData, _ := json.Marshal(pax)
		json.Unmarshal(byteData, &paxDetail)
		paxDetail.Type = paxTypeConverted
		switch paxTypeConverted {
		case "ADT":
			paxData = &adultPax
		case "CHD":
			paxData = &childPax
		case "INF":
			paxData = &infantPax
		}
		*paxData = append(*paxData, paxDetail)
	}

	if len(adultPax) < len(infantPax) {
		apiResponse.ResultString = "Number of adult must be at least equal to number of infant"
		apiResponse.ErrorString = fmt.Sprintln(getLine(), apiResponse.ResultString)
		apiResponse.ResultCode = 100
		apiResponse.Status = false
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}
	log.Println(getLine())
	resultString, errorString := sell(connectionParams, scheduleIDs, adultPax, childPax,
		xmlInLog, xmlOutLog)
	if errorString != "" {
		return errorString
	}

	if len(infantPax) > 0 {
		resultString, errorString = sellINF(connectionParams, scheduleIDs, infantPax,
			xmlInLog, xmlOutLog)
		if errorString != "" {
			return errorString
		}
	}

	// update contact data
	log.Println(getLine())

	resultString, errorString = updateContact(connectionParams, requestData.ScheduleID, requestData.ContactDetail,
		xmlInLog, xmlOutLog)
	if errorString != "" {
		log.Println(getLine(), errorString)
		return errorString
	}

	log.Println(getLine())
	resultString, errorString = updatePassenger(requestData.ScheduleID, adultPax, childPax, infantPax,
		xmlInLog, xmlOutLog)
	//	log.Println(getLine(), string(resultString))
	if errorString != "" {
		log.Println(getLine(), errorString)
		return errorString
	}
	log.Println(getLine())
	resultString, errorString = commitBooking(connectionParams, requestData.ScheduleID,
		uint8(len(adultPax)+len(childPax)),
		xmlInLog, xmlOutLog)

	if resultString == "" {
		return errorString
	}

	log.Println(getLine())
	commitResponse := bookingCommitResponseEnvelopeODT{}
	xml.Unmarshal([]byte(resultString), &commitResponse)

	if commitResponse.Body.Data.Detail.Success.RecordLocator == "" {
		return resultString
	}
	resultString, signature, errorString := getBookingInfo(connectionParams, requestData.ScheduleID,
		commitResponse.Body.Data.Detail.Success.RecordLocator,
		xmlInLog, xmlOutLog)
	if errorString != "" {
		return errorString
	}
	_ = signature
	bookingInfo := bookingInfoResponseEnvelopeODT{}
	xml.Unmarshal([]byte(resultString), &bookingInfo)

	if bookingInfo.Body.Fault.FaultCode != "" {
		apiResponse.ResultString = fmt.Sprint("Error get booking info ", bookingInfo.Body.Fault.FaultString)
		apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString)
		apiResponse.ResultCode = 200
		apiResponse.Status = false
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	if bookingInfo.Body.Data.Error.ErrorText != "" {
		apiResponse.ResultString = fmt.Sprint("Error get booking info ", bookingInfo.Body.Data.Error.ErrorText)
		apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString)
		apiResponse.ResultCode = 200
		apiResponse.Status = false
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}
	/*
		log.Println(getLine())
		log.Println(getLine(), "Add payment to booking")
		resultString, errorString = addPayment(connectionParams, signature, bookingInfo,
			xmlInLog, xmlOutLog)
		if resultString == "" {
			return errorString
		}
	*/
	apiResponse.Status = true
	bookingData := bookingInfo.Body.Data.Booking
	apiResponse.Amount = float64(bookingData.BookingSum.TotalCost)
	apiResponse.Total = float64(bookingData.BookingSum.TotalCost)
	apiResponse.TrxID = pnrToLong(bookingData.RecordLocator)
	apiResponse.Data.Amount = float64(bookingData.BookingSum.TotalCost)

	// convert booking expired from UTC to WIB+7

	wibExpired := bookingData.BookingHold.HoldDateTime
	amountCount := float64(0)
	if wibLocation != nil {
		utcTime, err := time.Parse("2006-01-02T15:04:05Z", bookingData.BookingHold.HoldDateTime)
		if err != nil {
			log.Println(getLine(), "Error parsing booking expired from parter ", bookingData.BookingHold.HoldDateTime)
			wibExpired = bookingData.BookingHold.HoldDateTime
		} else {
			wibTime := utcTime.In(wibLocation)
			wibExpired = wibTime.Format("2006-01-02 15:04:05")
		}
	} else {
		log.Println(getLine(), "WARNING!!:Location data timezon nil, just copy booking expired from partner ")
	}
	for i, journey := range bookingData.Journeys.Journey {
		apiDetail := apiBookingResponseDetailODT{}
		apiDetail.AirlineName = "Citilink"
		Segments := journey.Segments.Segment
		adultFare := float64(0)
		childFare := float64(0)
		srcFare := float64(0)
		infantFare := float64(0)
		for _, segment := range Segments {
			for _, fare := range segment.Fares.Fare {
				for _, paxFare := range fare.PaxFares.PaxFare {
					var ptrFare *float64
					//								log.Println(getLine(), "Pax Type ", paxFare.PaxType, serviceCharge.Amount)
					switch paxFare.PaxType {
					case "INF":
						ptrFare = &infantFare
					case "SRC":
						ptrFare = &srcFare
					case "CHD":
						ptrFare = &childFare
					default:
						ptrFare = &adultFare
					}
					for _, serviceCharge := range paxFare.ServiceCharges.BookingServiceCharge {

						multiplier := float64(1)
						switch serviceCharge.ChargeType {
						case "FarePrice", "TravelFee", "ServiceCharge", "Tax", "AddOnServicePrice", "AddOnServiceFee", "AddOnServiceMarkup", "FareSurcharge", "AddOnServiceCancelFee":
							multiplier = 1
						case "Discount":
							multiplier = -1
						default:
							log.Println(getLine(), "-Warning-unmapped ChargeType:", serviceCharge.ChargeType)
							multiplier = 1
						}
						tmpFloat := float64(serviceCharge.Amount)
						*ptrFare = *ptrFare + (tmpFloat * multiplier)
					}

				}
			}

		}
		apiDetail.AdultPrice = adultFare * float64(len(adultPax))
		apiDetail.ChildPrice = childFare * float64(len(childPax))
		divider := float64(1.0)
		if apiRequest.Data.ReturnScheduleID != "" {
			divider = 2
		}
		amountCount = amountCount + apiDetail.AdultPrice + apiDetail.ChildPrice
		apiDetail.InfantPrice = (apiResponse.Total / divider) - apiDetail.AdultPrice - apiDetail.ChildPrice //infantFare * float64(len(infantPax))
		apiDetail.ScheduleID = scheduleIDs[i]
		depSegment := Segments[0]
		lastSegmentIdx := len(Segments) - 1
		lastLegIdx := len(Segments[lastSegmentIdx].Legs.Leg) - 1
		arrSegment := Segments[lastSegmentIdx]
		apiDetail.DepartureCode = depSegment.DepartureStation
		apiDetail.ArrivalCode = arrSegment.ArrivalStation
		depLeg := depSegment.Legs.Leg[0]
		arrLeg := arrSegment.Legs.Leg[lastLegIdx]
		arrivalTime := strings.Split(arrLeg.STA, "T")[1]
		apiDetail.ArrivalTime = strings.TrimSuffix(arrivalTime, ":00")
		departureTime := strings.Split(depLeg.STD, "T")[1]
		apiDetail.DepartureTime = strings.TrimSuffix(departureTime, ":00")
		apiDetail.ArrivalDate = strings.Split(arrLeg.STA, "T")[0] + " " + arrivalTime
		apiDetail.DepartureDate = strings.Split(depLeg.STD, "T")[0] + " " + departureTime
		apiDetail.BookingCode = bookingData.RecordLocator
		apiDetail.BookingExpire = wibExpired
		apiResponse.Data.Details = append(apiResponse.Data.Details, apiDetail)
	}

	if amountCount > 0 {
		infantsAmount := (apiResponse.Total - amountCount) / float64(len(apiResponse.Data.Details))
		for i := range apiResponse.Data.Details {
			apiResponse.Data.Details[i].InfantPrice = infantsAmount
		}

	}
	byteString, _ := json.Marshal(apiResponse)
	return string(byteString)
	//	fmt.Fprintf(w, "%s", string(byteData))
}
