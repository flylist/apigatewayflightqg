package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"path/filepath"
	"regexp"
	"runtime"
	"strconv"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/go-redis/redis"
)

func getLine() string {
	pc, fn, line, _ := runtime.Caller(1)
	_ = pc
	_ = fn
	return fmt.Sprint(filepath.Base(fn), ":", line)
}

func createHash(key string) string {
	hasher := md5.New()
	hasher.Write([]byte(key))
	return hex.EncodeToString(hasher.Sum(nil))
}

func createHash256(key string) string {
	hasher := sha256.New()
	hasher.Write([]byte(key))
	return hex.EncodeToString(hasher.Sum(nil))
}

func encrypt(data []byte, passphrase string) []byte {
	block, _ := aes.NewCipher([]byte(createHash(passphrase)))
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return []byte("")
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return []byte("")
	}
	ciphertext := gcm.Seal(nonce, nonce, data, nil)
	return ciphertext
}

func decrypt(data []byte, passphrase string) []byte {
	key := []byte(createHash(passphrase))
	block, err := aes.NewCipher(key)
	if err != nil {
		return []byte("")
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return []byte("")
	}
	nonceSize := gcm.NonceSize()
	nonce, ciphertext := data[:nonceSize], data[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return []byte("")
	}
	return plaintext
}

func logReqRes(incomingFlag bool, outgoingFlag bool, incomingData interface{}, outgoingData interface{}) {
	if incomingFlag {
		byteData, _ := json.MarshalIndent(incomingData, " ", " ")
		log.Println("Incoming data")
		log.Println(string(byteData))
	}
	if outgoingFlag {
		byteData, _ := json.MarshalIndent(outgoingData, " ", " ")
		log.Println("Outgoing data")
		log.Println(string(byteData))
	}
}

func logReqResString(incomingFlag bool, outgoingFlag bool, incomingData string, outgoingData string) {
	if incomingFlag {
		log.Println("Incoming string")
		log.Println(incomingData)
	}
	if outgoingFlag {
		log.Println("Outgoing string")
		log.Println(outgoingData)
	}
}

func getCmdDataFromRedis(cmd string) (*soapCmdODT, string) {
	client := redis.NewClient(&redis.Options{
		Addr:     redisServer,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer client.Close()
	v, s := client.HGet(redisHashSOAPCommand, cmd).Result()

	//	log.Println(getLine(), v, s)

	if s != nil {
		return nil, fmt.Sprint(getLine(), "-", s)
	}

	cmdData := &soapCmdODT{}
	err := json.Unmarshal([]byte(v), cmdData)

	if err != nil {
		return nil, fmt.Sprintln(getLine(), err.Error())
	}
	return cmdData, ""
}

func getCmdDataFromMongoDB(cmd string) (*soapCmdODT, string) {
	session, err := mgo.Dial(mongoDBParamsURL)
	if err != nil {
		return nil, fmt.Sprint(getLine(), "-", err)
	}
	defer session.Close()
	collParams := session.DB("").C(mongoCollectionParams)
	var result connectionParamsODT

	query := bson.M{"_id": "connection_params"}
	err = collParams.Find(query).Select(bson.M{"soap_command": 1}).One(&result)
	if err != nil {
		return nil, fmt.Sprint(getLine(), "-", err)
	}

	//	log.Println(getLine(), result)
	cmdData := result.SOAPCommand[cmd]

	if cmdData.ActionHeader == "" {
		return nil, ""
	}
	log.Println(getLine(), "-Write to redis")

	client := redis.NewClient(&redis.Options{
		Addr:     redisServer,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	byteJSON, _ := json.Marshal(cmdData)
	defer client.Close()
	status := client.HSet(redisHashSOAPCommand, cmd, string(byteJSON)).Err()
	if status != nil {
		log.Println(getLine(), "-", status.Error())
	}

	return &cmdData, ""
}

func getCmdData(connectionParams *connectionParamsODT, cmd string) (*soapCmdODT, string) {
	if connectionParams != nil {
		val, ok := connectionParams.SOAPCommand[cmd]
		//		log.Println(getLine(), val, ok)
		if ok {
			return &val, ""
		}
	}
	cmdData, errorRedis := getCmdDataFromRedis(cmd)
	log.Println(getLine(), cmdData, errorRedis)
	if cmdData == nil {
		//		log.Println(getLine(), "- Get from Mongodb  ", errorRedis)
		cmdData, errorRedis = getCmdDataFromMongoDB(cmd)
	}
	//	byteString, _ := json.MarshalIndent(cmdData, " ", " ")
	//	log.Println(getLine(), string(byteString))
	return cmdData, errorRedis
}

func readConnectionParamsFromMongoDB() (*connectionParamsODT, string) {
	session, err := mgo.Dial(mongoDBParamsURL)
	if err != nil {
		return nil, fmt.Sprint(getLine(), "-", err)
	}
	defer session.Close()
	collParams := session.DB("").C(mongoCollectionParams)
	var result connectionParamsODT

	query := bson.M{"_id": "connection_params"}
	err = collParams.Find(query).One(&result)
	//	log.Println(getLine(), result)
	//	stringByte, _ := json.MarshalIndent(result, " ", " ")
	//	log.Println(getLine(), string(stringByte))
	if err != nil {
		return nil, fmt.Sprint(getLine(), "-", err)
	}

	return &result, ""
}

func writeConnectionParamsToRedis(connectionParams *connectionParamsODT) {
	client := redis.NewClient(&redis.Options{
		Addr:     redisServer,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer client.Close()
	byteData, _ := json.MarshalIndent(connectionParams, " ", " ")
	//		log.Println(getLine(), string(byteData))

	//update redis with value from db
	var serverConnection map[string]interface{}
	var serverParams serverParamsODT
	json.Unmarshal(byteData, &serverParams)
	byteData, _ = json.Marshal(serverParams)
	//		log.Println(getLine(), string(byteData))

	json.Unmarshal(byteData, &serverConnection)
	//		log.Println(getLine(), serverConnection)

	status := client.HMSet(redisHashParamsKey, serverConnection).Err()
	if status != nil {
		log.Println(getLine(), "-", status.Error())
	}

	for key, v := range connectionParams.SOAPCommand {
		byteData, _ = json.Marshal(v)
		status = client.HSet(redisHashSOAPCommand, key, string(byteData)).Err()
		if status != nil {
			log.Println(getLine(), "-Error Update command-", status.Error())
		}
	}

}

func readConnectionParams(cmd string) (*connectionParamsODT, string) {

	var connectionParams = &connectionParamsODT{}

	client := redis.NewClient(&redis.Options{
		Addr:     redisServer,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer client.Close()
	mHash, errRedis := client.HGetAll(redisHashParamsKey).Result()
	byteData, _ := json.MarshalIndent(mHash, " ", " ")

	//	log.Println(getLine(), string(byteData))
	json.Unmarshal(byteData, connectionParams)
	tmpInt, _ := strconv.Atoi(mHash["redis_aging"])
	connectionParams.RedisAging = uint16(tmpInt)
	tmpInt, _ = strconv.Atoi(mHash["max_connectings"])
	connectionParams.MaxConnectings = uint8(tmpInt)
	connectionParams.LogOutData = mHash["log_out_data"] == "1"
	connectionParams.LogInData = mHash["log_in_data"] == "1"
	//	byteString, _ := json.MarshalIndent(connectionParams, " ", " ")
	//	log.Println(getLine(), string(byteString))

	//	log.Println(getLine(), "- Read from redis") // - ", tmpConnectionParams)

	if connectionParams.Password == "" || connectionParams.AgentName == "" {
		log.Println(getLine(), "- Read from DB ")
		errString := ""
		connectionParams, errString = readConnectionParamsFromMongoDB()
		if errString != "" {
			return nil, fmt.Sprint(getLine(), "-", errString)
		}
		writeConnectionParamsToRedis(connectionParams)
	}

	//	log.Println(getLine(), connectionParams)
	//	log.Println(getLine(), tmpConnectionParams)
	if errRedis == nil {
		v, s := client.HGet(redisHashSOAPCommand, cmd).Result()
		if s == nil || v != "" {
			if connectionParams.SOAPCommand == nil {
				connectionParams.SOAPCommand = make(map[string]soapCmdODT)
			}
			var tmpSoapCmd soapCmdODT
			json.Unmarshal([]byte(v), &tmpSoapCmd)
			connectionParams.SOAPCommand[cmd] = tmpSoapCmd
			return connectionParams, ""
		}
		/*
			if connectionParams.URL == nil {
				return nil, fmt.Sprintln(getLine(), "-URL not found for ", cmd)
			}
		*/
	}

	v := connectionParams.SOAPCommand[cmd]

	if v.ActionHeader == "" {
		return connectionParams, ""

	}
	// use redis with value from db

	tmpData := &v

	if tmpData.ActionHeader == "" {
		var errString string
		tmpData, errString = getCmdDataFromMongoDB(cmd)
		if errString != "" {
			return connectionParams, errString
		}
	}

	if connectionParams.SOAPCommand == nil {
		connectionParams.SOAPCommand = make(map[string]soapCmdODT, 1)
		connectionParams.SOAPCommand[cmd] = *tmpData
	}

	if errRedis == nil {
		byteData, _ := json.Marshal(tmpData)
		status := client.HSet(redisHashSOAPCommand, cmd, string(byteData)).Err()
		if status != nil {
			log.Println(getLine(), "-", status.Error())
		}
	}

	return connectionParams, ""
}

func printEnd(text string) {
	log.Println(text)
	log.Println("===================")
}

func loadAirport() {
	session, err := mgo.Dial(mongoDBParamsURL)
	if err != nil {
		log.Println(getLine(), "-", err)
		return
	}
	defer session.Close()
	collParams := session.DB("").C("airport")
	var results []airportODT
	query := bson.M{}
	err = collParams.Find(query).All(&results)
	//	log.Println(getLine(), result)
	if err != nil {
		log.Println(getLine(), "-", err)
		return
	}

	client := redis.NewClient(&redis.Options{
		Addr:     redisServer,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer client.Close()
	for _, result := range results {
		byteString, _ := json.Marshal(result)
		status := client.HSet(redisHashAirport, result.ID, string(byteString)).Err()
		if status != nil {
			log.Println(getLine(), "Error loading aiport data into redis ", status.Error())
			return
		}
	}
}

func getAirportDetail(airportCode string) *airportODT {
	client := redis.NewClient(&redis.Options{
		Addr:     redisServer,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer client.Close()
	v, s := client.HGet(redisHashAirport, airportCode).Result()

	//	log.Println(getLine(), v, s)

	if s != nil {
		log.Println(getLine(), "Error get airport from redis -", s)
		return nil
	}

	airportData := &airportODT{}
	err := json.Unmarshal([]byte(v), airportData)

	if err != nil {
		log.Println(getLine(), "Error decoding airport data -", err.Error())
		return nil
	}
	return airportData

}

func pnrToLong(pnr string) uint64 {
	if len(pnr) > 8 {
		return 0
	}
	stringHex := fmt.Sprintf("%x", pnr)
	pnrLong, err := strconv.ParseUint(stringHex, 16, 64)
	if err != nil {
		return 0
	}
	return pnrLong
}

func longToPNR(uintPNR uint64) string {
	a := make([]byte, 8)
	binary.BigEndian.PutUint64(a, uintPNR)
	reg, err := regexp.Compile("[^a-zA-Z0-9]+")
	if err != nil {
		return ""
	}
	processedString := reg.ReplaceAllString(string(a), "")
	return string(processedString)
}
