package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func commitBooking(connectionParams *connectionParamsODT, scheduleID string,
	paxCount uint8, xmlInLog bool, xmlOutLog bool) (string, string) {

	apiResponse := &apiBasicResponseODT{APIVersion: VersionString}
	var scheduleInfo journeyDateMarketODT
	scheduleInfoString, errString := getScheduleInfo(scheduleID)
	if errString != "" {
		apiResponse.ResultString = "Error get schedule info"
		apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString, "-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = false
		byteString, _ := json.Marshal(apiResponse)
		return "", string(byteString)
	}

	if scheduleInfoString == "" {
		apiResponse.ResultString = "Schedule id not found - " + scheduleID
		apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString)
		apiResponse.ResultCode = 100
		apiResponse.Status = false
		byteString, _ := json.Marshal(apiResponse)
		return "", string(byteString)
	}
	err := json.Unmarshal([]byte(scheduleInfoString), &scheduleInfo)

	if err != nil {
		apiResponse.ResultString = "Error decoding schedule info"
		apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString, "-", err.Error())
		apiResponse.ResultCode = 100
		apiResponse.Status = false
		byteString, _ := json.Marshal(apiResponse)
		return "", string(byteString)
	}

	signature := scheduleInfo.Signature

	partnerRequest := bookingCommitRequestEnvelopeODT{Header: requestHeaderODT{ContractVersion: ContractVersion, Signature: signature}}
	partnerData := &partnerRequest.Body.Data.BookingCommitRequestData
	partnerData.State = "New"
	partnerData.CurrencyCode = "IDR"
	partnerData.RestrictionOverride = false
	partnerData.ChangeHoldDateTime = false
	partnerData.WaiveNameChangeFee = false
	partnerData.WaivePenaltyFee = false
	partnerData.WaiveSpoilageFee = false
	partnerData.DistributeToContacts = true
	partnerData.PaxCount = paxCount
	byteData, _ := xml.MarshalIndent(partnerRequest, " ", " ")
	cmd := "booking_commit"

	soapCmd, errString := getCmdData(connectionParams, cmd)
	if errString != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Error get command data-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error get connection data")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	if soapCmd == nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Command data not found-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Connection data not found")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	if xmlOutLog {
		log.Println(getLine(), "To Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	req, err := http.NewRequest("POST", soapCmd.URL, bytes.NewReader(byteData))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error preparing request")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	req.Header.Add("Content-Type", "text/xml")
	req.Header.Add("SOAPAction", soapCmd.ActionHeader)
	client := &http.Client{}
	log.Println(getLine(), "Send booking commit request")
	resp, err := client.Do(req)
	log.Println(getLine(), "Receive booking commit response")

	//	log.Println(getLine(), string(bodyRequest))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error during sending request")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}
	defer resp.Body.Close()
	defer resp.Body.Close()
	byteData, _ = ioutil.ReadAll(resp.Body)
	var partnerResponse bookingCommitResponseEnvelopeODT
	err = xml.Unmarshal(byteData, &partnerResponse)
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error(), "\n", string(byteData))
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error decoding response")
		byteData, _ := json.Marshal(apiResponse)
		if xmlInLog {
			log.Println(getLine(), "From Partner ")
			log.Println(getLine(), string(byteData))
			log.Println()
		}

		return "", string(byteData)
	}

	byteData, _ = xml.MarshalIndent(partnerResponse, " ", " ")
	if xmlInLog {
		log.Println(getLine(), "From Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	if partnerResponse.Body.Fault.FaultCode != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", partnerResponse.Body.Fault.FaultCode, "-", partnerResponse.Body.Fault.FaultString)
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Partner error: ", partnerResponse.Body.Fault.FaultString)
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}
	return string(byteData), ""
}
