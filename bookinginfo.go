package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

func storeBookingInfo(bookingData bookingODT) {
	session, err := mgo.Dial(mongoDBParamsURL)

	if err != nil {
		log.Println(getLine(), "-Error update airline data-", err)
	}
	defer session.Close()
	collParams := session.DB("").C("booking_info")

	upsertMain := bson.M{"$set": bookingData}
	_, err = collParams.UpsertId(bookingData.RecordLocator, upsertMain)
	if err != nil {
		log.Println(getLine(), "-", err, "--", string(bookingData.RecordLocator))
	}

}

func getBookingInfo(connectionParams *connectionParamsODT,
	scheduleID string, recordLocator string, xmlInLog bool, xmlOutLog bool) (string, string, string) {
	apiResponse := &apiBasicResponseODT{APIVersion: VersionString}

	signature := ""
	if scheduleID != "" {
		scheduleInfoString, _ := getScheduleInfo(scheduleID)
		if scheduleInfoString != "" {
			var scheduleInfo journeyDateMarketODT
			err := json.Unmarshal([]byte(scheduleInfoString), &scheduleInfo)

			if err == nil {
				signature = scheduleInfo.Signature
			}

		}
	}

	if signature == "" {
		sig, errCode, errString, tmpConnectionParams := getSignature(connectionParams)
		if tmpConnectionParams != nil {
			connectionParams = tmpConnectionParams
		}

		if errString != "" {
			apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", errString)
			apiResponse.ResultCode = int16(errCode)
			apiResponse.Status = apiResponse.ResultCode == 0
			apiResponse.ResultString = fmt.Sprintln("Error signature")
			byteData, _ := json.Marshal(apiResponse)
			return "", "", string(byteData)
		}
		signature = sig
	}
	partnerRequest := bookingInfoRequestEnvelopeODT{Header: requestHeaderODT{ContractVersion: ContractVersion, Signature: signature}}
	partnerData := &partnerRequest.Body.Data.GetBookingReqData
	partnerData.GetBookingBy = "RecordLocator"
	partnerData.GetByRecordLocator.RecordLocator = recordLocator
	cmd := "booking_info"

	soapCmd, errString := getCmdData(connectionParams, cmd)
	if errString != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Error get command data-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error get connection data")
		byteData, _ := json.Marshal(apiResponse)
		return "", "", string(byteData)
	}

	if soapCmd == nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Command data not found-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Connection data not found")
		byteData, _ := json.Marshal(apiResponse)
		return "", "", string(byteData)
	}

	byteData, _ := xml.MarshalIndent(partnerRequest, " ", " ")
	if xmlOutLog {
		log.Println(getLine(), "To Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	req, err := http.NewRequest("POST", soapCmd.URL, bytes.NewReader(byteData))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error preparing request")
		byteData, _ := json.Marshal(apiResponse)
		return "", "", string(byteData)
	}

	req.Header.Add("Content-Type", "text/xml")
	req.Header.Add("SOAPAction", soapCmd.ActionHeader)
	client := &http.Client{}
	log.Println(getLine(), "Send booking info request")
	resp, err := client.Do(req)
	log.Println(getLine(), "Receive booking info response")

	//	log.Println(getLine(), string(bodyRequest))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error during sending request")
		byteData, _ := json.Marshal(apiResponse)
		return "", "", string(byteData)
	}
	defer resp.Body.Close()
	byteData, _ = ioutil.ReadAll(resp.Body)
	//	log.Println(getLine(), string(byteData))
	var partnerResponse bookingInfoResponseEnvelopeODT
	err = xml.Unmarshal(byteData, &partnerResponse)
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error(), "\n", string(byteData))
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error decoding response")
		byteData, _ := json.Marshal(apiResponse)
		if xmlInLog {
			log.Println(getLine(), "From Partner ")
			log.Println(getLine(), string(byteData))
			log.Println()
		}

		return "", "", string(byteData)
	}

	byteData, _ = xml.MarshalIndent(partnerResponse, " ", " ")
	if xmlInLog {
		log.Println(getLine(), "From Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	if partnerResponse.Body.Fault.FaultCode != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", partnerResponse.Body.Fault.FaultCode, "-", partnerResponse.Body.Fault.FaultString)
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Partner error: ", partnerResponse.Body.Fault.FaultString)
		byteData, _ := json.Marshal(apiResponse)
		return "", "", string(byteData)
	}
	storeBookingInfo(partnerResponse.Body.Data.Booking)
	return string(byteData), signature, ""
}
