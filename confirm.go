package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func confirm(connectionParams *connectionParamsODT, stringRequest string) string {

	//recordLocator string,
	//	xmlInLog bool, xmlOutLog bool) (string, string) {

	apiResponse := &apiBasicResponseODT{APIVersion: VersionString}

	apiRequest := &apiConfirmRequestODT{}
	err := json.Unmarshal([]byte(stringRequest), apiRequest)

	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error decoding request")
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	recordLocator := longToPNR(apiRequest.TrxID)
	xmlInLog := apiRequest.TraceInStringReq || connectionParams.LogInData
	xmlOutLog := apiRequest.TraceOutStringReq || connectionParams.LogOutData

	bookingInfoString, signature, errString := getBookingInfo(connectionParams, "", recordLocator, xmlInLog, xmlOutLog)
	if errString != "" {
		return errString
	}

	if errString != "" {
		return string(errString)
	}

	bookingInfo := bookingInfoResponseEnvelopeODT{}
	xml.Unmarshal([]byte(bookingInfoString), &bookingInfo)

	if bookingInfo.Body.Fault.FaultCode != "" {
		apiResponse.ResultString = fmt.Sprint("Error get booking info ", bookingInfo.Body.Fault.FaultString)
		apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString)
		apiResponse.ResultCode = 200
		apiResponse.Status = false
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	if bookingInfo.Body.Data.Error.ErrorText != "" {
		apiResponse.ResultString = fmt.Sprint("Error get booking info ", bookingInfo.Body.Data.Error.ErrorText)
		apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString)
		apiResponse.ResultCode = 200
		apiResponse.Status = false
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	//test

	log.Println(getLine())
	log.Println(getLine(), "Add payment to booking")
	resultString, errorString := addPayment(connectionParams, signature, bookingInfo,
		apiRequest.TraceInStringReq, apiRequest.TraceOutStringReq)
	if resultString == "" {
		return errorString
	}

	//end test
	partnerRequest := bookingCommitRequestEnvelopeODT{Header: requestHeaderODT{ContractVersion: ContractVersion, Signature: signature}}
	partnerData := &partnerRequest.Body.Data.BookingCommitRequestData
	partnerData.State = "New"
	bookingInfoData := bookingInfo.Body.Data.Booking
	partnerData.RecordLocator = recordLocator
	partnerData.CurrencyCode = bookingInfoData.CurrencyCode
	partnerData.PaxCount = uint8(len(bookingInfoData.Passengers.Passenger))
	//	partnerData.BookingID = bookingInfoData.BookingID
	//	partnerData.BookingParentID = bookingInfoData.BookingParentID
	//	partnerData.NumericRecordLocator = bookingInfoData.NumericRecordLocator
	/*	partnerData.SourcePOS = bookingInfoData.SourcePOS
		partnerData.SourcePOS.AgencyCode = connectionParams.AgentName
		partnerData.SourcePOS.State = "New"
		partnerData.SourcePOS.LocationCode = ""
	*/
	partnerData.RestrictionOverride = false
	partnerData.ChangeHoldDateTime = false
	partnerData.WaiveNameChangeFee = false
	partnerData.WaivePenaltyFee = false
	partnerData.WaiveSpoilageFee = false
	partnerData.DistributeToContacts = true
	byteData, _ := xml.MarshalIndent(partnerRequest, " ", " ")
	cmd := "booking_commit"

	soapCmd, errString := getCmdData(connectionParams, cmd)
	if errString != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Error get command data-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error get connection data")
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	if soapCmd == nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Command data not found-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Connection data not found")
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	if xmlOutLog {
		log.Println(getLine(), "To Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	req, err := http.NewRequest("POST", soapCmd.URL, bytes.NewReader(byteData))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error preparing request")
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	req.Header.Add("Content-Type", "text/xml")
	req.Header.Add("SOAPAction", soapCmd.ActionHeader)
	client := &http.Client{}
	log.Println(getLine(), "Send confirm request")
	resp, err := client.Do(req)
	log.Println(getLine(), "Receive confirm response")

	//	log.Println(getLine(), string(bodyRequest))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error during sending request")
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}
	defer resp.Body.Close()
	byteData, _ = ioutil.ReadAll(resp.Body)
	var partnerResponse bookingCommitResponseEnvelopeODT
	err = xml.Unmarshal(byteData, &partnerResponse)
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error(), "\n", string(byteData))
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error decoding response")
		if xmlInLog {
			log.Println(getLine(), "From Partner ")
			log.Println(getLine(), string(byteData))
			log.Println()
		}
		byteData, _ = json.Marshal(apiResponse)

		return string(byteData)
	}

	//	log.Println(getLine(), string(byteData))
	byteData, _ = xml.MarshalIndent(partnerResponse, " ", " ")
	if xmlInLog {
		log.Println(getLine(), "From Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	if partnerResponse.Body.Fault.FaultCode != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", partnerResponse.Body.Fault.FaultCode, "-", partnerResponse.Body.Fault.FaultString)
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Partner error: ", partnerResponse.Body.Fault.FaultString)
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	partnerResponseData := partnerResponse.Body.Data.Detail
	if float64(partnerResponseData.Success.PNRAmount.BalanceDue) > 0 {
		log.Println(getLine(), "Error confirm ", partnerResponseData.Success.PNRAmount.BalanceDue)
		return string(byteData)
	}

	// get updated booking info
	log.Println(getLine(), "")
	bookingInfoString, _, errString = getBookingInfo(connectionParams, "", recordLocator, xmlInLog, xmlOutLog)
	if errString != "" {
		log.Println(getLine(), "Error get booking Info")
		return errString
	}

	bookingInfo = bookingInfoResponseEnvelopeODT{}
	xml.Unmarshal([]byte(bookingInfoString), &bookingInfo)

	if bookingInfo.Body.Fault.FaultCode != "" {
		apiResponse.ResultString = fmt.Sprint("Error get booking info ", bookingInfo.Body.Fault.FaultString)
		apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString)
		apiResponse.ResultCode = 200
		apiResponse.Status = false
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	if bookingInfo.Body.Data.Error.ErrorText != "" {
		apiResponse.ResultString = fmt.Sprint("Error get booking info ", bookingInfo.Body.Data.Error.ErrorText)
		apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString)
		apiResponse.ResultCode = 200
		apiResponse.Status = false
		byteData, _ := json.Marshal(apiResponse)
		return string(byteData)
	}

	apiConfirmResponse := apiConfirmResponseODT{}
	apiConfirmResponse.APIVersion = VersionString
	apiConfirmResponse.ResultCode = 0
	apiConfirmResponse.Status = true
	apiConfirmResponse.Data = bookingInfo.Body.Data.Booking
	byteData, _ = json.Marshal(apiConfirmResponse)
	return string(byteData)

}
