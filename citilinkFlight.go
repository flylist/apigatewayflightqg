package main

import (
	//	tvl "apigatewayhotel/traveloka"
	"log"
	"net/http"
	"os"
	"strings"
)

func CaselessMatcher(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.URL.Path = strings.ToLower(r.URL.Path)
		h.ServeHTTP(w, r)
	})
}

func main() {
	//	TickerRefresh()
	mux := http.NewServeMux()
	http.Handle("/", CaselessMatcher(mux))
	listenPort, exist := os.LookupEnv("QG_PORT")
	//	tiketcom.InitParams()
	if !exist {
		listenPort = "30004"
	}

	redisServer, exist := os.LookupEnv("QG_REDIS")
	if !exist {
		redisServer = "127.0.0.1:6379"

	}
	_ = redisServer

	mongoDBParamsURL, exist = os.LookupEnv("QG_MONGODB_PARAMS")
	if !exist {
		mongoDBParamsURL = "mongodb://127.0.0.1/citilink_flight"
	}

	mongoCollectionParams, exist = os.LookupEnv("QG_COLL_PARAMS")
	if !exist {
		mongoCollectionParams = "server_params"
	}
	//	readParams()

	loadAirport()
	connectionParams, _ := readConnectionParamsFromMongoDB()
	if connectionParams != nil {
		writeConnectionParamsToRedis(connectionParams)
	}
	log.Println("Listening Port: ", listenPort)
	log.Println("Main API Version:", VersionString)
	log.Println("PID:", os.Getpid())

	//	return
	//	signature, err := getSignature()
	//	result, errString := getCmdData("login")
	//	log.Println(getLine(), result, errString)
	//	return
	//	log.Println(getLine(), signature, err)

	mux.HandleFunc("/citilink/flight", flight)
	mux.HandleFunc("/citilink/config", config)
	/*	mux.HandleFunc("/flight/refreshparams", refreshRedis)
		mux.HandleFunc("/flight/tvlairportdata", TravelokaAirportData)
		mux.HandleFunc("/flight/tvlairlinedata", TravelokaAirlineData)
		mux.HandleFunc("/flight/airport", getAirPortList)
		mux.HandleFunc("/flight/bookingrule", getBookingRule)
		mux.HandleFunc("/flight/bookingpolicy", bookingPolicy)
		mux.HandleFunc("/flight/search", getFlightAvailability)
		mux.HandleFunc("/flight/book", flightBook)
		mux.HandleFunc("/flight/status", bookingStatus)
		mux.HandleFunc("/flight/confirm", flightConfirm)
		mux.HandleFunc("/flight/listrefundrequest", listRefundRequest)
		mux.HandleFunc("/flight/listpaymentrefunded", listPaymentRefundRequest)
		mux.HandleFunc("/flight/listreschedule", listReschedule)
		/*	mux.HandleFunc("/flight/airportdata", AirportData)
			mux.HandleFunc("/flight/tvlairlinedata", TravelokaAirlineData)
			mux.HandleFunc("/flight/airlinedata", AirlineData)*/
	log.Fatal(http.ListenAndServe(":"+listenPort, nil))
	return
}
