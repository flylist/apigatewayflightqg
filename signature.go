package main

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
)

func getSignature(connectionParams *connectionParamsODT) (string, uint16, string, *connectionParamsODT) {
	loginXML := initLogonRequest()

	xmlData := &loginXML.Body.Data.Param
	if connectionParams == nil {
		connectionParams, _ = readConnectionParams("login")
	}
	xmlData.AgentName = connectionParams.AgentName
	xmlData.Password = connectionParams.Password
	xmlData.DomainCode = connectionParams.DomainCode

	if connectionParams.LocationCode != "" {
		xmlData.LocationCode = connectionParams.LocationCode
	}
	if connectionParams.RoleCode != "" {
		xmlData.RoleCode = connectionParams.RoleCode
	}
	if connectionParams.TerminalInfo != "" {
		xmlData.TerminalInfo = connectionParams.TerminalInfo
	}

	byteData, _ := xml.Marshal(loginXML)
	//	log.Println(getLine(), string(byteData))
	soapCmd, _ := getCmdData(connectionParams, "login")
	//	log.Println(getLine(), connectionParams)
	//	byteString, _ := json.MarshalIndent(soapCmd, " ", " ")
	//	log.Println(getLine(), string(byteString))
	req, err := http.NewRequest("POST", soapCmd.URL, bytes.NewReader(byteData))
	if err != nil {
		//		apiResponse.ResultCode = 100
		//		apiResponse.ResultString = fmt.Sprint(getLine(), "-", err.Error())
		return "", 100, err.Error(), connectionParams
	}
	req.Header.Add("Content-Type", "text/xml")
	req.Header.Add("SOAPAction", soapCmd.ActionHeader)
	//	log.Println(getLine(), connectionParams.SOAPCommand["login"].URL)
	client := &http.Client{}
	resp, err := client.Do(req)

	//	log.Println(getLine(), string(bodyRequest))
	if err != nil {
		/*		apiResponse.ResultCode = 100
				apiResponse.ResultString = fmt.Sprint(getLine(), "-", err.Error())
				return "", apiResponse*/
		errString := fmt.Sprint(getLine(), err.Error())
		return "", 100, errString, connectionParams
	}
	defer resp.Body.Close()
	byteData, _ = ioutil.ReadAll(resp.Body)
	//	log.Println(getLine(), string(byteData))

	var logonResponse logonResponseEnvelopeODT
	err = xml.Unmarshal(byteData, &logonResponse)
	if logonResponse.Body.Data.Signature != "" {
		return logonResponse.Body.Data.Signature, 0, "", connectionParams
	}
	if err != nil {
		return "", 200, fmt.Sprint(getLine(), "-", err.Error(), string(byteData)), connectionParams
	}

	if logonResponse.Body.Fault.FaultCode != "" {
		return "", 200, fmt.Sprint(getLine(), "-", logonResponse.Body.Fault.FaultCode, "-",
			logonResponse.Body.Fault.FaultString), connectionParams
	}
	return "", 200, fmt.Sprint(getLine(), "-", string(byteData)), connectionParams
}
