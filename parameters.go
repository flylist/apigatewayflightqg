package main

import (
	"sync"
)

const VersionString = "3.3"

var serverParams = struct {
	sync.RWMutex
}{}

type soapCmdODT struct {
	Cmd          string `json:"cmd" bson:"command"`
	URL          string `json:"url" bson:"url"`
	ActionHeader string `json:"action_header" bson:"action_header"`
}

type serverParamsODT struct {
	DomainCode       string `json:"domain_code" bson:"domain_code"`
	MaxConnectings   uint8  `json:"max_connectings" bson:"max_connectings"`
	AgentName        string `json:"agent_name" bson:"agent_name"`
	Password         string `json:"password" bson:"password"`
	LocationCode     string `json:"location_code" bson:"location_code"`
	RoleCode         string `json:"role_code" bson:"role_code"`
	TerminalInfo     string `json:"terminal_info" bson:"terminal_info"`
	RedisAging       uint16 `json:"redis_aging" bson:"redis_aging"`
	OrganizationCode string `json:"organization_code" bson:"organization_code"`
	ImageURL         string `json:"image_url" bson:"image_url"`
	LogOutData       bool   `json:"log_out_data" bson:"log_out_data"`
	LogInData        bool   `json:"log_in_data" bson:"log_in_data"`
}

type connectionParamsODT struct {
	DomainCode       string                `json:"domain_code" bson:"domain_code"`
	MaxConnectings   uint8                 `json:"max_connectings" bson:"max_connectings"`
	AgentName        string                `json:"agent_name" bson:"agent_name"`
	Password         string                `json:"password" bson:"password"`
	LocationCode     string                `json:"location_code" bson:"location_code"`
	RoleCode         string                `json:"role_code" bson:"role_code"`
	TerminalInfo     string                `json:"terminal_info" bson:"terminal_info"`
	RedisAging       uint16                `json:"redis_aging" bson:"redis_aging"`
	OrganizationCode string                `json:"organization_code" bson:"organization_code"`
	ImageURL         string                `json:"image_url" bson:"image_url"`
	LogOutData       bool                  `json:"log_out_data" bson:"log_out_data"`
	LogInData        bool                  `json:"log_in_data" bson:"log_in_data"`
	SOAPCommand      map[string]soapCmdODT `json:"soap_command" bson:"soap_command"`
}

type airportODT struct {
	ID          string `json:"id" bson:"_id"`
	CityName    string `json:"city_name" bson:"city_name"`
	AirportName string `json:"international_name" bson:"international_name"`
	TimeZone    string `json:"timezone" bson:"timezone"`
}

type searchIDODT struct {
	LogID          string `json:"log_id" bson:"log_id"`
	FareSellKey    string `json:"fare_sell_key" bson:"fare_sell_key"`
	SegmentSellKey string `json:"segment_sell_key" bson:"segment_sell_key"`
	JourneySellKey string `json:"journey_sell_key" bson:"journey_sell_key"`
}

var mongoDBParamsURL string //= "mongodb://127.0.0.1/citilink_flight"
var mongoCollectionParams string
var redisServer string // = "127.0.0.1"
const redisAging = 30  //minutes
var connectionParams *connectionParamsODT

const redisHashParamsKey = "citilink:Flight"
const redisHashSOAPCommand = "citilink:Flight:SOAPCommand"
const redisSearchIncr = "citilink:Flight:ICR"
const redisSearchLog = "citilink:Flight:SearchLog"
const redisAvailLog = "citilink:Flight:AvailLog"
const redisHashAirport = "citilink:Flight:airports"

const mongoDBCommon = "common_ref"
const secretSalt = "tuoempleh"

var paxTypes = map[string]string{"adult": "ADT", "child": "CHD", "infant": "INF", "senior": "SRC"}

/*
how to use
counter.RLock()
n := counter.m["some_key"]
counter.RUnlock()
fmt.Println("some_key:", n)

counter.Lock()
counter.m["some_key"]++
counter.Unlock()
*/

func readParams() {
	serverParams.Lock()
	connectionParams = &connectionParamsODT{}
	_, _ = readConnectionParams("")

	serverParams.Unlock()
}
