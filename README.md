# Traveloka Hotel API

## Environment Variable

* QG_PORT : listening port

        default 30004
        
* QG_REDIS : redis server (ip:port)

        default 127.0.0.1:6379
        
* QG_MONGODB_PARAMS : mongodb server+database storing server parameters

        default mongodb://127.0.0.1/citilink_flight
        
* QG_COLL_PARAMS : mongodb collection storing server parameters

        default server_params

## DB requirement

* Mongodb

        DB Name as configured in the environment
        Collection name as configured in the environment
        Collection content (see file in the database directory)

* Redis
