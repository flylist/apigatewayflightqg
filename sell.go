package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/go-redis/redis"
)

func getScheduleInfo(scheduleID string) (string, string) {
	client := redis.NewClient(&redis.Options{
		Addr:     redisServer,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer client.Close()
	var key string
	key = redisAvailLog + ":" + scheduleID
	//	log.Println(getLine(), key)
	value, err := client.Get(key).Result()

	//	log.Println(getLine(), err)
	if err != nil {
		return "", fmt.Sprint(getLine(), "- error get schedule_id - ", err)
	}
	return value, ""
}

func sell(connectionParams *connectionParamsODT,
	scheduleIDs []string, adultPax []apiPaxDetailODT, childPax []apiPaxDetailODT,
	xmlInLog bool, xmlOutLog bool) (string, string) {

	apiResponse := &apiBasicResponseODT{APIVersion: VersionString}
	var scheduleInfoStrings []string
	for _, scheduleID := range scheduleIDs {
		scheduleInfoString, errString := getScheduleInfo(scheduleID)
		//		log.Println(getLine(), scheduleInfoString)
		if errString != "" {
			apiResponse.ResultString = "Error get schedule info-" + scheduleID
			apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString, "-", errString)
			apiResponse.ResultCode = 100
			apiResponse.Status = false
			byteString, _ := json.Marshal(apiResponse)
			return "", string(byteString)
		}
		scheduleInfoStrings = append(scheduleInfoStrings, scheduleInfoString)
	}

	if len(scheduleInfoStrings) < 1 {
		apiResponse.ResultString = fmt.Sprint("Schedule id not found - ", scheduleIDs)
		apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString)
		apiResponse.ResultCode = 100
		apiResponse.Status = false
		byteString, _ := json.Marshal(apiResponse)
		return "", string(byteString)
	}

	var scheduleInfos []journeyDateMarketODT
	for _, scheduleInfoString := range scheduleInfoStrings {
		scheduleInfo := journeyDateMarketODT{}
		err := json.Unmarshal([]byte(scheduleInfoString), &scheduleInfo)

		if err != nil {
			apiResponse.ResultString = "Error decoding schedule info"
			apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString, "-", err.Error())
			apiResponse.ResultCode = 100
			apiResponse.Status = false
			byteString, _ := json.Marshal(apiResponse)
			return "", string(byteString)
		}
		scheduleInfos = append(scheduleInfos, scheduleInfo)
	}
	partnerRequest := sellRequestEnvelopeODT{Header: requestHeaderODT{ContractVersion: ContractVersion,
		Signature: scheduleInfos[0].Signature}}
	partnerData := &partnerRequest.Body.Data.SellRequestDetail
	partnerData.SellBy = "JourneyBySellKey"
	sellData := &partnerData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData
	sellData.ActionStatusCode = "NN"
	journey := scheduleInfos[0].Journeys.Journey[0]
	sellData.CurrencyCode = journey.Segments.Segment[0].Fares.Fare[0].PaxFares.PaxFare[0].ServiceCharges.BookingServiceCharge[0].CurrencyCode
	sellData.IsAllotmentMarketFare = false
	sellData.PreventOverLap = false
	sellData.LoyaltyFilter = "MonetaryOnly"

	//filled based on request
	var ppt paxPriceTypeODT
	ppts := &sellData.PaxPriceType
	sellData.PaxCount = 0
	for range adultPax {
		ppt.PaxType = "ADT"
		ppts.PaxPriceType = append(ppts.PaxPriceType, ppt)
		sellData.PaxCount++
	}

	for range childPax {
		ppt.PaxType = "CHD"
		ppts.PaxPriceType = append(ppts.PaxPriceType, ppt)
		sellData.PaxCount++
	}

	for _, scheduleInfo := range scheduleInfos {
		for _, journey := range scheduleInfo.Journeys.Journey {
			jsk := journeySellKeyODT{}
			jointer := ""
			for _, segment := range journey.Segments.Segment {
				//				stringByte, _ := json.MarshalIndent(segment.Fares, " ", " ")
				//				log.Println(getLine(), string(stringByte))
				for _, fare := range segment.Fares.Fare {
					jsk.FareSellKey += jointer + fare.FareSellKey
					jointer = "^"
					//					log.Println(getLine(), segment.DepartureStation, fare.FareSellKey, fare.FareBasisCode)
				}
				jsk.JourneySellKey = journey.JourneySellKey
			}
			sellData.JourneySellKeys.SellKeyList = append(sellData.JourneySellKeys.SellKeyList, jsk)
		}
	}
	byteData, _ := xml.MarshalIndent(partnerRequest, "", " ")

	cmd := "sell"

	soapCmd, errString := getCmdData(connectionParams, cmd)
	if errString != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Error get command data-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error get connection data")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	if soapCmd == nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Command data not found-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Connection data not found")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	req, err := http.NewRequest("POST", soapCmd.URL, bytes.NewReader(byteData))

	if xmlOutLog {
		log.Println(getLine(), "To Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error preparing request")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}
	req.Header.Add("Content-Type", "text/xml")
	req.Header.Add("SOAPAction", soapCmd.ActionHeader)
	//	log.Println(getLine(), connectionParams.SOAPCommand["login"].URL)
	client := &http.Client{}
	log.Println(getLine(), "Send sell request")
	resp, err := client.Do(req)
	log.Println(getLine(), "Receive sell response")

	//	log.Println(getLine(), string(bodyRequest))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error during sending request")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}
	defer resp.Body.Close()
	byteData, _ = ioutil.ReadAll(resp.Body)
	//	log.Println(getLine(), string(byteData))
	var partnerResponse sellResponseEnvelopeODT
	err = xml.Unmarshal(byteData, &partnerResponse)
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error(), "\n", string(byteData))
		if xmlInLog {
			log.Println(getLine(), "From Partner ")
			log.Println(getLine(), string(byteData))
			log.Println()
		}

		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error decoding response")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	byteData, _ = xml.MarshalIndent(partnerResponse, " ", " ")
	if xmlInLog {
		log.Println(getLine(), "From Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	if partnerResponse.Body.Fault.FaultCode != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", partnerResponse.Body.Fault.FaultCode, "-", partnerResponse.Body.Fault.FaultString)
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Partner error: ", partnerResponse.Body.Fault.FaultString)
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}
	return string(byteData), ""
}

func sellINF(connectionParams *connectionParamsODT, scheduleIDs []string, infant []apiPaxDetailODT,
	xmlInLog bool, xmlOutLog bool) (string, string) {

	apiResponse := &apiBasicResponseODT{APIVersion: VersionString}

	var scheduleInfoStrings []string
	for _, scheduleID := range scheduleIDs {
		scheduleInfoString, errString := getScheduleInfo(scheduleID)
		if errString != "" {
			apiResponse.ResultString = "Error get schedule info-" + scheduleID
			apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString, "-", errString)
			apiResponse.ResultCode = 100
			apiResponse.Status = false
			byteString, _ := json.Marshal(apiResponse)
			return "", string(byteString)
		}
		scheduleInfoStrings = append(scheduleInfoStrings, scheduleInfoString)
	}

	if len(scheduleInfoStrings) < 1 {
		apiResponse.ResultString = fmt.Sprint("Schedule id not found - ", scheduleIDs)
		apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString)
		apiResponse.ResultCode = 100
		apiResponse.Status = false
		byteString, _ := json.Marshal(apiResponse)
		return "", string(byteString)
	}

	var scheduleInfos []journeyDateMarketODT
	for _, scheduleInfoString := range scheduleInfoStrings {
		scheduleInfo := journeyDateMarketODT{}
		err := json.Unmarshal([]byte(scheduleInfoString), &scheduleInfo)

		if err != nil {
			apiResponse.ResultString = "Error decoding schedule info"
			apiResponse.ErrorString = fmt.Sprint(getLine(), "-", apiResponse.ResultString, "-", err.Error())
			apiResponse.ResultCode = 100
			apiResponse.Status = false
			byteString, _ := json.Marshal(apiResponse)
			return "", string(byteString)
		}
		scheduleInfos = append(scheduleInfos, scheduleInfo)
	}

	partnerRequest := sellRequestEnvelopeODT{Header: requestHeaderODT{ContractVersion: ContractVersion, Signature: scheduleInfos[0].Signature}}
	partnerData := &partnerRequest.Body.Data.SellRequestDetail
	partnerData.SellBy = "SSR"
	sellData := &partnerData.SellSSR.SSRRequest
	segmentSSRRequests := &sellData.SegmentSSRRequests.SegmentSSRRequest
	for _, scheduleInfo := range scheduleInfos {
		journeys := scheduleInfo.Journeys.Journey
		sellData.CurrencyCode = journeys[0].Segments.Segment[0].Fares.Fare[0].PaxFares.PaxFare[0].ServiceCharges.BookingServiceCharge[0].CurrencyCode
		for _, journey := range journeys {
			for _, segment := range journey.Segments.Segment {
				segmentSSR := segmentSSRRequestODT{}
				paxSSRs := &segmentSSR.PaxSSRs.PaxSSR

				for i := range infant {
					paxSSR := paxSSRODT{}
					paxSSR.State = "New"
					paxSSR.ActionStatusCode = "NN"
					paxSSR.ArrivalStation = segment.ArrivalStation
					paxSSR.DepartureStation = segment.DepartureStation
					paxSSR.PassengerNumber = uint8(i)
					paxSSR.SSRCode = "INF"
					paxSSR.FeeCode = "INF"
					paxSSR.SSRNumber = uint8(i)
					paxSSR.SSRValue = "0"
					*paxSSRs = append(*paxSSRs, paxSSR)
				}
				segmentSSR.ArrivalStation = segment.ArrivalStation
				segmentSSR.DepartureStation = segment.DepartureStation
				segmentSSR.FlightDesignator = segment.FlightDesignator
				segmentSSR.STD = segment.STD
				*segmentSSRRequests = append(*segmentSSRRequests, segmentSSR)
			}
		}
	}
	byteData, _ := xml.MarshalIndent(partnerRequest, " ", " ")

	cmd := "sell"

	soapCmd, errString := getCmdData(connectionParams, cmd)
	if errString != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Error get command data-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error get connection data")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	if soapCmd == nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-Command data not found-", errString)
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Connection data not found")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}

	if xmlOutLog {
		log.Println(getLine(), "To Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	req, err := http.NewRequest("POST", soapCmd.URL, bytes.NewReader(byteData))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 100
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error preparing request")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}
	req.Header.Add("Content-Type", "text/xml")
	req.Header.Add("SOAPAction", soapCmd.ActionHeader)
	client := &http.Client{}
	log.Println(getLine(), "Send sell infant request")
	resp, err := client.Do(req)
	log.Println(getLine(), "Receive sell infant response")

	//	log.Println(getLine(), string(bodyRequest))
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error())
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error during sending request")
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}
	defer resp.Body.Close()
	byteData, _ = ioutil.ReadAll(resp.Body)
	var partnerResponse sellResponseEnvelopeODT
	err = xml.Unmarshal(byteData, &partnerResponse)
	if err != nil {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", err.Error(), "\n", string(byteData))
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Error decoding response")
		byteData, _ := json.Marshal(apiResponse)
		if xmlInLog {
			log.Println(getLine(), "From Partner ")
			log.Println(getLine(), string(byteData))
			log.Println()
		}

		return "", string(byteData)
	}

	byteData, _ = xml.MarshalIndent(partnerResponse, " ", " ")
	if xmlInLog {
		log.Println(getLine(), "From Partner ")
		log.Println(getLine(), string(byteData))
		log.Println()
	}

	if partnerResponse.Body.Fault.FaultCode != "" {
		apiResponse.ErrorString = fmt.Sprintln(getLine(), "-", partnerResponse.Body.Fault.FaultCode, "-", partnerResponse.Body.Fault.FaultString)
		apiResponse.ResultCode = 200
		apiResponse.Status = apiResponse.ResultCode == 0
		apiResponse.ResultString = fmt.Sprintln("Partner error: ", partnerResponse.Body.Fault.FaultString)
		byteData, _ := json.Marshal(apiResponse)
		return "", string(byteData)
	}
	return string(byteData), ""
}
